﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyekRPG
{
    public class ClsNode
    {
        string nama;
        int bobot;
        int level;

        List<int> x = new List<int>();
        List<int> y = new List<int>();

        internal ClsNode    parent;
        internal ClsNode    prev;
        internal ClsNode    next;
        internal ClsNode[]  child;
        int jumChildren;

        public List<int> X { get => x; set => x = value; }
        public List<int> Y { get => y; set => y = value; }

        public ClsNode() 
        {
            this.nama = "";
            this.bobot = 0; 
            this.parent = null;
            
            this.prev = null; 
            this.next = null;
            this.child = new ClsNode[100];
            for (int i = 0; i < 100; i++) { this.child[i] = null; }
            this.jumChildren = 0; 
        }

        public ClsNode(string nama, int bobot,List<int> x,List<int> y)
        {
            this.x = x;
            this.y = y;
            this.nama = nama;
            this.bobot = bobot;
            this.prev = null;
            this.next = null;
            this.child = new ClsNode[100];
            for (int i = 0; i < 100; i++) { this.child[i] = null; }
            this.jumChildren = 0;
        }

        public int getLevel() { return this.level; }
        public int getBobot() { return this.bobot; }
        public string getNama() { return this.nama; }
        public ClsNode getParent() { return this.parent; }
        public ClsNode getPrev() { return this.prev; }
        public ClsNode getNext() { return this.next; }
        public int getJumChildren() { return this.jumChildren; }
        public ClsNode getChildren(int nomerchildren) { return this.child[nomerchildren]; }

        public void setLevel(int level) { this.level = level; }
        public void setNama(string nama) { this.nama = nama; }
        public void setParent(ClsNode parent) { this.parent = parent; }
        public void setPrev(ClsNode prev)     { this.prev = prev; }
        public void setNext(ClsNode next)     { this.next = next; }
        public void insertChildAtFront(ClsNode newchild)
        {
            if (jumChildren == 0) { child[0] = newchild; }
            else
            {
                for (int i = jumChildren; i > 0; i--)
                {
                    child[i] = child[i - 1];
                }
                child[0] = newchild;
            }
            jumChildren += 1; 
        }
        public void insertChildAtBack(ClsNode newchild)
        {
            child[jumChildren] = newchild;
            jumChildren += 1; 
        }
    }
}
