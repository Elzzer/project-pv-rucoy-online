﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyekRPG
{
    public class Item
    {
        public int banyak { get; set; }
        public string jenis { get; set; }
        public int harga { get; set; }
        public Item()
        {

        }
        public Item(string jenis)
        {
            this.jenis = jenis;
            this.banyak = 5;
            if (jenis == "Potion") harga = 200;
            else if (jenis == "Max Potion") harga = 300;
            else if (jenis == "Calcium") harga = 400;
            else if (jenis == "Clarity") harga = 500;
            else if (jenis == "Attack Boost") harga = 600;
            else if (jenis == "Defense Boost") harga = 700;
            else if (jenis == "Arrow") harga = 800;
        }
    }
}
