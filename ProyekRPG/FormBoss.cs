﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Media;

namespace ProyekRPG
{
    public partial class FormBoss : Form
    {
        bool diserang = false;
        player p = new player();

        // buat jalan
        Image imgs, img;
        Image[,,] character;
        bool bolehjalan = true;
        int tempjalan = 0; int arahjalan = 0;
        int job = 0;

        Panel infoCharacter, panelInven;

        Label lblPlayer;

        Image bgInven = Image.FromFile("kotakinven1.png");
        Image[] cropWeapon = new Image[3];
        List<inventory> weapon = new List<inventory>();

        int clickWeaponY = -1, clickWeaponX = -1;
        bool isPaused = false;
        Image borderJob = Image.FromFile("borderjob.png"), borderChar = Image.FromFile("borderchar.png");
        int gold = 0;
        Image[,,] animationAttack;
        List<character> player = new List<character>();

        Image imgAtt; //temp buat animasi serang
        int tempanimasi = 0; //buat timer animasi
        bool bolehSerang = true;

        List<Item> listItem = new List<Item>(); //simpan item
        List<Image> inputItem = new List<Image>();

        public class bullet
        {
            public int xBullet;
            public int yBullet;
            public Image bulletImage;

            public bullet(int xBullet, int yBullet)
            {
                this.xBullet = xBullet;
                this.yBullet = yBullet;
                this.bulletImage = Image.FromFile("tengah.png");
            }
        }

        Random rand = new Random();
        int hpBoss = 200;
        int xBoss = 400, yBoss = 100;
        int ctr = 0, spawnPeluru = 0;

        string stage = "medium";
        int attBoss = 0;

        List<Image> boss1 = new List<Image>();
        List<bullet> bulletlist = new List<bullet>();

        bool chest = true;
        bool buka = false;
        string obtainItem = ""; int timerObtainItem = 60; bool appearObtainItem = false;

        bool dblatk = false;
        bool pieringShot = false;
        int ctrmage = 0;
        Image[] slash = new Image[5];
        int dmgmage = 0;
        int thunderSize = 50;
        bool mageSKill = false;
        Image[] lightningStrike = new Image[4];
        

        public FormBoss()
        {
            InitializeComponent();

            player.Add(new warrior());
            player.Add(new mage());
            player.Add(new archer());

            int ctrLightning = 0;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Bitmap bmp1 = new Bitmap("lightning.png");
                    Rectangle rr = new Rectangle((i * 700), (j * 700), 700, 700);
                    lightningStrike[ctrLightning] = bmp1.Clone(rr, bmp1.PixelFormat);
                    ctrLightning++;
                }
            }
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            SoundPlayer soundPlayer = new SoundPlayer(Application.StartupPath + @"\Battle3.wav");
            soundPlayer.Play();

            stage = ((Form1)MdiParent).stage;
            gold = ((Form1)MdiParent).gold;
            loadData();
            this.Size = new Size(800, 600);

            if (stage == "easy")
            {
                boss1.Add(Image.FromFile("boss11.png"));
                boss1.Add(Image.FromFile("boss12.png"));
                boss1.Add(Image.FromFile("boss13.png"));
                hpBoss = 200; attBoss = 20;
            }
            else if (stage == "medium")
            {
                boss1.Add(Image.FromFile("boss21.png"));
                boss1.Add(Image.FromFile("boss22.png"));
                boss1.Add(Image.FromFile("boss23.png"));
                hpBoss = 350; attBoss = 50;
            }

            else if (stage == "hard")
            {
                boss1.Add(Image.FromFile("boss31.png"));
                boss1.Add(Image.FromFile("boss32.png"));
                boss1.Add(Image.FromFile("boss33.png"));
                hpBoss = 500; attBoss = 100;
            }
            
            progressBar1.ForeColor = Color.Red;
            progressBar1.Maximum = player[job].hpbase;
            progressBar1.Value = player[job].hp;
            progressBar2.ForeColor = Color.Blue;
            progressBar2.Maximum = player[job].mpbase;
            progressBar2.Value = player[job].mp;

            progressBar3.ForeColor = Color.Red;
            progressBar3.Maximum = hpBoss;
            progressBar3.Value = hpBoss;
            //lblHpplayer.Text = player[job].hp + "/" + player[job].hpbase;
            lblHpplayer.Visible = false;
            lblHpBoss.Text = hpBoss + "";

            timerGerak.Start();
            timer2.Start();




            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic, null, panel1, new object[] { true });
            //for (int i = 0; i < 5; i++)
            //{
            //    Bitmap bmp1 = new Bitmap("Slash.png");
            //    Rectangle rr = new Rectangle((i * 192), 0, 192, 192);
            //    slash[i] = bmp1.Clone(rr, bmp1.PixelFormat);
            //}

            panel1.Size = new Size(800, 500);
            panel1.Location = new Point(300, 100);
            panel1.BackColor = Color.Transparent;
            //panel1.BackgroundImage = Image.FromFile("Map002.png");
            //this.BackgroundImage = Image.FromFile("Map002.png");
            this.BackgroundImageLayout = ImageLayout.Stretch;
            this.BackColor = Color.Black;
            //this.Size = new Size(800, 600);



            //awal print job(buat load)
            if (job == 0)
            {
                if (player[job].lvl < 10) imgs = Image.FromFile("character1.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("warrior2.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("warrior31.png") as Bitmap;
            }
            else if (job == 1)
            {
                if (player[job].lvl < 10) imgs = Image.FromFile("character2.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("mage11.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("mage21.png") as Bitmap;
            }
            else if (job == 2)
            {
                if (player[job].lvl < 10) imgs = Image.FromFile("character3.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("archer21.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("archer31.png") as Bitmap;
            }
            cropImage(); cropAttack(); panel1.Invalidate();

            infoCharacter = new Panel();
            infoCharacter.Location = new Point(300, 200);
            infoCharacter.Size = new Size(200, 200);
            infoCharacter.Paint += new PaintEventHandler(infoCharacter_paint);
            infoCharacter.MouseClick += new MouseEventHandler(Change_Job);
            panel1.Controls.Add(infoCharacter);
            infoCharacter.Visible = false;

            panelInven = new Panel();
            panelInven.Location = new Point(300, 200);
            panelInven.Size = new Size(260, 200);
            panelInven.Paint += new PaintEventHandler(panelInven_paint);
            panelInven.MouseClick += new MouseEventHandler(Click_Weapon);
            panelInven.MouseDoubleClick += new MouseEventHandler(DoubleClick_EquipWeapon);
            panel1.Controls.Add(panelInven);
            panelInven.Visible = false;

            Button b = new Button();
            b.Location = new Point(65, 160);
            b.Size = new Size(60, 30);
            b.Text = "Inventory";
            b.Click += new EventHandler(button_Inventory);
            infoCharacter.Controls.Add(b);

            imgs = Image.FromFile("weapon.png") as Bitmap;
            Rectangle r = new Rectangle(198, 64, 96, 64);
            Bitmap bmp = new Bitmap(imgs);
            cropWeapon[0] = bmp.Clone(r, bmp.PixelFormat); // sword
            r = new Rectangle(190, 324, 96, 60);
            bmp = new Bitmap(imgs);
            cropWeapon[1] = bmp.Clone(r, bmp.PixelFormat); //staff
            r = new Rectangle(480, 0, 96, 64);
            bmp = new Bitmap(imgs);
            cropWeapon[2] = bmp.Clone(r, bmp.PixelFormat); // bow

            p.X = 50;
            p.Y = 350;
            lblPlayer = new Label();
            lblPlayer.Size = new Size(50, 50);
            lblPlayer.Location = new Point(p.X, p.Y);
            panel1.Controls.Add(lblPlayer);
            lblPlayer.BackColor = Color.Transparent;
            lblPlayer.Click += new EventHandler(InfoPanel_ClickLabel);
            lblPlayer.Image = img;

            //weapon.Add(new inventory(0, 50, 0)); weapon[0].isEquip = true;
            //weapon.Add(new inventory(1, 50, 0)); weapon[1].isEquip = true;
            //weapon.Add(new inventory(2, 50, 0)); weapon[2].isEquip = true;


            panel1.BackColor = Color.Transparent;
            panel1.BackgroundImage = SetImageOpacity(Image.FromFile("mapboss" + ".png"), 1F);
            panel1.BackgroundImageLayout = ImageLayout.Stretch;
            lblPlayer.BackColor = Color.Transparent;
            lblPlayer.Image = SetImageOpacity(img, 1F);

            for (int i = 0; i < 6; i++)
            {
                inputItem.Add(Image.FromFile((1 + i) + ".png"));
            }
            inputItem.Add(Image.FromFile("Potion.png"));
            inputItem.Add(Image.FromFile("MaxPotion.png"));
            inputItem.Add(Image.FromFile("Clarity.png"));
            inputItem.Add(Image.FromFile("AttUp.png"));
            inputItem.Add(Image.FromFile("DefenseUp.png"));
            inputItem.Add(Image.FromFile("Arrow.png"));
        }

        public void skill1_mage(int damage)
        {
            //skill damage x 1,5 -- mana cost -50%

            thunderSize = 50;
            dmgmage = (damage * 3) / 2;
            mageSKill = true;
            bolehSerang = false;
            bolehjalan = false;
            timer1.Start();


        }
        public void skill2_mage(int damage)
        {
            //skill damage x2 -- no mana

            thunderSize = 100;
            dmgmage = damage * 2;
            mageSKill = true;
            bolehSerang = false;
            bolehjalan = false;
            timer1.Start();

        }

        public void doubleAttack()
        {
            player[job].arrow--;
            bolehSerang = false;
            tempanimasi = 0;
            imgAtt = animationAttack[job, arahjalan, 0];
            dblatk = true;
            timer1.Start();
            serang();

        }
        public void piercingShot()
        {
            player[job].arrow--;
            bolehSerang = false;
            tempanimasi = 0;
            imgAtt = animationAttack[job, arahjalan, 0];
            pieringShot = true;
            timer1.Start();
            serang();
        }

        public void whirlwind(int damage)
        {
            Rectangle a = new Rectangle(lblPlayer.Location.X - 25, lblPlayer.Location.Y - 25, lblPlayer.Width + 50, lblPlayer.Height + 50);
            Rectangle b = new Rectangle(xBoss - 10, yBoss - 10, 50 + 20, 50 + 20);
            if (a.IntersectsWith(b))
            {
                hpBoss -= damage;
                progressBar3.Value = hpBoss;
            }
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == 'w')player1.Top -= 20;
            //if (e.KeyChar == 'a')player1.Left -= 20;
            //if (e.KeyChar == 's')player1.Top += 20;
            //if (e.KeyChar == 'd')player1.Left += 20;

            
            if (e.KeyChar == 'p')
            {
                ((Form1)MdiParent).gold = gold;
                saveData();
                ((Form1)MdiParent).loadGame();
                this.Close();
            }

            if (!diserang && !isPaused)
            {
                if (e.KeyChar == 'w' && bolehjalan)
                {
                    //p.Y -= 50;
                    img = character[job, 3, 0];
                    tempjalan = 0; arahjalan = 3;
                    bolehjalan = false;
                    timer1.Start();
                }
                else if (e.KeyChar == 's' && bolehjalan)
                {
                    //p.Y += 50;
                    img = character[job, 0, 0];
                    tempjalan = 0; arahjalan = 2;
                    bolehjalan = false;
                    timer1.Start();
                }
                else if (e.KeyChar == 'a' && bolehjalan)
                {
                    //p.X -= 50;
                    img = character[job, 1, 0];
                    tempjalan = 0; arahjalan = 1;
                    bolehjalan = false;
                    timer1.Start();
                }
                else if (e.KeyChar == 'd' && bolehjalan)
                {
                    //p.X += 50;
                    img = character[job, 2, 0];
                    tempjalan = 0; arahjalan = 0;
                    bolehjalan = false;
                    timer1.Start();
                }
                else if (e.KeyChar == '1')
                {
                    if (listItem[0].banyak > 0)
                    {
                        listItem[0].banyak--;
                        player[job].hp += (player[job].hpbase * 10 / 100);
                        if (player[job].hp > player[job].hpbase) player[job].hp = player[job].hpbase;
                        progressBar1.Value = player[job].hp;
                        this.Invalidate();
                    }
                }
                else if (e.KeyChar == '2')
                {
                    if (listItem[1].banyak > 0)
                    {
                        listItem[1].banyak--;
                        player[job].hp = player[job].hpbase;
                        progressBar1.Value = player[job].hp;
                        this.Invalidate();
                    }
                }
                else if (e.KeyChar == '3')
                {
                    if (listItem[3].banyak > 0)
                    {
                        listItem[3].banyak--;
                        player[job].mp += (player[job].mpbase * 10 / 100);
                        if (player[job].mp > player[job].mpbase) player[job].mp = player[job].mpbase;
                        progressBar2.Value = player[job].mp;
                        this.Invalidate();
                    }
                }
                else if (e.KeyChar == '4' && listItem[4].banyak > 0)
                {
                    player[job].attBoost = true;
                    player[job].timerAttBoost = 200;
                    listItem[4].banyak--;
                    this.Invalidate();
                }
                else if (e.KeyChar == '5' && listItem[5].banyak > 0)
                {
                    player[job].defBoost = true;
                    player[job].timerDefBoost = 200;
                    listItem[5].banyak--;
                    this.Invalidate();
                }
                else if (e.KeyChar == '6' && job == 2)
                {
                    if (listItem[6].banyak > 0)
                    {
                        //MessageBox.Show("Test");
                        listItem[6].banyak--;
                        player[job].arrow += 10;
                        this.Invalidate();
                    }
                }
                else if (e.KeyChar == 'q')
                {
                    if (job == 0 && player[job].mp > 30 && player[job].lvl > 9) //warrior
                    {
                        if (player[job].skill.Equals("skill1"))
                        {
                            int index = 0;
                            for (int i = 0; i < weapon.Count; i++)
                            {
                                if (job == weapon[i].jenis && weapon[i].isEquip)
                                {
                                    index = i;
                                }
                            }

                            whirlwind(weapon[index].att);
                        }
                        else if (player[job].skill.Equals("skill2"))
                        {
                            //instantKill(arahjalan);
                        }
                        player[job].mp -= 30;
                        progressBar2.Value = player[job].mp; //PROGRESS BAR MP
                    }
                    else if (job == 1) // mage
                    {
                        if (player[job].skill.Equals("skill1") && player[job].mp > 45)
                        {
                            int index = -1;
                            for (int i = 0; i < weapon.Count; i++)
                            {
                                if (job == weapon[i].jenis && weapon[i].isEquip)
                                {
                                    index = i;
                                }
                            }
                            int damage = 100;
                            if (index != -1)
                            {
                                damage = weapon[index].att;
                            }

                            skill1_mage(damage);
                            player[job].mp /= 2;
                            progressBar2.Value = player[job].mp; //PROGRESS BAR MP
                        }
                        else if (player[job].skill.Equals("skill2"))
                        {
                            int index = -1;
                            for (int i = 0; i < weapon.Count; i++)
                            {
                                if (job == weapon[i].jenis && weapon[i].isEquip)
                                {
                                    index = i;
                                }
                            }
                            int damage = 100;
                            if (index != -1)
                            {
                                damage = weapon[index].att;
                            }

                            //player[job].mp /= 2;
                            skill2_mage(damage);
                            progressBar2.Value = player[job].mp; //PROGRESS BAR MP
                        }

                    }
                    else if (job == 2 && player[job].mp > 15) //archer
                    {
                        if (player[job].skill.Equals("skill1"))
                        {
                            int index = 0;
                            for (int i = 0; i < weapon.Count; i++)
                            {
                                if (job == weapon[i].jenis && weapon[i].isEquip)
                                {
                                    index = i;
                                }
                            }
                            doubleAttack();
                            //whirlwind(weapon[index].att);
                        }
                        else if (player[job].skill.Equals("skill2"))
                        {
                            piercingShot();
                        }
                        player[job].mp -= 15;
                        progressBar2.Value = player[job].mp; //PROGRESS BAR MP
                    }

                }
            }
        
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (player[job].attBoost)
            {
                player[job].timerAttBoost--;
                if (player[job].timerAttBoost == 0) player[job].attBoost = false;
            }
            if (player[job].defBoost)
            {
                player[job].timerDefBoost--;
                if (player[job].timerDefBoost == 0) player[job].defBoost = false;
            }
            if (appearObtainItem) timerObtainItem--;
            if (timerObtainItem <= 0) appearObtainItem = false;

            if (hpBoss <= 0)
            {
                saveData();
                //((Form1)MdiParent).loadGame();
                //this.Close();
                //timer2.Stop();
                timerGerak.Stop();
                
                if (chest == true)
                {
                    boss1[0] = Image.FromFile("chest_tutup.png");
                    boss1[1] = Image.FromFile("chest_tutup.png");
                    boss1[2] = Image.FromFile("chest_tutup.png");
                    chest = false;
                }
                if (!buka) { } //MessageBox.Show("You Win");
                else
                {
                    boss1[0] = Image.FromFile("chest_buka.png");
                    boss1[1] = Image.FromFile("chest_buka.png");
                    boss1[2] = Image.FromFile("chest_buka.png");
                }
            }

            for (int i = 0; i < bulletlist.Count; i++)
            {
                bulletlist[i].yBullet += 10;
            }

            panel1.Invalidate();

            int hapus = -1;

            for (int i = 0; i < bulletlist.Count; i++)
            {
                Rectangle r1 = new Rectangle(bulletlist[i].xBullet, bulletlist[i].yBullet, 50, 50);
                Rectangle r2 = new Rectangle(p.X, p.Y, 50, 50);
                if (r1.IntersectsWith(r2))
                {
                    if(player[job].defBoost) player[job].hp -= (attBoss/2);
                    else player[job].hp -= attBoss;
                    if (player[job].hp < 0)
                    {
                        player[job].hp = 0;
                        timer1.Stop();
                        timer2.Stop();
                        timerGerak.Stop();

                        panel1.BackColor = Color.Transparent;
                        panel1.BackgroundImage = SetImageOpacity(Image.FromFile("mapboss" + ".png"), 0.25F);
                        isPaused = true;
                        this.Invalidate();
                    }
                    progressBar1.Value = player[job].hp;
                    lblHpplayer.Text = player[job].hp + "/" + player[job].hpbase;
                    hapus = i;
                    
                }
            }
            if (hapus >= 0) bulletlist.RemoveAt(hapus);
        }

        private void timerGerak_Tick(object sender, EventArgs e)
        {
            ctr++;
            Invalidate();
            if (ctr == 3)ctr = 0;

            spawnPeluru++;
            label2.Text = spawnPeluru + "";
            if (spawnPeluru == 50)
            {
                bulletlist.Add(new bullet(xBoss, yBoss));
                spawnPeluru = 0;
            }

            if (stage == "medium" || stage == "hard")
            {
                if (p.X > xBoss)
                {
                    xBoss += 10;
                    Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                    Rectangle r2 = new Rectangle(xBoss, yBoss, 50, 50);
                    if (r1.IntersectsWith(r2))
                    {
                        xBoss -= 10;
                    }
                    if (xBoss == 760) xBoss -= 10;
                    Invalidate();
                }
                else if (p.X < xBoss)
                {
                    xBoss -= 10;
                    Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                    Rectangle r2 = new Rectangle(xBoss, yBoss, 50, 50);
                    if (r1.IntersectsWith(r2))
                    {
                        xBoss += 10;
                    }
                    if (xBoss <= 180) xBoss += 10;
                    Invalidate();
                }
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Font f = new Font("Arial", 16, FontStyle.Bold);
            Brush b = new SolidBrush(Color.White);
            //if (ctr == 0) n.DrawImage(boss1[0], xBoss, yBoss, 60, 60);
            //if (ctr == 1) n.DrawImage(boss1[1], xBoss, yBoss, 60, 60);
            //if (ctr == 2) n.DrawImage(boss1[2], xBoss, yBoss, 60, 60);
            //for (int i = 0; i < bulletlist.Count; i++)
            //{
            //    n.DrawImage(bulletlist[i].bulletImage, bulletlist[i].xBullet, bulletlist[i].yBullet, 40, 40);
            //}

            if (player[job].hp <= 0)
            {
                g.DrawString("Game Over !!!", f, b, 660, 630);
                g.DrawString("Press P to Go Home", f, b, 630, 660);
            }
            
            //g.DrawImage(inputItem[0], 610, 390, 20, 20); 
            g.DrawString(" " + listItem[0].banyak, new Font("arial", 11, FontStyle.Bold), b, 910, 620); g.DrawImage(inputItem[6], 910, 600, 20, 20);
            //g.DrawImage(inputItem[1], 640, 390, 20, 20); 
            g.DrawString(" " + listItem[1].banyak, new Font("arial", 11, FontStyle.Bold), b, 940, 620); g.DrawImage(inputItem[7], 940, 600, 20, 20);
            //g.DrawImage(inputItem[2], 670, 390, 20, 20); 
            g.DrawString(" " + listItem[3].banyak, new Font("arial", 11, FontStyle.Bold), b, 970, 620); g.DrawImage(inputItem[8], 970, 600, 20, 20);
            //g.DrawImage(inputItem[3], 700, 390, 20, 20); 
            g.DrawString(" " + listItem[4].banyak, new Font("arial", 11, FontStyle.Bold), b, 1000, 620); g.DrawImage(inputItem[9], 1000, 600, 20, 20);
            //g.DrawImage(inputItem[4], 730, 390, 20, 20); 
            g.DrawString(" " + listItem[5].banyak, new Font("arial", 11, FontStyle.Bold), b, 1030, 620); g.DrawImage(inputItem[10], 1030, 600, 20, 20);
            //g.DrawImage(inputItem[5], 760, 390, 20, 20); 
            g.DrawString(" " + listItem[6].banyak, new Font("arial", 11, FontStyle.Bold), b, 1060, 620); g.DrawImage(inputItem[11], 1060, 600, 20, 20);
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            if (ctr == 0) g.DrawImage(boss1[0], xBoss, yBoss, 60, 60);
            if (ctr == 1) g.DrawImage(boss1[1], xBoss, yBoss, 60, 60);
            if (ctr == 2) g.DrawImage(boss1[2], xBoss, yBoss, 60, 60);
            for (int i = 0; i < bulletlist.Count; i++)
            {
                g.DrawImage(bulletlist[i].bulletImage, bulletlist[i].xBullet, bulletlist[i].yBullet, 40, 40);
            }

            if (!bolehSerang)
            {
                //int gerakBow = 0;
                //if (job == 2) gerakBow = tempanimasi * 5;
                //if (arahjalan == 0) g.DrawImage(imgAtt, p.X + 30 + gerakBow, p.Y, 50, 50);
                //else if (arahjalan == 1) g.DrawImage(imgAtt, p.X - 30 - gerakBow, p.Y, 50, 50);
                //else if (arahjalan == 2) g.DrawImage(imgAtt, p.X, p.Y + 30 + gerakBow, 50, 50);
                //else if (arahjalan == 3) g.DrawImage(imgAtt, p.X, p.Y - 30 - gerakBow, 50, 50);

                int x = 0; int y = 0;
                int gerakBow = 0;
                if (job == 2) gerakBow = tempanimasi * 5;

                if (pieringShot)
                {
                    int index = -1;
                    for (int i = 0; i < weapon.Count; i++)
                    {
                        if (job == weapon[i].jenis && weapon[i].isEquip)
                        {
                            index = i;
                        }
                    }
                    int damage = 0;
                    if (index != -1)
                    {
                        damage = weapon[index].att;
                    }
                    damage = 100;


                    Rectangle a = new Rectangle(x, y, 50, 50);
                    Rectangle ab = new Rectangle(xBoss, yBoss, 50, 50);
                    if (a.IntersectsWith(ab))
                    {
                        hpBoss -= damage;
                        if (hpBoss >= 0) progressBar3.Value = hpBoss;
                        else progressBar3.Value = 0;
                        if (hpBoss <= 0) tambahExp();
                    }
                }
                else if (mageSKill)
                {
                    x = lblPlayer.Location.X;
                    y = lblPlayer.Location.Y;
                    if (arahjalan == 0) { x += 50; }
                    else if (arahjalan == 1) { x -= 50; }
                    else if (arahjalan == 2) { y += 50; }
                    else if (arahjalan == 3) { y -= 50; }

                    if (ctrmage < 10)
                    {
                        g.DrawImage(lightningStrike[0], x - (thunderSize / 100 * 25), y - (thunderSize - 50), thunderSize, thunderSize);
                    }
                    else if (ctrmage < 20)
                    {
                        g.DrawImage(lightningStrike[1], x - (thunderSize / 100 * 25), y - (thunderSize - 50), thunderSize, thunderSize);
                    }
                    else if (ctrmage < 30)
                    {
                        g.DrawImage(lightningStrike[2], x - (thunderSize / 100 * 25), y - (thunderSize - 50), thunderSize, thunderSize);
                    }
                    else
                    {
                        g.DrawImage(lightningStrike[3], x - (thunderSize / 100 * 25), y - (thunderSize - 50), thunderSize, thunderSize);
                    }

                }
                else
                {
                    if (arahjalan == 0) g.DrawImage(imgAtt, p.X + 30 + gerakBow, p.Y, 50, 50);
                    else if (arahjalan == 1) g.DrawImage(imgAtt, p.X - 30 - gerakBow, p.Y, 50, 50);
                    else if (arahjalan == 2) g.DrawImage(imgAtt, p.X, p.Y + 30 + gerakBow, 50, 50);
                    else if (arahjalan == 3) g.DrawImage(imgAtt, p.X, p.Y - 30 - gerakBow, 50, 50);
                }
            }
            Font f = new Font("Arial", 14, FontStyle.Bold);
            Brush b = new SolidBrush(Color.White);
            if (appearObtainItem) g.DrawString(obtainItem, f, b, 350, 50);
            if (timerObtainItem <= 0) g.DrawString("Press P to Go Town",f,b,350,50);

        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!bolehSerang)
            {
                //kanan kiri bawah atas
                if (tempanimasi <= 4) imgAtt = animationAttack[job, arahjalan, 0];
                else if (tempanimasi <= 9) imgAtt = animationAttack[job, arahjalan, 1];
                else if (tempanimasi == 10)
                {
                    imgAtt = animationAttack[job, arahjalan, 2];
                    bolehSerang = true; timer1.Stop();
                }
                tempanimasi++;
                panel1.Invalidate();

                if (pieringShot)
                {
                    //kanan kiri bawah atas
                    if (tempanimasi <= 4) imgAtt = animationAttack[job, arahjalan, 0];
                    else if (tempanimasi <= 19) imgAtt = animationAttack[job, arahjalan, 1];
                    else if (tempanimasi == 20)
                    {
                        imgAtt = animationAttack[job, arahjalan, 2];
                        bolehSerang = true; timer1.Stop();
                        bolehjalan = true;
                        pieringShot = false;
                    }
                    tempanimasi++;
                    panel1.Invalidate();
                }
                else if (mageSKill)
                {
                    if (ctrmage > 35)
                    {
                        mageSKill = false;
                        Rectangle a = new Rectangle(xBoss, yBoss, 50, 50);
                        int x = lblPlayer.Location.X;
                        int y = lblPlayer.Location.Y;
                        // kanan kiri bawah atas
                        if (arahjalan == 0)
                        {
                            x += 50;
                        }
                        else if (arahjalan == 1)
                        {
                            x -= 50;
                        }
                        else if (arahjalan == 2)
                        {
                            y += 50;
                        }
                        else if (arahjalan == 3)
                        {
                            y -= 50;
                        }
                        Rectangle b = new Rectangle(x + 10, y + 10, 30, 30);
                        if (a.IntersectsWith(b))
                        {
                            hpBoss -= dmgmage;
                            if (hpBoss >= 0) progressBar3.Value = hpBoss;
                            else progressBar3.Value = 0;
                            //if (hpBoss <= 0) tambahExp();
                        }
                        ctrmage = 0;
                        timer1.Stop();
                        bolehjalan = true;
                        bolehSerang = true;
                    }
                    else
                    {
                        ctrmage++;
                        if (ctrmage > 20)
                        {
                            Rectangle a = new Rectangle(xBoss, yBoss, 50, 50);
                            int x = lblPlayer.Location.X;
                            int y = lblPlayer.Location.Y;
                            // kanan kiri bawah atas
                            if (arahjalan == 0)
                            {
                                x += 50;
                            }
                            else if (arahjalan == 1)
                            {
                                x -= 50;
                            }
                            else if (arahjalan == 2)
                            {
                                y += 50;
                            }
                            else if (arahjalan == 3)
                            {
                                y -= 50;
                            }
                            Rectangle b = new Rectangle(x + 10, y + 10, 30, 30);
                            if (a.IntersectsWith(b))
                            {
                                hpBoss -= dmgmage;
                                if (hpBoss >= 0) progressBar3.Value = hpBoss;
                                else progressBar3.Value = 0;
                                //if (hpBoss <= 0) tambahExp();

                            }
                        }

                    }
                    panel1.Invalidate();
                }
                else
                {
                    //kanan kiri bawah atas
                    if (tempanimasi <= 4) imgAtt = animationAttack[job, arahjalan, 0];
                    else if (tempanimasi <= 9) imgAtt = animationAttack[job, arahjalan, 1];
                    else if (tempanimasi == 10)
                    {
                        imgAtt = animationAttack[job, arahjalan, 2];
                        bolehSerang = true; timer1.Stop();
                        bolehjalan = true;
                        if (dblatk)
                        {
                            player[job].arrow--;
                            bolehSerang = false;
                            tempanimasi = 0;
                            imgAtt = animationAttack[job, arahjalan, 0];
                            dblatk = false;
                            timer1.Start();
                            serang();
                        }
                    }
                    tempanimasi++;
                    panel1.Invalidate();
                }

            }
            //gerak animasi player
            if (!bolehjalan)
            {
                if (arahjalan == 0) //Ke Kanan
                {
                    if (tempjalan == 0) img = character[job, 2, 1];
                    else if (tempjalan == 1) img = character[job, 2, 2];
                    else if (tempjalan == 2) img = character[job, 2, 0];
                    else if (tempjalan == 3) img = character[job, 2, 2];
                    else if (tempjalan == 4) { img = character[job, 2, 0]; bolehjalan = true; timer1.Stop(); }
                    p.X += 10; tempjalan++;

                    Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                    Rectangle r2 = new Rectangle(xBoss, yBoss, 50, 50);
                    if (r1.IntersectsWith(r2))
                    {
                        p.X -= 10;
                    }

                    if (p.Y >= 350 && p.Y <= 400 && p.X == 560) p.X -= 10;
                    if (p.Y >= 250 && p.Y <= 300 && p.X == 410) p.X -= 10;
                    if (p.X > 700) p.X -= 10;
                }
                else if (arahjalan == 1)
                { //Ke Kiri
                    if (tempjalan == 0) img = character[job, 1, 1];
                    else if (tempjalan == 1) img = character[job, 1, 2];
                    else if (tempjalan == 2) img = character[job, 1, 0];
                    else if (tempjalan == 3) img = character[job, 1, 2];
                    else if (tempjalan == 4) { img = character[job, 1, 0]; bolehjalan = true; timer1.Stop(); }
                    p.X -= 10; tempjalan++;

                    Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                    Rectangle r2 = new Rectangle(xBoss, yBoss, 50, 50);
                    if (r1.IntersectsWith(r2))
                    {
                        p.X += 10;
                    }

                    if (p.Y == 50 && p.X == 240) p.X += 10;
                    if (p.Y == 100 && p.X == 190) p.X += 10;
                    if (p.Y == 150 && p.X == 140) p.X += 10;
                    if (p.Y >= 250 && p.Y <= 300 && p.X == 390) p.X += 10;
                    if (p.X < 50) p.X += 10;
                }
                else if (arahjalan == 2) //Ke Bawah
                {
                    if (tempjalan == 0) img = character[job, 0, 1];
                    else if (tempjalan == 1) img = character[job, 0, 2];
                    else if (tempjalan == 2) img = character[job, 0, 0];
                    else if (tempjalan == 3) img = character[job, 0, 2];
                    else if (tempjalan == 4) { img = character[job, 0, 0]; bolehjalan = true; timer1.Stop(); }
                    p.Y += 10; tempjalan++;

                    Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                    Rectangle r2 = new Rectangle(xBoss, yBoss, 50, 50);
                    if (r1.IntersectsWith(r2))
                    {
                        p.Y -= 10;
                    }

                    if (p.X >= 450 && p.X <= 700 && p.Y == 210) p.Y -= 10;
                    if (p.X >= 50 && p.X <= 350 && p.Y == 210) p.Y -= 10;
                    if (p.Y > 400) p.Y -= 10;
                }
                else if (arahjalan == 3) //Ke Atas
                {
                    if (tempjalan == 0) img = character[job, 3, 1];
                    else if (tempjalan == 1) img = character[job, 3, 2];
                    else if (tempjalan == 2) img = character[job, 3, 0];
                    else if (tempjalan == 3) img = character[job, 3, 2];
                    else if (tempjalan == 4) { img = character[job, 3, 0]; bolehjalan = true; timer1.Stop(); }
                    p.Y -= 10; tempjalan++;

                    Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                    Rectangle r2 = new Rectangle(xBoss, yBoss, 50, 50);
                    if (r1.IntersectsWith(r2))
                    {
                        p.Y += 10;
                    }

                    if (p.X == 200 && p.Y == 90) p.Y += 10;
                    if (p.X == 150 && p.Y == 140) p.Y += 10;
                    if (p.X >= 50 && p.X <= 100 && p.Y == 190) p.Y += 10;
                    if (p.X >= 450 && p.X <= 550 && p.Y == 340) p.Y += 10;
                    if (p.X >= 50 && p.X <= 350 && p.Y == 340) p.Y += 10;
                    if (p.Y < 50) p.Y += 10;
                }
                lblPlayer.Image = img;
                lblPlayer.Location = new Point(p.X, p.Y);
            }
        }
        //UNTUK PLAYER
        private void InfoPanel_ClickLabel(object sender, EventArgs e)
        {
            timer2.Stop();
            timerGerak.Stop();

            panel1.BackColor = Color.Transparent;
            panel1.BackgroundImage = SetImageOpacity(Image.FromFile("mapboss" + ".png"), 0.25F);
            lblPlayer.BackColor = Color.Transparent;
            lblPlayer.Image = SetImageOpacity(img, 0.5F);
            isPaused = true;

            //MessageBox.Show("Info Karakter!!!");
            infoCharacter.Visible = true;
            infoCharacter.BackColor = Color.FromArgb(150, Color.MintCream);
            panelInven.BackColor = Color.FromArgb(150, Color.MintCream);
        }
        private void DoubleClick_EquipWeapon(object sender, MouseEventArgs e)
        {
            clickWeaponX = e.X - 18; clickWeaponX /= 25;
            clickWeaponY = e.Y; clickWeaponY /= 66;

            bool cekBenarEquip = false;
            int index = clickWeaponX + clickWeaponY * 6;
            if (weapon[index].jenis == job)
            {
                weapon[index].isEquip = true;
                cekBenarEquip = true;
            }

            for (int i = 0; i < weapon.Count; i++)
            {
                if (cekBenarEquip)
                {
                    if (job == 0 && weapon[i].jenis == 0 && i != index) weapon[i].isEquip = false;
                    else if (job == 1 && weapon[i].jenis == 1 && i != index) weapon[i].isEquip = false;
                    else if (job == 2 && weapon[i].jenis == 2 && i != index) weapon[i].isEquip = false;
                }
            }
            panelInven.Invalidate();
        }

        private void Click_Weapon(object sender, MouseEventArgs e)
        {
            if (e.X >= 19 && e.X <= 170)
            {
                clickWeaponX = e.X - 18; clickWeaponX /= 25;
                clickWeaponY = e.Y; clickWeaponY /= 66;
            }

            panelInven.Invalidate();
            //MessageBox.Show("X : " + e.X + "\nY : " + e.Y);
            //170x20
        }

        private void panelInven_paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(bgInven, 18, 0, 150, 200);
            Brush b = new SolidBrush(Color.Black);
            Brush bw = new SolidBrush(Color.White);
            Font f = new Font("Monotype Corsiva", 13, FontStyle.Bold);
            // buat kluarin shop
            for (int i = 0; i < weapon.Count; i++)
            {
                if (weapon[i].jenis == 0) g.DrawImage(cropWeapon[0], i % 6 * 25 + 15, i / 6 * 70, 55, 50); //draw sword
                else if (weapon[i].jenis == 1) g.DrawImage(cropWeapon[1], i % 6 * 25 + 9, i / 6 * 70, 55, 50); //draw staff
                else if (weapon[i].jenis == 2) g.DrawImage(cropWeapon[2], i % 6 * 25, i / 6 * 70, 55, 50); //draw bow

                //draw gambar e utk equip
                if (weapon[i].isEquip) g.DrawString("E", f, bw, i % 6 * 25 + 25, i / 6 * 67);
            }
            if (clickWeaponX != -1 && clickWeaponY != -1)
            {
                int index = clickWeaponX + clickWeaponY * 6;
                if (weapon[index].jenis == 0) g.DrawImage(cropWeapon[0], 193, 10, 55, 50);
                else if (weapon[index].jenis == 1) g.DrawImage(cropWeapon[1], 193 - 6, 10, 55, 50);
                else if (weapon[index].jenis == 2) g.DrawImage(cropWeapon[2], 193 - 15, 10, 55, 50);
                g.DrawString(weapon[index].att + "", f, b, 195, 70);
                g.DrawString("+" + weapon[index].plusUpgrade, f, b, 213, 20);

            }

        }

        private void Change_Job(object sender, MouseEventArgs e)
        {
            Rectangle r = new Rectangle(e.X, e.Y, 1, 1);
            Rectangle r1 = new Rectangle(10, 130, 60, 30);
            Rectangle r2 = new Rectangle(70, 130, 50, 30);
            Rectangle r3 = new Rectangle(120, 130, 50, 30);

            if (r.IntersectsWith(r1))
            {
                job = 0;
                //masukkan gambar ke array
                if (player[job].lvl < 10) imgs = Image.FromFile("character1.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("warrior2.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("warrior31.png") as Bitmap;
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();

                progressBar1.ForeColor = Color.Red;
                progressBar1.Maximum = player[job].hpbase;
                progressBar1.Value = player[job].hp;
                progressBar2.ForeColor = Color.Blue;
                progressBar2.Maximum = player[job].mpbase;
                progressBar2.Value = player[job].mp;
            }
            else if (r.IntersectsWith(r2))
            {
                job = 1;
                //masukkan gambar ke array
                if (player[job].lvl < 10) imgs = Image.FromFile("character2.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("mage11.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("mage21.png") as Bitmap;
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();

                progressBar1.ForeColor = Color.Red;
                progressBar1.Maximum = player[job].hpbase;
                progressBar1.Value = player[job].hp;
                progressBar2.ForeColor = Color.Blue;
                progressBar2.Maximum = player[job].mpbase;
                progressBar2.Value = player[job].mp;
            }
            else if (r.IntersectsWith(r3))
            {
                job = 2;
                //masukkan gambar ke array
                if (player[job].lvl < 10) imgs = Image.FromFile("character3.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("archer21.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("archer31.png") as Bitmap;
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();

                progressBar1.ForeColor = Color.Red;
                progressBar1.Maximum = player[job].hpbase;
                progressBar1.Value = player[job].hp;
                progressBar2.ForeColor = Color.Blue;
                progressBar2.Maximum = player[job].mpbase;
                progressBar2.Value = player[job].mp;
            }
            lblPlayer.Image = img;
        }

        private void button_Inventory(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            //MessageBox.Show("INVENTORY!!!");
            infoCharacter.Visible = false;
            panelInven.Visible = true;
            clickWeaponX = -1; clickWeaponY = -1;
        }

        private void panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //spawnenemymedium();
            
        }

        private void infoCharacter_paint(object sender, PaintEventArgs e)
        {

            Graphics g = e.Graphics;
            Brush b = new SolidBrush(Color.Black);
            Font f = new Font("Monotype Corsiva", 17, FontStyle.Bold);

            //cetak border job
            if (job == 0) g.DrawImage(borderJob, 8, 128, 62, 26);
            else if (job == 1) g.DrawImage(borderJob, 68, 128, 52, 26);
            else if (job == 2) g.DrawImage(borderJob, 118, 128, 52, 26);

            //cetak border Character
            //g.DrawImage(borderChar, 120, 10, 60, 70);

            g.DrawImage(img, 135, 20, 50, 50);
            g.DrawString("HP : " + player[job].hp + "/" + player[job].hpbase, f, b, 10, 10);
            g.DrawString("MP : " + player[job].mp + "/" + player[job].mpbase, f, b, 10, 40);
            g.DrawString("Lvl : " + player[job].lvl, f, b, 10, 70);
            g.DrawString("Gold : " + gold + " Dz", f, b, 10, 100);
            f = new Font("Monotype Corsiva", 13, FontStyle.Bold);
            g.DrawString("Warrior", f, b, 10, 130);
            g.DrawString("Mage", f, b, 70, 130);
            g.DrawString("Archer", f, b, 120, 130);
        }
        public Image SetImageOpacity(Image image, float opacity)
        {
            Bitmap bmp = new Bitmap(image.Width, image.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                ColorMatrix matrix = new ColorMatrix();
                matrix.Matrix33 = opacity;
                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default,
                                                  ColorAdjustType.Bitmap);
                g.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height),
                                   0, 0, image.Width, image.Height,
                                   GraphicsUnit.Pixel, attributes);
            }
            return bmp;
        }

        //coding potong gambar gerak player
        public void cropImage()
        {
            character = new Image[3, 4, 3];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    //MessageBox.Show(i + " " + j);
                    Rectangle r = new Rectangle(j * 47, i * 48, 47, 48);
                    Bitmap bmp = new Bitmap(imgs);
                    character[job, i, j] = bmp.Clone(r, bmp.PixelFormat);
                }
            }


            if (arahjalan == 0) img = character[job, 2, 0];
            else if (arahjalan == 1) img = character[job, 1, 0];
            else if (arahjalan == 2) img = character[job, 0, 0];
            else if (arahjalan == 3) img = character[job, 3, 0];
        }

        public void cropAttack()
        {
            //potong gambar buat animasi pedang
            animationAttack = new Image[3, 4, 3];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i <= 1)
                    {
                        Rectangle r = new Rectangle((i * 360) + (j * 180), 180, 180, 180);
                        Bitmap bmp = new Bitmap(Image.FromFile("swordslash.png"));
                        animationAttack[0, i, j] = bmp.Clone(r, bmp.PixelFormat);
                    }
                    else if (i == 2)
                    {
                        Rectangle r = new Rectangle(j * 180, 360, 180, 180);
                        Bitmap bmp = new Bitmap(Image.FromFile("swordslash.png"));
                        animationAttack[0, i, j] = bmp.Clone(r, bmp.PixelFormat);
                    }
                    else if (i == 3)
                    {
                        Rectangle r = new Rectangle(j * 180 + 360, 540, 180, 180);
                        Bitmap bmp = new Bitmap(Image.FromFile("swordslash.png"));
                        animationAttack[0, i, j] = bmp.Clone(r, bmp.PixelFormat);
                    }
                }
            }
            for (int i = 0; i < 3; i++)
            {
                Rectangle r = new Rectangle(i * 192 + 192, 576, 192, 192);
                Bitmap bmp = new Bitmap(Image.FromFile("magethunder.png"));
                for (int j = 0; j < 4; j++)
                {
                    animationAttack[1, j, i] = bmp.Clone(r, bmp.PixelFormat);
                }
            }
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i == 0) animationAttack[2, i, j] = Image.FromFile((j + 1) + " kanan.png");
                    else if (i == 1) animationAttack[2, i, j] = Image.FromFile((j + 1) + " kiri.png");
                    else if (i == 2) animationAttack[2, i, j] = Image.FromFile((j + 1) + " bwh.png");
                    else if (i == 3) animationAttack[2, i, j] = Image.FromFile((j + 1) + " atas.png");
                }
            }
        }
        
        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            if (player[job].hp > 0)
            {
                Rectangle mouse = new Rectangle(e.X, e.Y, 1, 1);
                Rectangle karakter = new Rectangle(p.X - 50, p.Y, 50, 50);
                if (mouse.IntersectsWith(karakter))
                {
                    //MessageBox.Show("Info Karakter!!!");
                    //infoCharacter.Visible = true;
                    //pindah di event click pada label player
                }
                else
                {
                    timer2.Start();
                    timerGerak.Start();

                    isPaused = false;
                    //ganti OPACITY
                    panel1.BackColor = Color.Transparent;
                    panel1.BackgroundImage = SetImageOpacity(Image.FromFile("mapboss" + ".png"), 1F);
                    lblPlayer.BackColor = Color.Transparent;
                    lblPlayer.Image = SetImageOpacity(img, 1F);

                    infoCharacter.Visible = false;
                    panelInven.Visible = false;

                    //animasi serang
                    if ((job == 2 && player[job].arrow > 0) || job == 1 || job == 0)
                    {
                        if (job == 2) player[job].arrow--;
                        bolehSerang = false; tempanimasi = 0;
                        imgAtt = animationAttack[job, arahjalan, 0];
                        timer1.Start();
                    }

                    //BUAT SERANG!!!!

                    if (hpBoss > 0)
                    {
                        serang();
                        if (hpBoss < 0) hpBoss = 0;
                        progressBar3.Value = hpBoss;
                    }
                    else
                    {
                        Rectangle mouse1 = new Rectangle(e.X, e.Y, 5, 5);
                        Rectangle chest = new Rectangle(xBoss, yBoss, 50, 50);
                        if (mouse1.IntersectsWith(chest) && buka == false)
                        {
                            boss1[0] = Image.FromFile("chest_tutup.png");
                            boss1[1] = Image.FromFile("chest_buka_dikit.png");
                            boss1[2] = Image.FromFile("chest_buka.png");
                            buka = true;
                            int jenis = rand.Next(0, 3);
                            if (jenis == 0) obtainItem = "You Got Sword!!";
                            else if (jenis == 1) obtainItem = "You Got Staff!!";
                            else if (jenis == 2) obtainItem = "You Got Bow!!";
                            appearObtainItem = true;
                            if (stage == "easy")
                            {
                                int att = rand.Next(30, 41);
                                weapon.Add(new inventory(jenis, att, 0));
                            }
                            else if (stage == "medium")
                            {
                                int att = rand.Next(70, 86);
                                weapon.Add(new inventory(jenis, att, 0));
                            }
                            else if (stage == "hard")
                            {
                                int att = rand.Next(100, 136);
                                weapon.Add(new inventory(jenis, att, 0));

                            }
                            panel1.Invalidate();
                        }
                    }
                }
            }
        }
        
        public void tambahExp(string musuh)
        {
            if (musuh == "easy") player[job].exp += 10;
            else if (musuh == "medium") player[job].exp += 15;
            else if (musuh == "hard") player[job].exp += 120;

            if (player[job].exp >= 100)
            {
                player[job].exp %= 100;
                player[job].lvl += 5;
                player[job].hpbase += 1;
                player[job].mpbase += 1;
                progressBar1.Maximum = player[job].hpbase;
                progressBar2.Maximum = player[job].mpbase;
            }
            if (player[job].lvl >= 10 && player[job].lvl < 20)
            {
                MessageBox.Show("evo 2");
                if (job == 0) { imgs = Image.FromFile("warrior2.png") as Bitmap; player[job].skill = "skill1"; }
                else if (job == 1) { imgs = Image.FromFile("mage11.png") as Bitmap; player[job].skill = "skill1"; }
                else if (job == 2) { imgs = Image.FromFile("archer21.png") as Bitmap; player[job].skill = "skill1"; }
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
                lblPlayer.Image = img;
                player[job].hpbase *= 2;
                player[job].mpbase *= 2;
                progressBar1.Maximum = player[job].hpbase;
                progressBar2.Maximum = player[job].mpbase;
            }
            else if (player[job].lvl >= 20)
            {
                MessageBox.Show("evo 3");
                if (job == 0) { imgs = Image.FromFile("warrior31.png") as Bitmap; player[job].skill = "skill2"; }
                else if (job == 1) { imgs = Image.FromFile("mage21.png") as Bitmap; player[job].skill = "skill2"; }
                else if (job == 2) { imgs = Image.FromFile("archer31.png") as Bitmap; player[job].skill = "skill2"; }
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
                lblPlayer.Image = img;
                player[job].hpbase *= 2;
                player[job].mpbase *= 2;
                progressBar1.Maximum = player[job].hpbase;
                progressBar2.Maximum = player[job].mpbase;
            }

        }

        public void serang()
        {
            int serang = 0;
            for (int i = 0; i < weapon.Count; i++)
            {
                if (job == weapon[i].jenis && weapon[i].isEquip) serang = weapon[i].att;
            }
            if (player[job].attBoost) serang = serang + (serang / 2); //buat boost item attack
            if (arahjalan == 0)
            {
                //kanan
                Rectangle kotakPlayer = new Rectangle(p.X + 50, p.Y, 50, 50);
                Rectangle kotakBoss = new Rectangle(xBoss, yBoss, 50, 50);
                if (kotakPlayer.IntersectsWith(kotakBoss)){
                    hpBoss -= serang;
                    if (hpBoss <= 0) { tambahExp(); }
                }
            }
            else if (arahjalan == 1)
            {
                //kiri
                Rectangle kotakPlayer = new Rectangle(p.X - 50, p.Y, 50, 50);
                Rectangle kotakBoss = new Rectangle(xBoss, yBoss, 50, 50);
                if (kotakPlayer.IntersectsWith(kotakBoss)){
                    hpBoss -= serang;
                    if (hpBoss <= 0) { tambahExp(); }
                }
            }
            else if (arahjalan == 2)
            {
                //bawah
                Rectangle kotakPlayer = new Rectangle(p.X, p.Y + 50, 50, 50);
                Rectangle kotakBoss = new Rectangle(xBoss, yBoss, 50, 50);
                if (kotakPlayer.IntersectsWith(kotakBoss)){
                    hpBoss -= serang;
                    if (hpBoss <= 0) { tambahExp(); }
                }
            }
            else if (arahjalan == 3)
            {
                //atas
                Rectangle kotakPlayer = new Rectangle(p.X, p.Y - 50, 50, 50);
                Rectangle kotakBoss = new Rectangle(xBoss, yBoss, 50, 50);
                if (kotakPlayer.IntersectsWith(kotakBoss)){
                    hpBoss -= serang;
                    if (hpBoss <= 0) { tambahExp(); }
                }
            }
            lblHpBoss.Text = hpBoss + "";
        }

        public void tambahExp()
        {
            if (stage == "easy") { player[job].exp += 50; gold += 300; }
            else if (stage == "medium") { player[job].exp += 100; gold += 600; }
            else if (stage == "hard") { player[job].exp += 150; gold += 900; }

            if (player[job].exp >= player[job].batasExp)
            {
                player[job].exp -= player[job].batasExp;
                player[job].batasExp += (player[job].batasExp / 5);
                player[job].lvl += 1;
                player[job].hpbase += 1;
                player[job].mpbase += 1;
            }
            if (player[job].lvl >= 10 && player[job].lvl < 20)
            {
                //MessageBox.Show("evo 2");
                if (job == 0) { imgs = Image.FromFile("warrior2.png") as Bitmap; player[job].skill = "skill1"; }
                else if (job == 1) { imgs = Image.FromFile("mage11.png") as Bitmap; player[job].skill = "skill1"; }
                else if (job == 2) { imgs = Image.FromFile("archer21.png") as Bitmap; player[job].skill = "skill1"; }
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
                lblPlayer.Image = img;
                player[job].hpbase *= 2;
                player[job].mpbase *= 2;

            }
            else if (player[job].lvl >= 20)
            {
                //MessageBox.Show("evo 3");
                if (job == 0) { imgs = Image.FromFile("warrior31.png") as Bitmap; player[job].skill = "skill2"; }
                else if (job == 1) { imgs = Image.FromFile("mage21.png") as Bitmap; player[job].skill = "skill2"; }
                else if (job == 2) { imgs = Image.FromFile("archer31.png") as Bitmap; player[job].skill = "skill2"; }
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
                lblPlayer.Image = img;
                player[job].hpbase *= 2;
                player[job].mpbase *= 2;
            }
        }

        public void loadData()
        {
            StreamReader sr = new StreamReader(Application.StartupPath + "/player.txt");
            string line = sr.ReadLine();
            string[] temp = line.Split('-');
            
            int arahjalan = Convert.ToInt32(temp[3]);
            gold = Convert.ToInt32(temp[4]);
            sr.Close();

            XmlSerializer serializer = new XmlSerializer(typeof(List<inventory>));
            using (FileStream stream = File.OpenRead("inven.xml"))
            {
                weapon = (List<inventory>)serializer.Deserialize(stream);
            }

            serializer = new XmlSerializer(typeof(List<Item>));
            using (FileStream stream = File.OpenRead("item.xml"))
            {
                listItem = (List<Item>)serializer.Deserialize(stream);
            }

            sr = new StreamReader(Application.StartupPath + "/player.txt");
            line = sr.ReadLine();
            temp = line.Split('-');
            job = Convert.ToInt32(temp[2]);
            sr.Close();

            sr = new StreamReader(Application.StartupPath + "/playerInfo.txt");
            int index = 0;
            while ((line = sr.ReadLine()) != null)
            {
                temp = line.Split('-');
                player[index].hp = Convert.ToInt32(temp[0]);
                player[index].mp = Convert.ToInt32(temp[1]);
                player[index].lvl = Convert.ToInt32(temp[2]);
                player[index].skill = temp[3];
                player[index].exp = Convert.ToInt32(temp[4]);
                player[index].hpbase = Convert.ToInt32(temp[5]);
                player[index].mpbase = Convert.ToInt32(temp[6]);
                player[index].arrow = Convert.ToInt32(temp[7]);
                index++;
            }
            sr.Close();
        }

        public void saveData()
        {
            //save posisi dan last job
            StreamWriter sw = new StreamWriter(Application.StartupPath + "/player.txt");
            sw.Write(((Form1)MdiParent).pX + "-" + ((Form1)MdiParent).pY + "-" + job + "-" + arahjalan + "-" + gold);
            sw.Close();

            System.IO.File.WriteAllText("inven.xml", string.Empty);
            Stream stream = File.OpenWrite(Environment.CurrentDirectory + "\\inven.xml");
            XmlSerializer xmlser = new XmlSerializer(typeof(List<inventory>));
            xmlser.Serialize(stream, weapon);
            stream.Close();

            System.IO.File.WriteAllText("item.xml", string.Empty);
            stream = File.OpenWrite(Environment.CurrentDirectory + "\\item.xml");
            xmlser = new XmlSerializer(typeof(List<Item>));
            xmlser.Serialize(stream, listItem);
            stream.Close();

            // save info dari karakter spt hp, mp dkk
            sw = new StreamWriter(Application.StartupPath + "/playerInfo.txt");
            for (int i = 0; i < 3; i++)
            {
                sw.WriteLine(player[i].hp + "-" + player[i].mp + "-" + player[i].lvl + "-" + player[i].skill + "-" + player[i].exp + "-" + player[i].hpbase + "-" + player[i].mpbase + "-" + player[i].arrow);
            }
            sw.Close();
        }

    }
}
public class player
{
    int x;
    int y;
    Image b;
    //Image[,] serang = new Image[4, 3];
    public player()
    {
        this.x = 50;
        this.y = 350;
    }
    public int X { get => x; set => x = value; }
    public int Y { get => y; set => y = value; }
    public Image B { get => b; set => b = value; }
}
