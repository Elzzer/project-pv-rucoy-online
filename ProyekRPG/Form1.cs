﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ProyekRPG;
using System.Xml.Serialization;

namespace ProyekRPG
{
    public partial class Form1 : Form
    {
        MenuUtama menuutama;
        Map2 map2;
        public int pX = 200, pY = 250;
        public int map = 2;
        int job = 1;
        public string stage = "";

        public int gold = 5000;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            //MessageBox.Show("X: " + e.X + "\nY : " + e.Y);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            menuutama = new MenuUtama();
            menuutama.MdiParent = this;
            menuutama.Show();
            //menuutama.Size = new Size(1500, 800);
            //menuutama.FormBorderStyle = FormBorderStyle.None;
            //menuutama.WindowState = FormWindowState.Maximized;

            this.FormBorderStyle = FormBorderStyle.None;
        }

        public void loadMap2() { //17x13
            map2 = new Map2();
            map2.MdiParent = this;
            map2.Show();
            map2.StartPosition = FormStartPosition.CenterScreen;
        }
        public void oi() {
            MessageBox.Show("Test");
        }
        public void loadGame()
        {
            List<character> player = new List<character>();
            player.Add(new warrior());
            player.Add(new mage());
            player.Add(new archer());

            List<inventory> weapon = new List<inventory>();
            List<Item> listItem = new List<Item>();
            //load posisi dan last job
            StreamReader sr = new StreamReader(Application.StartupPath + "/player.txt");
            string line = sr.ReadLine();
            string[] temp = line.Split('-');

            pX = Convert.ToInt32(temp[0]);
            pY = Convert.ToInt32(temp[1]);
            int job = Convert.ToInt32(temp[2]);
            int arahjalan = Convert.ToInt32(temp[3]);
            gold = Convert.ToInt32(temp[4]);
            sr.Close();

            //load info character

            sr = new StreamReader(Application.StartupPath + "/playerInfo.txt");
            int index = 0;
            while ((line = sr.ReadLine()) != null)
            {
                temp = line.Split('-');
                player[index].hp = Convert.ToInt32(temp[0]);
                player[index].mp = Convert.ToInt32(temp[1]);
                player[index].lvl = Convert.ToInt32(temp[2]);
                player[index].skill = temp[3];
                player[index].exp = Convert.ToInt32(temp[4]);
                player[index].hpbase = Convert.ToInt32(temp[5]);
                player[index].mpbase = Convert.ToInt32(temp[6]);
                player[index].arrow = Convert.ToInt32(temp[7]);

                index++;
            }
            sr.Close();

            //sr = new StreamReader(Application.StartupPath + "/inven.txt");
            //while ((line = sr.ReadLine()) != null)
            //{
            //    temp = line.Split('-');
            //    weapon.Add(new inventory(Convert.ToInt32(temp[1]), Convert.ToInt32(temp[0]), Convert.ToInt32(temp[2])));
            //}
            //sr.Close();
            XmlSerializer serializer = new XmlSerializer(typeof(List<inventory>));
            using (FileStream stream = File.OpenRead("inven.xml"))
            {
                weapon = (List<inventory>)serializer.Deserialize(stream);
            }


            serializer = new XmlSerializer(typeof(List<Item>));
            using (FileStream stream = File.OpenRead("item.xml"))
            {
                listItem = (List<Item>)serializer.Deserialize(stream);
            }

            map2 = new Map2(player, weapon, job, arahjalan, listItem);
            map2.MdiParent = this;
            map2.Show();
            map2.StartPosition = FormStartPosition.CenterScreen;
        }

        private void fontDialog1_Apply(object sender, EventArgs e)
        {

        }

        public void masukdungeon() {
            formdungeon fd = new formdungeon();
            fd.MdiParent = this;
            fd.Show();
            fd.Size = new Size(800, 600);
            fd.WindowState = FormWindowState.Maximized;
            fd.Location = new Point(300, 100);
            fd.FormBorderStyle = FormBorderStyle.None;
        }

        public void OpenBoss() {
            FormBoss fb = new FormBoss();
            fb.MdiParent = this;
            fb.Show();
        }

    }
}
