﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyekRPG
{
    public partial class MenuUtama : Form
    {
        int pilihMenu = 0;
        public MenuUtama()
        {
            InitializeComponent();
        }

        private void MenuUtama_Load(object sender, EventArgs e)
        {
            SoundPlayer soundPlayer = new SoundPlayer(Application.StartupPath + @"\Theme1.wav");
            soundPlayer.Play();

            this.BackgroundImage = Image.FromFile("rucoy start.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
            panel1.Size = new Size(320, 80);
            panel1.Location = new Point(615, 480);
            panel1.BackColor = Color.Transparent;


            panel2.Size = new Size(320, 80);
            panel2.Location = new Point(615, 590);
            panel2.BackColor = Color.Transparent;
        }

        private void panel1_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 1)
            {
                pilihMenu = 1;
                this.Invalidate();
            }
        }

        private void panel2_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 2)
            {
                pilihMenu = 2;
                this.Invalidate();
            }
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("LOAD GAME BOZKU!!!");
            ((Form1)MdiParent).loadGame();
            this.Close();
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            ((Form1)MdiParent).loadMap2();
            this.Close();
        }

        private void MenuUtama_MouseHover(object sender, EventArgs e)
        {
            if (pilihMenu != 0)
            {
                pilihMenu = 0;
                this.Invalidate();
            }
        }

        private void MenuUtama_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush brush = new SolidBrush(Color.FromArgb(64, Color.Gray));
            if (pilihMenu == 1) g.FillRectangle(brush, 620, 485, 310, 75);
            else if (pilihMenu == 2) g.FillRectangle(brush, 620, 597, 310, 75);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
