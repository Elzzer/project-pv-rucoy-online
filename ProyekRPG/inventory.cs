﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyekRPG
{
    public class inventory
    {
        public int jenis { get; set; }
        public int att { get; set; }
        public int plusUpgrade { get; set; }
        public bool isEquip { get; set; }
        public inventory()
        {

        }
        public inventory(int jenis, int att, int plusUpgrade)
        {
            this.jenis = jenis;
            this.att = att;
            this.plusUpgrade = plusUpgrade;
            this.isEquip = false;
        }
    }
}
