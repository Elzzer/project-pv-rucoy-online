﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ProyekRPG;
using System.Drawing.Imaging;
using System.Xml.Serialization;
using System.Reflection;
using System.Media;

namespace ProyekRPG
{
    public partial class Map2 : Form
    {
        Random rand = new Random();
        Image imgs = Image.FromFile("character1.png") as Bitmap;
        Image[,] character;
        Image[,,] animationAttack;
        Image img, borderJob=Image.FromFile("borderjob.png"), borderChar=Image.FromFile("borderchar.png");

        Image imgAtt; //temp buat animasi serang
        int tempanimasi = 0; //buat timer animasi

        //img inven
        Image bgInven = Image.FromFile("kotakinven1.png");
        Image[] cropWeapon = new Image[3];
        List<inventory> weapon = new List<inventory>();
        List<inventory> listShop = new List<inventory>(); //list untuk shop
        int clickWeaponY = -1,clickWeaponX = -1;

        int cekBlacksmith = 0; //guna untuk memunculkan drawstring blacksmith dan listshop
        
        bool bolehjalan = true, bolehSerang = true;
        int tempjalan = 0; int arahjalan = 2; int gold = 5000;

        List<character> player = new List<character>();
        Panel infoCharacter, panelInven,panelShop,panelDifficulty;
        int job = 0;
        Label lblPlayer; // label buat cetak gambar player

        bool isPaused = false; //buat pause saat inventory

        List<Item> listItem = new List<Item>();
        int indexShop = -2, indexItemShop = -1, banyakItem = 0; bool cetakItemShop = false;
        List<Image> inputItem = new List<Image>();
        Button upItem, downItem;
        Label TextHoverItem;
        Label[] HoverDifficulty = new Label[3];

        public Map2()
        {
            InitializeComponent();
            player.Add(new warrior());
            player.Add(new mage());
            player.Add(new archer());
            
            weapon.Add(new inventory(0, 10, 0)); weapon[0].isEquip = true;
            weapon.Add(new inventory(1, 10, 0)); weapon[1].isEquip = true;
            weapon.Add(new inventory(2, 10, 0)); weapon[2].isEquip = true;
            //panel1.BackColor = Color.Transparent;
        }
        public Map2(List<character> player, List<inventory> weapon, int job, int arahjalan, List<Item> listItem)
        {
            InitializeComponent();
            this.player = player;
            this.weapon = weapon;
            this.job = job;
            this.arahjalan = arahjalan;
            this.listItem = listItem;
        }

        private void Map2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!isPaused)
            {
                //MAP002
                if (e.KeyChar == 'd' && ((Form1)MdiParent).pX < 800 && bolehjalan)
                {
                    //img = character[2, 0];
                    //((Form1)MdiParent).pX += 50;
                    //if (((Form1)MdiParent).pX == 200 && ((Form1)MdiParent).pY == 100) ((Form1)MdiParent).pX -= 50;
                    //if (((Form1)MdiParent).pX == 400 && ((Form1)MdiParent).pY == 0) ((Form1)MdiParent).pX -= 50;
                    //if (((Form1)MdiParent).pX == 700) ((Form1)MdiParent).pX -= 50;
                    //if (((Form1)MdiParent).pX == 400 && ((Form1)MdiParent).pY == 400) ((Form1)MdiParent).pX -= 50;
                    //if (((Form1)MdiParent).pX == 450 && ((Form1)MdiParent).pY == 450) ((Form1)MdiParent).pX -= 50;
                    img = character[2, 0];
                    tempjalan = 0; arahjalan = 0;
                    bolehjalan = false;
                    timer1.Start();
                }
                else if (e.KeyChar == 'a' && ((Form1)MdiParent).pX > 0 && bolehjalan)
                {
                    //img = character[1, 0];
                    //((Form1)MdiParent).pX -= 50;
                    //if (((Form1)MdiParent).pX == 200 && (((Form1)MdiParent).pY == 0 || ((Form1)MdiParent).pY == 100)) ((Form1)MdiParent).pX += 50;
                    //if (((Form1)MdiParent).pX == 100 && ((Form1)MdiParent).pY == 50) ((Form1)MdiParent).pX += 50;
                    //if (((Form1)MdiParent).pY >= 150 && ((Form1)MdiParent).pY <= 450 && ((Form1)MdiParent).pX == 600) ((Form1)MdiParent).pX += 50;
                    //if (((Form1)MdiParent).pY >= 400 && ((Form1)MdiParent).pY <= 500 && ((Form1)MdiParent).pX == 200) ((Form1)MdiParent).pX += 50;
                    img = character[1, 0];
                    tempjalan = 0; arahjalan = 1;
                    bolehjalan = false;
                    timer1.Start();
                }
                //MAP005
                else if (e.KeyChar == 's' && ((Form1)MdiParent).pY == 400 && ((Form1)MdiParent).pX >= 200 && ((Form1)MdiParent).pX <= 300 && bolehjalan && ((Form1)MdiParent).map == 5)//Ke map 1
                {
                    img = character[0, 0];
                    tempjalan = 0; arahjalan = 2;
                    panel1.BackgroundImage = Image.FromFile("Map001.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pY = 250;
                    ((Form1)MdiParent).pX = 450;
                    ((Form1)MdiParent).map = 1;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }
                //Map006
                else if (e.KeyChar == 's' && ((Form1)MdiParent).pY >= 600 && ((Form1)MdiParent).pX == 400 && bolehjalan && ((Form1)MdiParent).map == 6)//Ke map 3
                {
                    img = character[0, 0];
                    tempjalan = 0; arahjalan = 2;
                    panel1.BackgroundImage = Image.FromFile("Map003.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pY = 350;
                    ((Form1)MdiParent).pX = 150;
                    ((Form1)MdiParent).map = 3;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }
                //Jalan biasa
                else if (e.KeyChar == 's' && ((Form1)MdiParent).pY < 600 && bolehjalan)
                {
                    //img = character[0, 0];
                    //((Form1)MdiParent).pY += 50;
                    //if (((Form1)MdiParent).pX >= 0 && ((Form1)MdiParent).pX <= 200 && ((Form1)MdiParent).pX != 150) ((Form1)MdiParent).pY -= 50;
                    //if (((Form1)MdiParent).pX == 150 && ((Form1)MdiParent).pY == 150) ((Form1)MdiParent).pY -= 50;
                    //if (((Form1)MdiParent).pX != 650 && ((Form1)MdiParent).pY == 150) ((Form1)MdiParent).pY -= 50;
                    //if (((Form1)MdiParent).pY > 500) ((Form1)MdiParent).pY -= 50;
                    img = character[0, 0];
                    tempjalan = 0; arahjalan = 2;
                    bolehjalan = false;
                    timer1.Start();
                }
                //Map001
                else if (e.KeyChar == 'w' && ((Form1)MdiParent).pY == 250 && ((Form1)MdiParent).pX == 450 && bolehjalan && ((Form1)MdiParent).map == 1)//Ke map 5
                {
                    img = character[3, 0];
                    tempjalan = 0; arahjalan = 3;
                    panel1.BackgroundImage = Image.FromFile("Map005.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pY = 400;
                    ((Form1)MdiParent).pX = 250;
                    ((Form1)MdiParent).map = 5;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }
                //Map003
                else if (e.KeyChar == 'w' && ((Form1)MdiParent).pY == 350 && ((Form1)MdiParent).pX == 150 && bolehjalan && ((Form1)MdiParent).map == 3) //Ke map 1
                {
                    img = character[3, 0];
                    tempjalan = 0; arahjalan = 3;
                    panel1.BackgroundImage = Image.FromFile("Map006.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pY = 600;
                    ((Form1)MdiParent).pX = 400;
                    ((Form1)MdiParent).map = 6;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }

                else if (e.KeyChar == 'w' && ((Form1)MdiParent).pY > 0 && bolehjalan && !(((Form1)MdiParent).map == 1 && (((Form1)MdiParent).pX == 150 && ((Form1)MdiParent).pY == 250)))
                {

                    //img = character[3, 0];
                    //((Form1)MdiParent).pY -= 50;
                    //if (((Form1)MdiParent).pX >= 0 && ((Form1)MdiParent).pX <= 200 && ((Form1)MdiParent).pX != 150) ((Form1)MdiParent).pY += 50;
                    //if (((Form1)MdiParent).pX == 150 && ((Form1)MdiParent).pY == 0) ((Form1)MdiParent).pY += 50;
                    //if (((Form1)MdiParent).pX >= 400 && ((Form1)MdiParent).pX <= 650 && ((Form1)MdiParent).pY == 0) ((Form1)MdiParent).pY += 50;
                    //if (((Form1)MdiParent).pX >= 450 && ((Form1)MdiParent).pX <= 600 && ((Form1)MdiParent).pY == 450) ((Form1)MdiParent).pY += 50;
                    //if (((Form1)MdiParent).pX == 400 && ((Form1)MdiParent).pY == 400) ((Form1)MdiParent).pY += 50;
                    //if (((Form1)MdiParent).pX >= 250 && ((Form1)MdiParent).pX <= 350 && ((Form1)MdiParent).pY == 350) ((Form1)MdiParent).pY += 50;
                    img = character[3, 0];
                    tempjalan = 0; arahjalan = 3;
                    bolehjalan = false;
                    timer1.Start();

                }
                //MAP001
                else if (e.KeyChar == 'd' && ((Form1)MdiParent).pX >= 800 && bolehjalan && ((Form1)MdiParent).map == 1) //Ke map 2
                {
                    img = character[2, 0];
                    tempjalan = 0; arahjalan = 0;
                    panel1.BackgroundImage = Image.FromFile("Map002.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pX = 0;
                    ((Form1)MdiParent).map = 2;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }
                else if (e.KeyChar == 's' && ((Form1)MdiParent).pY >= 600 && bolehjalan && ((Form1)MdiParent).map == 1) //Ke map 3
                {
                    img = character[0, 0];
                    tempjalan = 0; arahjalan = 2;
                    panel1.BackgroundImage = Image.FromFile("Map003.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pY = 0;
                    ((Form1)MdiParent).map = 3;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }
                else if (e.KeyChar == 'w' && ((Form1)MdiParent).pY == 250 && ((Form1)MdiParent).pX == 150 && bolehjalan && ((Form1)MdiParent).map == 1)//Ke map 4
                {

                    img = character[3, 0];
                    tempjalan = 0; arahjalan = 3;
                    panel1.BackgroundImage = Image.FromFile("Map004.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pY = 600;
                    ((Form1)MdiParent).pX += 200;
                    ((Form1)MdiParent).map = 4;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }
                //MAP002
                else if (e.KeyChar == 'a' && ((Form1)MdiParent).pX <= 0 && bolehjalan && ((Form1)MdiParent).map == 2) //Ke map 1
                {
                    img = character[1, 0];
                    tempjalan = 0; arahjalan = 1;
                    panel1.BackgroundImage = Image.FromFile("Map001.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pX = 800;
                    ((Form1)MdiParent).map = 1;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }
                //MAP003
                else if (e.KeyChar == 'w' && ((Form1)MdiParent).pY <= 0 && bolehjalan && ((Form1)MdiParent).map == 3) //Ke map 1
                {
                    img = character[3, 0];
                    tempjalan = 0; arahjalan = 3;
                    panel1.BackgroundImage = Image.FromFile("Map001.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pY = 600;
                    ((Form1)MdiParent).map = 1;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }
                //MAP004
                else if (e.KeyChar == 's' && ((Form1)MdiParent).pY >= 600 && bolehjalan && ((Form1)MdiParent).map == 4) //Ke map 1
                {
                    img = character[0, 0];
                    tempjalan = 0; arahjalan = 2;
                    panel1.BackgroundImage = Image.FromFile("Map001.png");
                    panel1.BackgroundImageLayout = ImageLayout.Stretch;
                    ((Form1)MdiParent).pY = 250;
                    ((Form1)MdiParent).pX = 150;
                    ((Form1)MdiParent).map = 1;
                    lblPlayer.Image = img;
                    lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                }
                //USE ITEM
                else if (e.KeyChar == '1')
                {
                    if (listItem[0].banyak > 0)
                    {
                        listItem[0].banyak--;
                        player[job].hp += (player[job].hpbase * 10 / 100);
                        if (player[job].hp > player[job].hpbase) player[job].hp = player[job].hpbase;
                        panel1.Invalidate();
                    }
                }
                else if (e.KeyChar == '2')
                {
                    if (listItem[1].banyak > 0)
                    {
                        listItem[1].banyak--;
                        player[job].hp = player[job].hpbase;
                        panel1.Invalidate();
                    }
                }
                else if (e.KeyChar == '3')
                {
                    if (listItem[3].banyak > 0)
                    {
                        listItem[3].banyak--;
                        player[job].mp += (player[job].mpbase * 10 / 100);
                        if (player[job].mp > player[job].mpbase) player[job].mp = player[job].mpbase;
                        panel1.Invalidate();
                    }
                }
                else if (e.KeyChar == '4' && listItem[4].banyak > 0) listItem[4].banyak--;
                else if (e.KeyChar == '5' && listItem[5].banyak > 0) listItem[5].banyak--;
                else if (e.KeyChar == '6' && job == 2) {
                    if (listItem[6].banyak > 0)
                    {
                        //MessageBox.Show("Test");
                        listItem[6].banyak--;
                        player[job].arrow += 10;
                    }
                }
                //DUNGEON
                else if (e.KeyChar == ' ')
                {
                    clickWeaponX = -1; clickWeaponY = -1;
                    if (((Form1)MdiParent).map == 2 && ((Form1)MdiParent).pX == 300 && ((Form1)MdiParent).pY == 400 && arahjalan == 3)
                    {
                        panelDifficulty.Visible = true;
                        panel1.BackColor = Color.Transparent;
                        panel1.BackgroundImage = SetImageOpacity(Image.FromFile("Map00" + ((Form1)MdiParent).map + ".png"), 0.25F);
                        lblPlayer.BackColor = Color.Transparent;
                        lblPlayer.Image = SetImageOpacity(img, 0.5F);
                        isPaused = true;
                    }
                    else if (((Form1)MdiParent).map == 5 && ((Form1)MdiParent).pX == 250 && ((Form1)MdiParent).pY == 200)
                    {
                        //MessageBox.Show("Masuk Blacksmith");
                        panelInven.Visible = true;
                        cekBlacksmith = 1;

                        panel1.BackColor = Color.Transparent;
                        panel1.BackgroundImage = SetImageOpacity(Image.FromFile("Map00" + ((Form1)MdiParent).map + ".png"), 0.25F);
                        lblPlayer.BackColor = Color.Transparent;
                        lblPlayer.Image = SetImageOpacity(img, 0.5F);
                        panelInven.BackColor = Color.FromArgb(150, Color.MintCream);
                        isPaused = true;
                    }
                    else if (((((Form1)MdiParent).pX == 150 || ((Form1)MdiParent).pX == 100) && ((Form1)MdiParent).pY == 300))
                    {
                        if (((Form1)MdiParent).map == 4 && arahjalan == 3)
                        {
                            string halo = DateTime.Now.ToString("h:mm:ss");
                            MessageBox.Show(halo);
                            MessageBox.Show("SAVE!!!");
                            //save posisi dan last job
                            StreamWriter sw = new StreamWriter(Application.StartupPath + "/player.txt");
                            sw.Write(((Form1)MdiParent).pX + "-" + ((Form1)MdiParent).pY + "-" + job + "-" + arahjalan + "-" + gold + "-" + halo);
                            sw.Close();

                            // save info dari karakter spt hp, mp dkk
                            sw = new StreamWriter(Application.StartupPath + "/playerInfo.txt");
                            for (int i = 0; i < 3; i++)
                            {
                                sw.WriteLine(player[i].hp + "-" + player[i].mp + "-" + player[i].lvl + "-" + player[i].skill + "-" + player[i].exp + "-" + player[i].hpbase + "-" + player[i].mpbase + "-" + player[i].arrow);
                            }
                            sw.Close();

                            ////save inventory
                            //sw = new StreamWriter(Application.StartupPath + "/inven.txt");
                            //for (int i = 0; i < weapon.Count; i++)
                            //{
                            //    sw.WriteLine(weapon[i].att + "-" + weapon[i].jenis + "-" + weapon[i].plusUpgrade);
                            //}
                            //sw.Close();

                            System.IO.File.WriteAllText("inven.xml", string.Empty);
                            Stream stream = File.OpenWrite(Environment.CurrentDirectory + "\\inven.xml");
                            XmlSerializer xmlser = new XmlSerializer(typeof(List<inventory>));
                            xmlser.Serialize(stream, weapon);
                            stream.Close();

                            System.IO.File.WriteAllText("item.xml", string.Empty);
                            stream = File.OpenWrite(Environment.CurrentDirectory + "\\item.xml");
                            xmlser = new XmlSerializer(typeof(List<Item>));
                            xmlser.Serialize(stream, listItem);
                            stream.Close();

                            //((Form1)MdiParent).masukdungeon();
                            //this.Close();
                        }
                    }
                    else if (((Form1)MdiParent).pX == 400 && ((Form1)MdiParent).pY == 350 && ((Form1)MdiParent).map == 6)
                    {
                        isPaused = true;
                        panelShop.Size = new Size(170, 130);
                        panelShop.Visible = true;
                        panelShop.BackColor = Color.FromArgb(150, Color.MintCream);
                        isPaused = true;
                        panel1.BackColor = Color.Transparent;
                        panel1.BackgroundImage = SetImageOpacity(Image.FromFile("Map00" + ((Form1)MdiParent).map + ".png"), 0.25F);
                        lblPlayer.BackColor = Color.Transparent;
                        lblPlayer.Image = SetImageOpacity(img, 0.5F);
                    }
                }
                //panel1.Invalidate();
            }
            if (e.KeyChar == 's' && panelShop.Visible == true)
            {
                indexShop++;
                if (!cetakItemShop)
                {
                    if (indexShop > 2) indexShop = 2;
                    if (indexShop < 0) indexShop = 0;
                }
                else
                {
                    if (indexShop > 16) indexShop = 16;
                    if (indexShop < 10) indexShop = 10;
                }
                panelShop.Invalidate();
            }
            else if (e.KeyChar == 'w' && panelShop.Visible == true)
            {
                indexShop--;
                if (indexShop < 0 && !cetakItemShop) indexShop = 0;
                else if (cetakItemShop && indexShop < 10) indexShop = 10;
                panelShop.Invalidate();
            }
            else if (e.KeyChar == ' ' && indexItemShop != -1) {
                listItem[indexItemShop].banyak += banyakItem;
                gold -= banyakItem * listItem[indexItemShop].harga;
                if (indexItemShop == 2) {
                    for (int i = 0; i < banyakItem; i++)
                    {
                        player[job].hpbase += (player[job].hpbase * 5 / 100);
                    }
                }
                panel1.Invalidate();
                MessageBox.Show("Item Purchased!!!");
            }
            else if (e.KeyChar == ' ' && panelShop.Visible == true && indexShop != -2)
            {
                panelShop.Visible = false;
                if (indexShop == 0)
                {
                    panelInven.Visible = true;
                    cekBlacksmith = 2;
                    panelInven.BackColor = Color.FromArgb(150, Color.MintCream);
                }
                else if (indexShop == 1)
                {
                    //MessageBox.Show("Beli item!!!");
                    cetakItemShop = true;
                    indexShop = 10;
                    panelShop.Size = new Size(170, 240);
                    panelShop.Visible = false;
                    panelShop.Visible = true;
                }
                else if (indexShop == 2)
                {
                    MessageBox.Show("Sell!!!");
                    panelInven.Visible = true;
                    cekBlacksmith = 3;
                    panelInven.BackColor = Color.FromArgb(150, Color.MintCream);
                }
                else
                {
                    //buat beli item item
                    indexItemShop = indexShop - 10;
                    panelShop.Size = new Size(200, 60);
                    panelShop.Visible = false;
                    panelShop.Visible = true;
                    upItem.Visible = true;
                    downItem.Visible = true;
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush b = new SolidBrush(Color.Black);
            Brush bb = new SolidBrush(Color.White);
            Font f = new Font("Arial", 15, FontStyle.Bold);
            //Image img = Image.FromFile("pacman.png");
            //img.PixelFormat = System.Drawing.Imaging.PixelFormat
            //g.DrawImage(img, ((Form1)MdiParent).pX, ((Form1)MdiParent).pY, 50, 50);
            if (!bolehSerang)
            {
                int gerakBow = 0;
                if (job == 2) gerakBow = tempanimasi * 5;
                if (arahjalan == 0) g.DrawImage(imgAtt, ((Form1)MdiParent).pX + 30 + gerakBow, ((Form1)MdiParent).pY, 50, 50);
                else if (arahjalan == 1) g.DrawImage(imgAtt, ((Form1)MdiParent).pX - 30 - gerakBow, ((Form1)MdiParent).pY, 50, 50);
                else if (arahjalan == 2) g.DrawImage(imgAtt, ((Form1)MdiParent).pX, ((Form1)MdiParent).pY + 30 + gerakBow, 50, 50);
                else if (arahjalan == 3) g.DrawImage(imgAtt, ((Form1)MdiParent).pX, ((Form1)MdiParent).pY - 30 - gerakBow, 50, 50);
            }
            g.FillRectangle(b, 695, 0, 250, 75);
            g.DrawString("HP    : " + player[job].hp, f, bb, 700, 10);
            g.DrawString("MP    : " + player[job].mp, f, bb, 700, 30);
            g.DrawString("Gold : " + gold + " Dz",f,bb,700,50);
            g.DrawImage(inputItem[0], 740, 520, 20, 20); g.DrawString(" " + listItem[0].banyak, new Font("arial", 10), b, 740, 560); g.DrawImage(inputItem[6], 742, 545, 15, 15);
            g.DrawImage(inputItem[1], 770, 520, 20, 20); g.DrawString(" " + listItem[1].banyak, new Font("arial", 10), b, 770, 560); g.DrawImage(inputItem[7], 770, 540, 20, 20);
            g.DrawImage(inputItem[2], 800, 520, 20, 20); g.DrawString(" " + listItem[3].banyak, new Font("arial", 10), b, 800, 560); g.DrawImage(inputItem[8], 802, 545, 15, 15);
            g.DrawImage(inputItem[3], 740, 580, 20, 20); g.DrawString(" " + listItem[4].banyak, new Font("arial", 10), b, 740, 620); g.DrawImage(inputItem[9], 740, 600, 20, 20);
            g.DrawImage(inputItem[4], 770, 580, 20, 20); g.DrawString(" " + listItem[5].banyak, new Font("arial", 10), b, 770, 620); g.DrawImage(inputItem[10], 770, 600, 20, 20);
            g.DrawImage(inputItem[5], 800, 580, 20, 20); g.DrawString(" " + listItem[6].banyak, new Font("arial", 10), b, 800, 620); g.DrawImage(inputItem[11], 800, 600, 20, 20);
        }

        public void cropImage() {

            character = new Image[4, 3];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    //MessageBox.Show(i + " " + j);
                    Rectangle r = new Rectangle(j * 47, i * 48, 47, 48);
                    Bitmap bmp = new Bitmap(imgs);
                    character[i, j] = bmp.Clone(r, bmp.PixelFormat);
                }
            }
            if (arahjalan == 0) img = character[2, 0];
            else if (arahjalan == 1) img = character[1, 0];
            else if (arahjalan == 2) img = character[0, 0];
            else if (arahjalan == 3) img = character[3, 0];
            
        }
        public void cropAttack() {
            //potong gambar buat animasi pedang
            animationAttack = new Image[3, 4, 3];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i <= 1)
                    {
                        Rectangle r = new Rectangle((i * 360) + (j * 180), 180, 180, 180);
                        Bitmap bmp = new Bitmap(Image.FromFile("swordslash.png"));
                        animationAttack[0, i, j] = bmp.Clone(r, bmp.PixelFormat);
                    }
                    else if (i == 2)
                    {
                        Rectangle r = new Rectangle(j * 180, 360, 180, 180);
                        Bitmap bmp = new Bitmap(Image.FromFile("swordslash.png"));
                        animationAttack[0, i, j] = bmp.Clone(r, bmp.PixelFormat);
                    }
                    else if (i == 3)
                    {
                        Rectangle r = new Rectangle(j * 180 + 360, 540, 180, 180);
                        Bitmap bmp = new Bitmap(Image.FromFile("swordslash.png"));
                        animationAttack[0, i, j] = bmp.Clone(r, bmp.PixelFormat);
                    }
                }
            }
            for (int i = 0; i < 3; i++)
            {
                Rectangle r = new Rectangle(i * 192 + 192, 576, 192, 192);
                Bitmap bmp = new Bitmap(Image.FromFile("magethunder.png"));
                for (int j = 0; j < 4; j++)
                {
                    animationAttack[1, j, i] = bmp.Clone(r, bmp.PixelFormat);
                }
            }
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i == 0) animationAttack[2, i, j] = Image.FromFile((j + 1) + " kanan.png");
                    else if (i == 1) animationAttack[2, i, j] = Image.FromFile((j + 1) + " kiri.png");
                    else if (i == 2) animationAttack[2, i, j] = Image.FromFile((j + 1) + " bwh.png");
                    else if (i == 3) animationAttack[2, i, j] = Image.FromFile((j + 1) + " atas.png");
                }
            }
        }

        private void Map2_Load(object sender, EventArgs e)
        {
            SoundPlayer soundPlayer = new SoundPlayer(Application.StartupPath + @"\Town1.wav");
            soundPlayer.Play();

            gold = ((Form1)MdiParent).gold;

            for (int i = 0; i < player.Count; i++)
            {
                player[i].hp = player[i].hpbase;
                player[i].mp = player[i].mpbase;
            }

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic, null, panel1, new object[] { true });
            
            if (((Form1)MdiParent).map == 2) { ((Form1)MdiParent).pX = 300; ((Form1)MdiParent).pY = 400; arahjalan = 2; } // habis pulang dari lawan boss atau lawan dungeon

            //buat masukkan image icon item
            for (int i = 0; i < 6; i++)
            {
                inputItem.Add(Image.FromFile((1+i) + ".png"));
            }
            inputItem.Add(Image.FromFile("Potion.png"));
            inputItem.Add(Image.FromFile("MaxPotion.png"));
            inputItem.Add(Image.FromFile("Clarity.png"));
            inputItem.Add(Image.FromFile("AttUp.png"));
            inputItem.Add(Image.FromFile("DefenseUp.png"));
            inputItem.Add(Image.FromFile("Arrow.png"));


            refreshShop(); //refresh isi shop
            //awal load map (buat load)
            panel1.Size = new Size(850, 650);
            panel1.BackgroundImage = Image.FromFile("Map00" + ((Form1)MdiParent).map + ".png");
            panel1.Location = new Point(350, 100);
            panel1.BackgroundImageLayout = ImageLayout.Stretch;
            panel1.BringToFront();
            //awal print job (buat load)
            if (job == 0)
            {
                if (player[job].lvl < 10) imgs = Image.FromFile("character1.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("warrior2.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("warrior31.png") as Bitmap;
            }
            else if (job == 1)
            {
                if (player[job].lvl < 10) imgs = Image.FromFile("character2.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("mage11.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("mage21.png") as Bitmap;
            }
            else if (job == 2)
            {
                if (player[job].lvl < 10) imgs = Image.FromFile("character3.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("archer21.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("archer31.png") as Bitmap;
            }
            cropImage(); cropAttack(); panel1.Invalidate();

            infoCharacter = new Panel();
            infoCharacter.Location = new Point(300, 200);
            infoCharacter.Size = new Size(200, 200);
            infoCharacter.Paint += new PaintEventHandler(infoCharacter_paint);
            infoCharacter.MouseClick += new MouseEventHandler(Change_Job);
            panel1.Controls.Add(infoCharacter);
            infoCharacter.Visible = false;

            panelDifficulty = new Panel();
            panelDifficulty.Location = new Point(300, 200);
            panelDifficulty.Size = new Size(250, 200);
            panelDifficulty.BackgroundImage = Image.FromFile("difficultDungeon.png");
            panelDifficulty.MouseHover += new EventHandler(MakeLabelTransparant);
            panelDifficulty.BackgroundImageLayout = ImageLayout.Stretch;
            panel1.Controls.Add(panelDifficulty);
            panelDifficulty.Visible = false;
            for (int i = 0; i < 3; i++)
            {
                HoverDifficulty[i] = new Label();
                if (i == 0)
                {
                    HoverDifficulty[i].Location = new Point(10, 145);
                    HoverDifficulty[i].Size = new Size(60, 30);
                }
                else if (i == 1)
                {
                    HoverDifficulty[i].Location = new Point(80, 145);
                    HoverDifficulty[i].Size = new Size(85, 30);
                }
                else
                {
                    HoverDifficulty[i].Location = new Point(180, 145);
                    HoverDifficulty[i].Size = new Size(60, 30);
                }
                HoverDifficulty[i].MouseHover += new EventHandler(Choose_DifficultDungeon);
                HoverDifficulty[i].MouseClick += new MouseEventHandler(Click_Dungeon);
                HoverDifficulty[i].BackColor = Color.Transparent;
                panelDifficulty.Controls.Add(HoverDifficulty[i]);
            }

            panelInven = new Panel();
            panelInven.Location = new Point(300, 200);
            panelInven.Size = new Size(260, 200);
            panelInven.Paint += new PaintEventHandler(panelInven_paint);
            panelInven.MouseClick += new MouseEventHandler(Click_Weapon);
            panelInven.MouseDoubleClick += new MouseEventHandler(DoubleClick_EquipWeapon);
            panel1.Controls.Add(panelInven);
            panelInven.Visible = false;

            panelShop = new Panel();
            panelShop.Location = new Point(460, 255);
            panelShop.Size = new Size(170, 130);
            panelShop.Paint += new PaintEventHandler(panelShop_paint);
            panel1.Controls.Add(panelShop);
            panelShop.Visible = false;

            Button b = new Button();
            b.Location = new Point(65, 160);
            b.Size = new Size(60, 30);
            b.Text = "Inventory";
            b.Click += new EventHandler(button_Inventory);
            infoCharacter.Controls.Add(b);

            //untuk up down beli item!!!
            upItem = new Button();
            upItem.Location = new Point(122, 0);
            upItem.Size = new Size(40, 20);
            upItem.BackgroundImage = Image.FromFile("upArrow.png");
            upItem.BackgroundImageLayout = ImageLayout.Stretch;
            upItem.FlatStyle = FlatStyle.Popup;
            upItem.BackColor = Color.Transparent;
            upItem.MouseClick += new MouseEventHandler(button_UpItemShop);
            panelShop.Controls.Add(upItem);
            upItem.Visible = false;

            downItem = new Button();
            downItem.Location = new Point(122, 40);
            downItem.Size = new Size(40, 20);
            downItem.BackgroundImage = Image.FromFile("downArrow.png");
            downItem.BackgroundImageLayout = ImageLayout.Stretch;
            downItem.FlatStyle = FlatStyle.Popup;
            downItem.BackColor = Color.Transparent;
            downItem.MouseClick += new MouseEventHandler(button_DownItemShop);
            panelShop.Controls.Add(downItem);
            downItem.Visible = false;

            for (int i = 0; i < 6; i++)
            {
                Image iconItem = Image.FromFile("Potion.png");
                int yPosition = 0;
                if (i < 3) yPosition = 540;
                else yPosition = 600;
                int xPosition = (i % 3) * 30 + 740;
                if (i == 0) iconItem = Image.FromFile("Potion.png");
                else if (i == 1) iconItem = Image.FromFile("MaxPotion.png");
                else if (i == 2) iconItem = Image.FromFile("Clarity.png");
                else if (i == 3) iconItem = Image.FromFile("Potion.png");
                else if (i == 4) iconItem = Image.FromFile("Potion.png");
                else if (i == 5) iconItem = Image.FromFile("Arrow.png");

                Label l = new Label();
                l.Size = new Size(20, 20);
                l.Location = new Point(xPosition, yPosition);
                l.BackColor = Color.Transparent;
                l.SendToBack();
                l.MouseHover += new EventHandler(Hover_Item);
                panel1.Controls.Add(l);
            }
            TextHoverItem = new Label();
            TextHoverItem.Location = new Point(740, 500);
            TextHoverItem.Size = new Size(200, 20);
            TextHoverItem.Text = "";
            TextHoverItem.BackColor = Color.Transparent;
            panel1.Controls.Add(TextHoverItem);

            //character = new Image[4,3];
            //for (int i = 0; i < 4; i++)
            //{
            //    for (int j = 0; j < 3; j++)
            //    {
            //        //MessageBox.Show(i + " " + j);
            //        Rectangle r = new Rectangle(j * 47, i * 48,47,48);
            //        character[i,j] = cropImage(imgs, r);
            //    }
            //}
            //img = character[0, 0];

            //weapon //96x64
            imgs = Image.FromFile("weapon.png") as Bitmap;
            Rectangle r = new Rectangle(198, 64, 96, 64);
            Bitmap bmp = new Bitmap(imgs);
            cropWeapon[0] = bmp.Clone(r, bmp.PixelFormat); // sword
            r = new Rectangle(190, 324, 96, 60);
            bmp = new Bitmap(imgs);
            cropWeapon[1] = bmp.Clone(r, bmp.PixelFormat); //staff
            r = new Rectangle(480, 0, 96, 64);
            bmp = new Bitmap(imgs);
            cropWeapon[2] = bmp.Clone(r, bmp.PixelFormat); // bow
            
            lblPlayer = new Label();
            lblPlayer.Size = new Size(50, 50);
            lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
            this.Controls.Add(lblPlayer);
            lblPlayer.Image = img;
            lblPlayer.BackColor = Color.Transparent;
            lblPlayer.Click += new EventHandler(InfoPanel_ClickLabel);
            lblPlayer.Parent = panel1;

            listItem.Add(new Item("Potion"));
            listItem.Add(new Item("Max Potion"));
            listItem.Add(new Item("Calcium"));
            listItem.Add(new Item("Clarity"));
            listItem.Add(new Item("Attack Boost"));
            listItem.Add(new Item("Defense Boost"));
            listItem.Add(new Item("Arrow"));

        }

        private void Click_Dungeon(object sender, MouseEventArgs e)
        {
            Label a = (Label)sender;
            if (a.Left >= 10 && a.Left <= 70 && a.Top == 145)
            {
                ((Form1)MdiParent).stage = "easy";
                saveData();
                //MessageBox.Show("Masuk Dungeon");
                ((Form1)MdiParent).masukdungeon();
                this.Close();
            }
            else if (a.Left >= 80 && a.Left <= 165 && a.Top == 145)
            {
                ((Form1)MdiParent).stage = "medium";
                saveData();
                //MessageBox.Show("Masuk Dungeon");
                ((Form1)MdiParent).masukdungeon();
                this.Close();
            }
            else if (a.Left >= 180 && a.Left <= 240 && a.Top == 145)
            {
                ((Form1)MdiParent).stage = "hard";
                saveData();
                //MessageBox.Show("Masuk Dungeon");
                ((Form1)MdiParent).masukdungeon();
                this.Close();
            }
        }

        private void MakeLabelTransparant(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
            {
                HoverDifficulty[i].BackColor = Color.Transparent;
            }
        }

        private void Choose_DifficultDungeon(object sender, EventArgs e)
        {
            Label a = (Label)sender;
            if (a.Left >= 10 && a.Left <= 70 && a.Top == 145)
            {
                a.BackColor = Color.FromArgb(150, Color.MintCream);
                HoverDifficulty[1].BackColor = Color.Transparent;
                HoverDifficulty[2].BackColor = Color.Transparent;
            }
            else if (a.Left >= 80 && a.Left <= 165 && a.Top == 145)
            {
                a.BackColor = Color.FromArgb(150, Color.MintCream);
                HoverDifficulty[0].BackColor = Color.Transparent;
                HoverDifficulty[2].BackColor = Color.Transparent;
            }
            else if (a.Left >= 180 && a.Left <= 240 && a.Top == 145)
            {
                a.BackColor = Color.FromArgb(150, Color.MintCream);
                HoverDifficulty[0].BackColor = Color.Transparent;
                HoverDifficulty[1].BackColor = Color.Transparent;
            }
        }

        private void Hover_Item(object sender, EventArgs e)
        {
            Label a = (Label)sender;
            if (a.Top == 540 && a.Left == 740) TextHoverItem.Text = ("HP + 10%");
            else if (a.Top == 540 && a.Left == 770) TextHoverItem.Text = ("HP Fully Restore");
            else if (a.Top == 540 && a.Left == 800) TextHoverItem.Text = ("MP + 10%");
            else if (a.Top == 600 && a.Left == 740) TextHoverItem.Text = ("Attack Boost x 1.5");
            else if (a.Top == 600 && a.Left == 770) TextHoverItem.Text = ("Damage Reduce 50%");
            else if (a.Top == 600 && a.Left == 800) TextHoverItem.Text = ("Arrow Archer +10");
        }

        private void button_DownItemShop(object sender, EventArgs e)
        {
            if (banyakItem > 0) banyakItem--;
            panelShop.Invalidate();
        }

        private void button_UpItemShop(object sender, EventArgs e)
        {
            banyakItem++;
            panelShop.Invalidate();
        }

        private void panelShop_paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Font f = new Font("Monotype Corsiva", 17, FontStyle.Regular);
            Brush b = new SolidBrush(Color.Black);
            if (!cetakItemShop && indexItemShop == -1)
            {
                g.DrawString("Buy Weapon", f, b, 25, 20);
                g.DrawString("Buy Item", f, b, 25, 50);
                g.DrawString("Sell Weapon", f, b, 25, 80);
                Image gbrPanah = SetImageOpacity(Image.FromFile("tunjukMenuShop.png"), 0.5F);
                g.DrawImage(gbrPanah, 5, indexShop * 30 + 22, 25, 25);
            }
            else if (cetakItemShop && indexItemShop == -1)
            {
                g.DrawString("Potion", f, b, 25, 20);
                g.DrawString("Max Potion", f, b, 25, 50);
                g.DrawString("Calcium", f, b, 25, 80);
                g.DrawString("Clarity", f, b, 25, 110);
                g.DrawString("Attack Boost", f, b, 25, 140);
                g.DrawString("Defense Boost", f, b, 25, 170);
                g.DrawString("Arrow", f, b, 25, 200);
                Image gbrPanah = SetImageOpacity(Image.FromFile("tunjukMenuShop.png"), 0.5F);
                g.DrawImage(gbrPanah, 5, (indexShop - 10) * 30 + 22, 25, 25);
            }
            else if (indexItemShop != -1) {
                g.DrawString(listItem[indexItemShop].jenis, f, b, 5, 16);
                g.DrawString(banyakItem + "", f, b, 130, 16);
            }
        }

        public Image SetImageOpacity(Image image, float opacity)
        {
            Bitmap bmp = new Bitmap(image.Width, image.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                ColorMatrix matrix = new ColorMatrix();
                matrix.Matrix33 = opacity;
                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default,
                                                  ColorAdjustType.Bitmap);
                g.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height),
                                   0, 0, image.Width, image.Height,
                                   GraphicsUnit.Pixel, attributes);
            }
            return bmp;
        }

        private void InfoPanel_ClickLabel(object sender, EventArgs e)
        {
            panel1.BackColor = Color.Transparent;
            panel1.BackgroundImage = SetImageOpacity(Image.FromFile("Map00" + ((Form1)MdiParent).map + ".png"), 0.25F);
            lblPlayer.BackColor = Color.Transparent;
            lblPlayer.Image = SetImageOpacity(img, 0.5F);
            isPaused = true;

            //MessageBox.Show("Info Karakter!!!");
            infoCharacter.Visible = true;
            infoCharacter.BackColor = Color.FromArgb(150,Color.MintCream);
            panelInven.BackColor = Color.FromArgb(150, Color.MintCream);
        }

        private void panel1_MouseHover(object sender, EventArgs e)
        {
            //untuk kembalikan tulisan hover item
            TextHoverItem.Text = "";
        }

        private void DoubleClick_EquipWeapon(object sender, MouseEventArgs e)
        {
            clickWeaponX = e.X - 18; clickWeaponX /= 25;
            clickWeaponY = e.Y; clickWeaponY /= 66;

            bool cekBenarEquip = false;
            int index = clickWeaponX + clickWeaponY * 6;
            if (weapon[index].jenis == job)
            {
                weapon[index].isEquip = true;
                cekBenarEquip = true;
            }

            for (int i = 0; i < weapon.Count; i++)
            {
                if (cekBenarEquip)
                {
                    if (job == 0 && weapon[i].jenis == 0 && i != index) weapon[i].isEquip = false;
                    else if (job == 1 && weapon[i].jenis == 1 && i != index) weapon[i].isEquip = false;
                    else if (job == 2 && weapon[i].jenis == 2 && i != index) weapon[i].isEquip = false;
                }
            }
            panelInven.Invalidate();
        }

        private void Click_Weapon(object sender, MouseEventArgs e)
        {
            if (e.X >= 19 && e.X <= 170) {
                clickWeaponX = e.X - 18; clickWeaponX /= 25;
                clickWeaponY = e.Y; clickWeaponY /= 66;
            }
            else if (e.Y > 150 && e.X > 178)
            {   //buat UPGRADE
                if (cekBlacksmith == 1)
                {
                    int index = clickWeaponX + clickWeaponY * 6;
                    int biaya = (weapon[index].plusUpgrade + 1) * 50;
                    if (gold >= biaya)
                    {
                        MessageBox.Show("UPGRADED!!!");
                        panel1.Invalidate();
                        gold -= ((weapon[index].plusUpgrade + 1) * 50);
                        weapon[index].plusUpgrade++;
                        int randomAttack = rand.Next(10, 21);
                        weapon[index].att += (weapon[index].att * randomAttack / 100);
                    }
                    clickWeaponX = -1; clickWeaponY = -1;
                }
                else if (cekBlacksmith == 2)
                {
                    int index = clickWeaponX + clickWeaponY * 6;
                    int biaya = listShop[index].att * 30;
                    if (gold >= biaya)
                    {
                        MessageBox.Show("Purchased!!!");
                        panel1.Invalidate();
                        gold -= biaya;
                        weapon.Add(new inventory(listShop[index].jenis, listShop[index].att, 0));
                        listShop.RemoveAt(index);
                    }
                    clickWeaponX = -1; clickWeaponY = -1;
                }
                else if (cekBlacksmith == 3) {
                    int index = clickWeaponX + clickWeaponY * 6;
                    int biaya = weapon[index].att * 10;
                    MessageBox.Show("SOLD!!!");
                    gold += biaya;
                    weapon.RemoveAt(index);
                    clickWeaponX = -1; clickWeaponY = -1;
                }
            }
            panelInven.Invalidate();
            //MessageBox.Show("X : " + e.X + "\nY : " + e.Y);
            //170x20
        }

        private void panelInven_paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(bgInven, 18, 0, 150, 200);
            Brush b = new SolidBrush(Color.Black);
            Brush bw = new SolidBrush(Color.White);
            Font f = new Font("Monotype Corsiva", 13, FontStyle.Bold);
            // buat kluarin shop
            if (cekBlacksmith == 2) {
                for (int i = 0; i < listShop.Count; i++)
                {
                    if (listShop[i].jenis == 0) g.DrawImage(cropWeapon[0], i % 6 * 25 + 15, i / 6 * 70, 55, 50); //draw sword
                    else if (listShop[i].jenis == 1) g.DrawImage(cropWeapon[1], i % 6 * 25 + 9, i / 6 * 70, 55, 50); //draw staff
                    else if (listShop[i].jenis == 2) g.DrawImage(cropWeapon[2], i % 6 * 25, i / 6 * 70, 55, 50); //draw bow
                }
                if (clickWeaponX != -1 && clickWeaponY != -1)
                {
                    int index = clickWeaponX + clickWeaponY * 6;
                    if (listShop[index].jenis == 0) g.DrawImage(cropWeapon[0], 193, 10, 55, 50);
                    else if (listShop[index].jenis == 1) g.DrawImage(cropWeapon[1], 193 - 6, 10, 55, 50);
                    else if (listShop[index].jenis == 2) g.DrawImage(cropWeapon[2], 193 - 15, 10, 55, 50);

                    g.DrawString(listShop[index].att + "", f, b, 195, 70);
                    g.DrawString("+" + listShop[index].plusUpgrade, f, b, 213, 20);

                    //cetak tulisan buy dkk
                    g.DrawString("Cost : " + (listShop[index].att * 30), f, b, 177, 115);
                    g.DrawString("Purchase", f, b, 178, 150);

                }
            }
            else
            {
                for (int i = 0; i < weapon.Count; i++)
                {
                    if (weapon[i].jenis == 0) g.DrawImage(cropWeapon[0], i % 6 * 25 + 15, i / 6 * 70, 55, 50); //draw sword
                    else if (weapon[i].jenis == 1) g.DrawImage(cropWeapon[1], i % 6 * 25 + 9, i / 6 * 70, 55, 50); //draw staff
                    else if (weapon[i].jenis == 2) g.DrawImage(cropWeapon[2], i % 6 * 25, i / 6 * 70, 55, 50); //draw bow

                    //draw gambar e utk equip
                    if(weapon[i].isEquip)g.DrawString("E", f, bw, i % 6*25 + 25, i / 6*67);
                }
                if (clickWeaponX != -1 && clickWeaponY != -1)
                {
                    int index = clickWeaponX + clickWeaponY * 6;
                    if (weapon[index].jenis == 0) g.DrawImage(cropWeapon[0], 193, 10, 55, 50);
                    else if (weapon[index].jenis == 1) g.DrawImage(cropWeapon[1], 193 - 6, 10, 55, 50);
                    else if (weapon[index].jenis == 2) g.DrawImage(cropWeapon[2], 193 - 15, 10, 55, 50);
                    g.DrawString(weapon[index].att + "", f, b, 195, 70);
                    g.DrawString("+" + weapon[index].plusUpgrade, f, b, 213, 20);

                    if (cekBlacksmith == 1)
                    {
                        g.DrawString("+ 10~20 %", f, b, 172, 100);
                        g.DrawString("Cost : " + ((weapon[index].plusUpgrade + 1) * 50), f, b, 177, 115);
                        g.DrawString("Upgrade", f, b, 178, 150);
                    }
                    else if (cekBlacksmith == 3) {
                        g.DrawString("Sell", f, b, 178, 150);
                    }
                }
            }
        }

        private void Change_Job(object sender, MouseEventArgs e)
        {
            Rectangle r = new Rectangle(e.X, e.Y, 1, 1);
            Rectangle r1 = new Rectangle(10, 130, 60, 30);
            Rectangle r2 = new Rectangle(70, 130, 50, 30);
            Rectangle r3 = new Rectangle(120, 130, 50, 30);

            if (r.IntersectsWith(r1)) {
                job = 0;
                //masukkan gambar ke array
                if (player[job].lvl < 10) imgs = Image.FromFile("character1.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("warrior2.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("warrior31.png") as Bitmap;
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
            }
            else if (r.IntersectsWith(r2)) {
                job = 1;
                //masukkan gambar ke array
                if (player[job].lvl < 10) imgs = Image.FromFile("character2.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("mage11.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("mage21.png") as Bitmap;
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
            }
            else if (r.IntersectsWith(r3)) {
                job = 2;
                //masukkan gambar ke array
                if (player[job].lvl < 10) imgs = Image.FromFile("character3.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("archer21.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("archer31.png") as Bitmap;
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
            }
            lblPlayer.Image = img;
        }

        private void button_Inventory(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            //MessageBox.Show("INVENTORY!!!");
            infoCharacter.Visible = false;
            panelInven.Visible = true;
            clickWeaponX = -1; clickWeaponY = -1;
        }

        private void infoCharacter_paint(object sender, PaintEventArgs e)
        {

            Graphics g = e.Graphics;
            Brush b = new SolidBrush(Color.Black);
            Font f = new Font("Monotype Corsiva", 17, FontStyle.Bold);

            //cetak border job
            if (job == 0) g.DrawImage(borderJob, 8, 128, 62, 26);
            else if (job == 1) g.DrawImage(borderJob, 68, 128, 52, 26);
            else if (job == 2) g.DrawImage(borderJob, 118, 128, 52, 26);

            //cetak border Character
            //g.DrawImage(borderChar, 120, 10, 60, 70);

            g.DrawImage(img, 135, 20, 50, 50);
            g.DrawString("HP : " + player[job].hp + "/" + player[job].hpbase, f, b, 10, 10);
            g.DrawString("MP : " + player[job].mp + "/" + player[job].mpbase, f, b, 10, 40);
            g.DrawString("Lvl : " + player[job].lvl, f, b, 10, 70);
            g.DrawString("Gold : " + gold + " Dz", f, b, 10, 100);
            f = new Font("Monotype Corsiva", 13, FontStyle.Bold);
            g.DrawString("Warrior", f, b, 10, 130);
            g.DrawString("Mage", f, b, 70, 130);
            g.DrawString("Archer", f, b, 120, 130);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!bolehSerang)
            {
                //kanan kiri bawah atas
                if (tempanimasi <= 4) imgAtt = animationAttack[job, arahjalan, 0];
                else if (tempanimasi <= 9) imgAtt = animationAttack[job, arahjalan, 1];
                else if (tempanimasi == 10)
                {
                    imgAtt = animationAttack[job, arahjalan, 2];
                    bolehSerang = true; timer1.Stop();
                }
                tempanimasi++;
                panel1.Invalidate();
            }
            if (!bolehjalan)
            {
                if (arahjalan == 0) //Ke Kanan
                {
                    if (tempjalan == 0) img = character[2, 1];
                    else if (tempjalan == 1) img = character[2, 2];
                    else if (tempjalan == 2) img = character[2, 0];
                    else if (tempjalan == 3) img = character[2, 2];
                    else if (tempjalan == 4) { img = character[2, 0]; bolehjalan = true; timer1.Stop(); }
                    ((Form1)MdiParent).pX += 10; tempjalan++;
                    //atur tabrak
                    if (((Form1)MdiParent).map == 2)
                    {
                        if (((Form1)MdiParent).pX == 160 && ((Form1)MdiParent).pY == 100) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 360 && ((Form1)MdiParent).pY == 0) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 660) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 360 && ((Form1)MdiParent).pY == 400) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 410 && ((Form1)MdiParent).pY == 450) ((Form1)MdiParent).pX -= 10;
                    }
                    else if (((Form1)MdiParent).map == 1)
                    {
                        if (((Form1)MdiParent).pX == 160 && ((Form1)MdiParent).pY != 400) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 460 && ((Form1)MdiParent).pY != 400) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 660 && ((Form1)MdiParent).pY != 100) ((Form1)MdiParent).pX -= 10;
                    }
                    else if (((Form1)MdiParent).map == 3)
                    {
                        if (((Form1)MdiParent).pX == 460) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 160 && ((Form1)MdiParent).pY != 450 && ((Form1)MdiParent).pY != 400) ((Form1)MdiParent).pX -= 10;
                    }
                    else if (((Form1)MdiParent).map == 4)
                    {
                        if (((Form1)MdiParent).pY >= 500 && ((Form1)MdiParent).pY <= 600 && ((Form1)MdiParent).pX == 360) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY >= 500 && ((Form1)MdiParent).pY <= 550 && ((Form1)MdiParent).pX == 510) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 760) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY >= 250 && ((Form1)MdiParent).pY <= 400 && ((Form1)MdiParent).pX == 610) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY == 150 && ((Form1)MdiParent).pX == 610) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY >= 200 && ((Form1)MdiParent).pY <= 250 && ((Form1)MdiParent).pX == 460) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY >= 150 && ((Form1)MdiParent).pY <= 200 && ((Form1)MdiParent).pX == 310) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY == 250 && ((Form1)MdiParent).pX == 60) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY >= 300 && ((Form1)MdiParent).pY <= 550 && ((Form1)MdiParent).pX == 210) ((Form1)MdiParent).pX -= 10;
                    }
                    else if (((Form1)MdiParent).map == 5)
                    {
                        if (((Form1)MdiParent).pX > 750) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY > 350 && ((Form1)MdiParent).pX > 650) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 310 && ((Form1)MdiParent).pY >= 200 && ((Form1)MdiParent).pY <= 350) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pX == 510 && ((Form1)MdiParent).pY >= 350 && ((Form1)MdiParent).pY <= 400) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY == 150 && ((Form1)MdiParent).pX == 210) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY >= 300 && ((Form1)MdiParent).pY <= 350 && ((Form1)MdiParent).pX == 110) ((Form1)MdiParent).pX -= 10;
                    }
                    else if (((Form1)MdiParent).map == 6)
                    {
                        if (((Form1)MdiParent).pX > 750) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY >= 500 && ((Form1)MdiParent).pX > 650) ((Form1)MdiParent).pX -= 10;
                        if (((Form1)MdiParent).pY >= 200 && ((Form1)MdiParent).pY <= 300 && ((Form1)MdiParent).pX == 210) ((Form1)MdiParent).pX -= 10;
                        if ((((Form1)MdiParent).pY == 350 || ((Form1)MdiParent).pY == 400) && ((Form1)MdiParent).pX > 700) ((Form1)MdiParent).pX -= 10;
                        if ((((Form1)MdiParent).pY == 500 || ((Form1)MdiParent).pY == 550) && ((Form1)MdiParent).pX == 310) ((Form1)MdiParent).pX -= 10;
                        if ((((Form1)MdiParent).pY == 500 || ((Form1)MdiParent).pY == 550) && ((Form1)MdiParent).pX == 410) ((Form1)MdiParent).pX -= 10;
                    }
                }
                else if (arahjalan == 1) { //Ke Kiri
                    if (tempjalan == 0) img = character[1, 1];
                    else if (tempjalan == 1) img = character[1, 2];
                    else if (tempjalan == 2) img = character[1, 0];
                    else if (tempjalan == 3) img = character[1, 2];
                    else if (tempjalan == 4) { img = character[1, 0]; bolehjalan = true; timer1.Stop(); }
                    ((Form1)MdiParent).pX -= 10; tempjalan++;
                    //atur map
                    if (((Form1)MdiParent).map == 2)
                    {
                        if (((Form1)MdiParent).pX == 240 && (((Form1)MdiParent).pY == 0 || ((Form1)MdiParent).pY == 100)) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pX == 140 && ((Form1)MdiParent).pY == 50) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY >= 150 && ((Form1)MdiParent).pY <= 450 && ((Form1)MdiParent).pX == 640) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY >= 400 && ((Form1)MdiParent).pY <= 500 && ((Form1)MdiParent).pX == 240) ((Form1)MdiParent).pX += 10;
                    }
                    else if (((Form1)MdiParent).map == 1)
                    {
                        if (((Form1)MdiParent).pX == 640 && ((Form1)MdiParent).pY != 400) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pX == 140) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pX == 390 && ((Form1)MdiParent).pY != 400) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pX == 440 && ((Form1)MdiParent).pY < 400) ((Form1)MdiParent).pX += 10;
                    }
                    else if (((Form1)MdiParent).map == 3)
                    {
                        if (((Form1)MdiParent).pX == 390 && ((Form1)MdiParent).pY != 400 && ((Form1)MdiParent).pY != 450) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pX == 40) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pX == 140 && ((Form1)MdiParent).pY != 450 && ((Form1)MdiParent).pY != 400) ((Form1)MdiParent).pX += 10;
                    }
                    else if (((Form1)MdiParent).map == 4)
                    {
                        if (((Form1)MdiParent).pY == 600 && ((Form1)MdiParent).pX == 340) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY >= 500 && ((Form1)MdiParent).pY <= 550 && ((Form1)MdiParent).pX == 290) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 450 && ((Form1)MdiParent).pX == 340) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY >= 500 && ((Form1)MdiParent).pY <= 550 && ((Form1)MdiParent).pX == 440) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 400 && ((Form1)MdiParent).pX == 690) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY >= 200 && ((Form1)MdiParent).pY <= 400 && ((Form1)MdiParent).pX == 540) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY >= 150 && ((Form1)MdiParent).pY <= 200 && ((Form1)MdiParent).pX == 440) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 150 && ((Form1)MdiParent).pX == 290) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 200 && ((Form1)MdiParent).pX == 240) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 250 && ((Form1)MdiParent).pX == 190) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY >= 250 && ((Form1)MdiParent).pY <= 300 && ((Form1)MdiParent).pX == 40) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY >= 350 && ((Form1)MdiParent).pY <= 450 && ((Form1)MdiParent).pX == 190) ((Form1)MdiParent).pX += 10;

                        if (((Form1)MdiParent).pY == 500 && ((Form1)MdiParent).pX == 140) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 550 && ((Form1)MdiParent).pX == 90) ((Form1)MdiParent).pX += 10;
                    }
                    else if (((Form1)MdiParent).map == 5)
                    {
                        if (((Form1)MdiParent).pX < 50) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pX == 540 && ((Form1)MdiParent).pY >= 200 && ((Form1)MdiParent).pY <= 450) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pX == 190 && ((Form1)MdiParent).pY >= 200 && ((Form1)MdiParent).pY <= 350) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 150 && ((Form1)MdiParent).pX == 290) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 350 && ((Form1)MdiParent).pX == 390) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 300 && ((Form1)MdiParent).pX == 90) ((Form1)MdiParent).pX += 10;
                    }
                    else if (((Form1)MdiParent).map == 6)
                    {
                        if (((Form1)MdiParent).pX < 50) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY == 550 && ((Form1)MdiParent).pX < 100) ((Form1)MdiParent).pX += 10;
                        if ((((Form1)MdiParent).pY == 350 || ((Form1)MdiParent).pY == 400) && ((Form1)MdiParent).pX < 150) ((Form1)MdiParent).pX += 10;
                        if (((Form1)MdiParent).pY >= 200 && ((Form1)MdiParent).pY <= 300 && ((Form1)MdiParent).pX == 590) ((Form1)MdiParent).pX += 10;
                        if ((((Form1)MdiParent).pY == 500 || ((Form1)MdiParent).pY == 550) && ((Form1)MdiParent).pX == 390) ((Form1)MdiParent).pX += 10;
                        if ((((Form1)MdiParent).pY == 500 || ((Form1)MdiParent).pY == 550) && ((Form1)MdiParent).pX == 490) ((Form1)MdiParent).pX += 10;
                    }
                }
                else if (arahjalan == 2) //Ke Bawah
                {
                    if (tempjalan == 0) img = character[0, 1];
                    else if (tempjalan == 1) img = character[0, 2];
                    else if (tempjalan == 2) img = character[0, 0];
                    else if (tempjalan == 3) img = character[0, 2];
                    else if (tempjalan == 4) { img = character[0, 0]; bolehjalan = true; timer1.Stop(); }
                    ((Form1)MdiParent).pY += 10; tempjalan++;
                    //atur map
                    if (((Form1)MdiParent).map == 2)
                    {
                        if (((Form1)MdiParent).pX >= 0 && ((Form1)MdiParent).pX <= 200 && ((Form1)MdiParent).pX != 150) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX == 150 && ((Form1)MdiParent).pY == 110) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX != 650 && ((Form1)MdiParent).pY == 110) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY > 500) ((Form1)MdiParent).pY -= 10;
                    }
                    else if (((Form1)MdiParent).map == 1)
                    {
                        if (((Form1)MdiParent).pY == 110 && ((Form1)MdiParent).pX != 650) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX != 400 && ((Form1)MdiParent).pX != 450 && ((Form1)MdiParent).pY == 410) ((Form1)MdiParent).pY -= 10;
                    }
                    else if (((Form1)MdiParent).map == 3)
                    {
                        if (((Form1)MdiParent).pY == 460) ((Form1)MdiParent).pY -= 10;
                    }
                    else if (((Form1)MdiParent).map == 4)
                    {
                        if (((Form1)MdiParent).pY == 560 && ((Form1)MdiParent).pX != 350) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX == 300 && ((Form1)MdiParent).pY == 560) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX == 400 && ((Form1)MdiParent).pY == 460) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX >= 550 && ((Form1)MdiParent).pX <= 600 && ((Form1)MdiParent).pY == 460) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX >= 650 && ((Form1)MdiParent).pX <= 750 && ((Form1)MdiParent).pY == 460) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX >= 650 && ((Form1)MdiParent).pX <= 750 && ((Form1)MdiParent).pY == 210) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX >= 250 && ((Form1)MdiParent).pX <= 450 && ((Form1)MdiParent).pY == 260) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX >= 50 && ((Form1)MdiParent).pX <= 150 && ((Form1)MdiParent).pY == 310) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pX == 500 && ((Form1)MdiParent).pY == 160) ((Form1)MdiParent).pY -= 10;
                    }
                    else if (((Form1)MdiParent).map == 5)
                    {
                        if (((Form1)MdiParent).pY > 550) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY == 410 && ((Form1)MdiParent).pX >= 50 && ((Form1)MdiParent).pX <= 500) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY == 360 && ((Form1)MdiParent).pX >= 700 && ((Form1)MdiParent).pX <= 750) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY == 160 && ((Form1)MdiParent).pX >= 50 && ((Form1)MdiParent).pX <= 150) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY == 160 && ((Form1)MdiParent).pX >= 350 && ((Form1)MdiParent).pX <= 500) ((Form1)MdiParent).pY -= 10;
                    }
                    else if (((Form1)MdiParent).map == 6)
                    {
                        if (((Form1)MdiParent).pY > 550 && ((Form1)MdiParent).pX != 400) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY > 300 && ((Form1)MdiParent).pY <= 450 && ((Form1)MdiParent).pX >= 50 && ((Form1)MdiParent).pX <= 100) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY > 300 && ((Form1)MdiParent).pY <= 450 && ((Form1)MdiParent).pX == 750) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY > 500 && ((Form1)MdiParent).pX == 50) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY > 450 && ((Form1)MdiParent).pX >= 700 && ((Form1)MdiParent).pX <= 750) ((Form1)MdiParent).pY -= 10;
                        if (((Form1)MdiParent).pY > 450 && (((Form1)MdiParent).pX == 350 || ((Form1)MdiParent).pX == 450)) ((Form1)MdiParent).pY -= 10;
                    }
                }
                else if (arahjalan == 3) //Ke Atas
                {
                    if (tempjalan == 0) img = character[3, 1];
                    else if (tempjalan == 1) img = character[3, 2];
                    else if (tempjalan == 2) img = character[3, 0];
                    else if (tempjalan == 3) img = character[3, 2];
                    else if (tempjalan == 4) { img = character[3, 0]; bolehjalan = true; timer1.Stop(); }
                    ((Form1)MdiParent).pY -= 10; tempjalan++;
                    //atur map
                    if (((Form1)MdiParent).map == 2)
                    {
                        if (((Form1)MdiParent).pX >= 0 && ((Form1)MdiParent).pX <= 200 && ((Form1)MdiParent).pX != 150) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 150 && ((Form1)MdiParent).pY == 40) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX >= 400 && ((Form1)MdiParent).pX <= 650 && ((Form1)MdiParent).pY == 40) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX >= 450 && ((Form1)MdiParent).pX <= 600 && ((Form1)MdiParent).pY == 490) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 400 && ((Form1)MdiParent).pY == 440) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX >= 250 && ((Form1)MdiParent).pX <= 350 && ((Form1)MdiParent).pY == 390) ((Form1)MdiParent).pY += 10;
                    }
                    else if (((Form1)MdiParent).map == 1)
                    {
                        if (((Form1)MdiParent).pY <= 90) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX != 150 && ((Form1)MdiParent).pX != 650 && ((Form1)MdiParent).pX != 450 && ((Form1)MdiParent).pY == 390) ((Form1)MdiParent).pY += 10;
                    }
                    else if (((Form1)MdiParent).map == 3)
                    {
                        if (((Form1)MdiParent).pX != 400 && ((Form1)MdiParent).pX != 450 && ((Form1)MdiParent).pX != 150 && ((Form1)MdiParent).pY == 390) ((Form1)MdiParent).pY += 10;
                    }
                    else if (((Form1)MdiParent).map == 4)
                    {
                        if (((Form1)MdiParent).pX == 300 && ((Form1)MdiParent).pY == 490) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX >= 350 && ((Form1)MdiParent).pX <= 500 && ((Form1)MdiParent).pY == 440) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 650 && ((Form1)MdiParent).pY == 440) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX >= 700 && ((Form1)MdiParent).pX <= 750 && ((Form1)MdiParent).pY == 390) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX >= 650 && ((Form1)MdiParent).pX <= 750 && ((Form1)MdiParent).pY == 190) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX >= 450 && ((Form1)MdiParent).pX <= 600 && ((Form1)MdiParent).pY == 140) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX >= 350 && ((Form1)MdiParent).pX <= 400 && ((Form1)MdiParent).pY == 240) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 300 && ((Form1)MdiParent).pY == 140) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 250 && ((Form1)MdiParent).pY == 190) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 200 && ((Form1)MdiParent).pY == 240) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX >= 100 && ((Form1)MdiParent).pX <= 150 && ((Form1)MdiParent).pY == 290) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 50 && ((Form1)MdiParent).pY == 240) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 150 && ((Form1)MdiParent).pY == 490) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 100 && ((Form1)MdiParent).pY == 540) ((Form1)MdiParent).pY += 10;
                    }
                    else if (((Form1)MdiParent).map == 5)
                    {
                        if (((Form1)MdiParent).pY < 150) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pY == 490 && ((Form1)MdiParent).pX >= 50 && ((Form1)MdiParent).pX <= 500) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pY == 190 && ((Form1)MdiParent).pX == 250) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pY == 390 && (((Form1)MdiParent).pX == 150 || ((Form1)MdiParent).pX == 350)) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pY == 340 && ((Form1)MdiParent).pX >= 400 && ((Form1)MdiParent).pX <= 500) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 50 && ((Form1)MdiParent).pY == 340) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pX == 100 && ((Form1)MdiParent).pY == 290) ((Form1)MdiParent).pY += 10;
                    }
                    else if (((Form1)MdiParent).map == 6)
                    {
                        if (((Form1)MdiParent).pY < 200) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pY < 350 && ((Form1)MdiParent).pX >= 250 && ((Form1)MdiParent).pX <= 550) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pY > 300 && ((Form1)MdiParent).pY < 450 && ((Form1)MdiParent).pX >= 50 && ((Form1)MdiParent).pX <= 100) ((Form1)MdiParent).pY += 10;
                        if (((Form1)MdiParent).pY > 300 && ((Form1)MdiParent).pY < 450 && ((Form1)MdiParent).pX == 750) ((Form1)MdiParent).pY += 10;
                    }
                }
                lblPlayer.Image = img;
                lblPlayer.Location = new Point(((Form1)MdiParent).pX, ((Form1)MdiParent).pY);
                //panel1.Invalidate();
            }
            
        }
        

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            Rectangle mouse = new Rectangle(e.X, e.Y, 1, 1);
            Rectangle karakter = new Rectangle(((Form1)MdiParent).pX-50, ((Form1)MdiParent).pY, 50, 50);
            if (mouse.IntersectsWith(karakter))
            {
                //MessageBox.Show("Info Karakter!!!");
                //infoCharacter.Visible = true;
                //pindah di event click pada label player
            }
            else {
                isPaused = false;
                //ganti OPACITY
                panel1.BackColor = Color.Transparent;
                panel1.BackgroundImage = SetImageOpacity(Image.FromFile("Map00" + ((Form1)MdiParent).map + ".png"), 1F);
                lblPlayer.BackColor = Color.Transparent;
                lblPlayer.Image = SetImageOpacity(img, 1F);

                infoCharacter.Visible = false;
                panelInven.Visible = false;
                panelDifficulty.Visible = false;
                panelShop.Visible = false; indexShop = -2; cetakItemShop = false; indexItemShop = -1;
                upItem.Visible = false; downItem.Visible = false; banyakItem = 0; //button buat naik turun beli item potion dkk
                cekBlacksmith = 0;

                //animasi serang
                if ((job == 2 && player[job].arrow > 0) || job == 1 || job == 0)
                {
                    if (job == 2) player[job].arrow--;
                    bolehSerang = false; tempanimasi = 0;
                    imgAtt = animationAttack[job, arahjalan, 0];
                    timer1.Start();
                }
            }
        }
        public void refreshShop() {
            for (int i = 0; i < 18; i++)
            {
                int a = rand.Next(0, 3);
                int b = rand.Next(10, 50);
                listShop.Add(new inventory(a, b, 0));
            }
        }


        public void saveData()
        {
            //MessageBox.Show("SAVE!!!");
            //save posisi dan last job
            StreamWriter sw = new StreamWriter(Application.StartupPath + "/player.txt");
            sw.Write(((Form1)MdiParent).pX + "-" + ((Form1)MdiParent).pY + "-" + job + "-" + arahjalan + "-" + gold);
            sw.Close();

            System.IO.File.WriteAllText("inven.xml", string.Empty);
            Stream stream = File.OpenWrite(Environment.CurrentDirectory + "\\inven.xml");
            XmlSerializer xmlser = new XmlSerializer(typeof(List<inventory>));
            xmlser.Serialize(stream, weapon);
            stream.Close();

            System.IO.File.WriteAllText("item.xml", string.Empty);
            stream = File.OpenWrite(Environment.CurrentDirectory + "\\item.xml");
            xmlser = new XmlSerializer(typeof(List<Item>));
            xmlser.Serialize(stream, listItem);
            stream.Close();

            // save info dari karakter spt hp, mp dkk
            sw = new StreamWriter(Application.StartupPath + "/playerInfo.txt");
            for (int i = 0; i < 3; i++)
            {
                sw.WriteLine(player[i].hp + "-" + player[i].mp + "-" + player[i].lvl + "-" + player[i].skill + "-" + player[i].exp + "-" + player[i].hpbase + "-" + player[i].mpbase + "-" + player[i].arrow);
            }
            sw.Close();
        }

    }
}
