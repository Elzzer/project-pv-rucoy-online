﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyekRPG
{
    class FibHeapNode<T>
    {
        private bool c, mark;
        private int degree;
        private T key;
        private List<int> jalurx = new List<int>();
        private List<int> jalury = new List<int>();
        private FibHeapNode<T> parent, child, left, right;

        public bool C
        {
            get
            {
                return c;
            }

            set
            {
                c = value;
            }
        }

        public bool Mark
        {
            get
            {
                return mark;
            }

            set
            {
                mark = value;
            }
        }

        public int Degree
        {
            get
            {
                return degree;
            }

            set
            {
                degree = value;
            }
        }

        public T Key
        {
            get
            {
                return key;
            }

            set
            {
                key = value;
            }
        }

        public FibHeapNode<T> Parent
        {
            get
            {
                return parent;
            }

            set
            {
                parent = value;
            }
        }

        public FibHeapNode<T> Left
        {
            get
            {
                return left;
            }

            set
            {
                left = value;
            }
        }

        public FibHeapNode<T> Right
        {
            get
            {
                return right;
            }

            set
            {
                right = value;
            }
        }

        public FibHeapNode<T> Child
        {
            get
            {
                return child;
            }

            set
            {
                child = value;
            }
        }

        public List<int> Jalurx { get => jalurx; set => jalurx = value; }
        public List<int> Jalury { get => jalury; set => jalury = value; }

        public FibHeapNode() { }

        public FibHeapNode(int degree, T key, List<int>jalx, List<int> jaly)
        {
            this.jalurx = jalx;
            this.jalury = jaly;
            this.degree = degree;
            this.key = key;
        }

        public FibHeapNode(FibHeapNode<T> copy)
        {
            this.jalurx = copy.jalurx;
            this.jalury = copy.jalury;
            this.key = copy.key;
            this.child = copy.child;
            this.left = copy.left;
            this.right = copy.right;
            this.parent = copy.parent;
        }
    }
}
