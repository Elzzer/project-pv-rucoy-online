﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ProyekRPG
{

    public class Maze
    {
        
        private IntFibHeap heap = new IntFibHeap();
        private IntFibHeap H = new IntFibHeap();


        const int height = 10;
        const int width = 10;
        int[,] maze = new int[width, height];
        int startX = 0, startY = 0; // Starting X and Y values
        int endX = 9, endY = 9;     // Ending X and Y values
        List<List<int>> completePathX = new List<List<int>>();
        List<List<int>> completePathY = new List<List<int>>();

        public List<List<int>> CompletePathX { get => completePathX; set => completePathX = value; }
        public List<List<int>> CompletePathY { get => completePathY; set => completePathY = value; }
        public List<int> jalurkecilx()
        {
            List<int> jalx = new List<int>();
            IntFibHeap templagi = H;

            //FibHeapNode<int> temp = heap.ExtractMin(ref templagi);
            //jalx = temp.Jalurx;

            jalx = H.Min.Jalurx;

            return jalx;
        }
        public List<int> jalurkecily()
        {
            List<int> jaly = new List<int>();
            IntFibHeap templagi = H;
            IntFibHeap templagilagi = heap;

            FibHeapNode<int> temp = templagilagi.ExtractMin(ref templagi);
            jaly = temp.Jalury;

            return jaly;
        }
        // List of solutions of the maze

        public void InitMaze()
        {

            // Create Maze (0 = path, 1 = wall)
            int[,] maze1 = new int[,]{
                { 0,0,0,0,0,0,0,0,0,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,1,1,1,0,1,1,1,1,0},
                { 0,0,0,0,0,0,0,0,0,0}
            };

            int[,] maze2 = new int[,]{
                { 0,0,0,0,0,0,0,0,0,0},
                { 0,1,0,1,1,1,0,1,0,1},
                { 0,1,0,0,0,0,0,1,0,0},
                { 0,1,0,1,1,1,0,1,0,1},
                { 0,0,0,0,0,0,0,0,0,0},
                { 1,1,0,1,0,1,0,1,0,0},
                { 1,1,0,1,0,1,0,1,0,0},
                { 0,0,0,0,0,0,0,0,0,0},
                { 0,0,1,0,0,0,1,0,0,0},
                { 0,0,0,1,1,0,1,1,0,0}
            };

            int[,] maze3 = new int[,]{
                { 0,0,0,0,0,0,0,0,0,0},
                { 0,1,0,1,1,1,0,1,0,1},
                { 0,1,0,0,0,0,0,1,0,0},
                { 0,1,0,1,1,1,0,1,0,1},
                { 0,0,0,0,0,0,0,0,0,0},
                { 1,1,0,1,0,1,0,1,0,0},
                { 1,1,0,1,0,1,0,1,0,0},
                { 0,0,0,0,0,0,0,0,0,0},
                { 0,0,1,0,0,0,1,0,0,0},
                { 0,0,0,0,0,0,0,0,0,0}
            };
            maze = maze2;
        }
        public void InitMaze(int[,] maze, int startx, int starty, int endx, int endy)
        {
            // Create Maze (0 = path, 1 = wall)
            this.maze = maze;
            this.startX = startx;
            this.startY = starty;
            this.endX = endx;
            this.endY = endy;

            this.H = new IntFibHeap();
            
        }

        bool alreadyVisited(List<int> pX, List<int> pY, int x, int y)
        {
            bool notVisited = true;
            int n = pX.Count - 1;
            while (n >= 0 && notVisited)
            {
                if (pX[n] == x)
                    if (pY[n] == y)
                    {
                        notVisited = false;
                    }
                n--;
            }
            return !notVisited;
        }

        public void SolveMaze()
        {
            List<List<int>> pathXList = new List<List<int>>();
            List<List<int>> pathYList = new List<List<int>>();
            List<int> pathX = new List<int> { startX };
            List<int> pathY = new List<int> { startY };
            pathXList.Add(pathX);
            pathYList.Add(pathY);
            int ctr = 0;
            while (pathXList.Count > 0)
            {
                int pathNumber = pathXList.Count;
                while (pathNumber > 0)
                {
                    pathX = pathXList[0];
                    pathY = pathYList[0];
                    int n = pathX.Count - 1;
                    int x = pathX[n];
                    int y = pathY[n];
                    if (x == endX && y == endY)
                    {
                        CompletePathX.Add(pathX);
                        CompletePathY.Add(pathY);
                        heap.Insert(ref H, new FibHeapNode<int>(0, CompletePathX[CompletePathX.Count-1].Count, CompletePathX[CompletePathX.Count - 1], CompletePathY[CompletePathY.Count - 1]));
                        
                        ctr++;
                        pathXList.RemoveAt(0);
                        pathYList.RemoveAt(0);
                    }
                    else
                    {
                        if (x < width - 1) // Checks if not on right edge
                            if (maze[x + 1, y] != 1)
                                if (!alreadyVisited(pathX, pathY, x + 1, y))
                                {
                                    List<int> pX = new List<int>(pathX);
                                    List<int> pY = new List<int>(pathY);
                                    pX.Add(x + 1);
                                    pY.Add(y);
                                    pathXList.Add(pX);
                                    pathYList.Add(pY);
                                }
                        if (x > 0) // Checks if not on left edge
                            if (maze[x - 1, y] != 1)
                                if (!alreadyVisited(pathX, pathY, x - 1, y))
                                {
                                    List<int> pX = new List<int>(pathX);
                                    List<int> pY = new List<int>(pathY);
                                    pX.Add(x - 1);
                                    pY.Add(y);
                                    pathXList.Add(pX);
                                    pathYList.Add(pY);
                                }
                        if (y > 0)  // Checks if not on top edge
                            if (maze[x, y - 1] != 1)
                                if (!alreadyVisited(pathX, pathY, x, y - 1))
                                {
                                    List<int> pX = new List<int>(pathX);
                                    List<int> pY = new List<int>(pathY);
                                    pX.Add(x);
                                    pY.Add(y - 1);
                                    pathXList.Add(pX);
                                    pathYList.Add(pY);
                                }
                        if (y < height - 1) // Checks if not on bottom edge
                            if (maze[x, y + 1] != 1)
                                if (!alreadyVisited(pathX, pathY, x, y + 1))
                                {
                                    List<int> pX = new List<int>(pathX);
                                    List<int> pY = new List<int>(pathY);
                                    pX.Add(x);
                                    pY.Add(y + 1);
                                    pathXList.Add(pX);
                                    pathYList.Add(pY);
                                }
                        pathXList.RemoveAt(0);
                        pathYList.RemoveAt(0);
                    }
                    pathNumber--;
                }
            }
        }

        public void PrintOneMaze(List<int> pX, List<int> pY)
        {
            string[] symbols = { " ", "#", "." };
            int[,] m = (int[,])maze.Clone(); //deep copy because it is an array of int
            int n = pX.Count;
            //The solution path with 2
            for (int i = 0; i < n; i++)
            {
                m[pX[i], pY[i]] = 2;
            }
            for (int row = 0; row < 2 * height + 2; row++)
                Console.Out.Write("-");
            Console.Out.WriteLine();
            for (int row = 0; row < width; row++)
            {
                Console.Out.Write("|");
                for (int col = 0; col < height; col++)
                {
                    Console.Out.Write(symbols[m[row, col]] + " ");
                }
                Console.Out.Write("|");
                Console.Out.WriteLine();
            }
            for (int row = 0; row < 2 * height + 2; row++)
                Console.Out.Write("-");
            Console.Out.WriteLine();
        }

        public void PrintMaze()
        {
            int n = CompletePathX.Count;
            for (int i = 0; i < n; i++)
            {
                PrintOneMaze(CompletePathX[i], CompletePathY[i]);
                Console.Out.WriteLine();
            }
            Console.WriteLine("\r\n" + n + " solutions");
            
        }

    }
}

