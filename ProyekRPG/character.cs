﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyekRPG
{
    public class character
    {
        public character()
        {
            this.batasExp = 100;
        }
        public int lvl { get; set; }
        //public int px { get; set; }
        //public int py { get; set; }
        public int batasExp { get; set; }
        public string skill { get; set; }
        public int hp { get; set; }
        public int mp { get; set; }    
        public int mpbase { get; set; }
        public int hpbase { get; set; }
        public int exp { get; set; }
        public int arrow { get; set; }

        public bool attBoost { get; set; }
        public bool defBoost { get; set; }
        public int timerAttBoost { get; set; }
        public int timerDefBoost { get; set; }
    }
    public class warrior : character {
        public warrior() : base()
        {
            hp = 100;
            mp = 50;
            mpbase = 50;
            hpbase = 100;
            this.lvl = 1;
            //this.px = px;
            //this.py = py;
            skill = "";
            this.exp = 0;

            this.attBoost = false;
            this.defBoost = false;
        }
    }
    public class mage : character
    {
        public mage() : base()
        {
            hp = 100;
            mp = 100;
            mpbase = 100;
            hpbase = 90;
            this.lvl = 1;
            skill = "";
            this.exp = 0;

            this.attBoost = false;
            this.defBoost = false;
        }
    }
    public class archer : character
    {
        public archer() : base()
        {
            hp = 80;
            mp = 100;
            mpbase = 100;
            hpbase = 80;
            this.lvl = 1;
            skill = "";
            this.exp = 0;
            this.arrow = 10;

            this.attBoost = false;
            this.defBoost = false;
        }
    }

}
