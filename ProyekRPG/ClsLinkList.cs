﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyekRPG
{
    public class ClsLinkList
    {
        //ListBox lb; 
        ClsNode[] minNode = new ClsNode[100000];

        //public void setListBox(ListBox lb) { this.lb = lb; }

        public ClsLinkList()
        {
            
            for (int i = 0; i < 100; i++) { minNode[i] = null; }
        }

        public ClsNode findMin()
        {
            ClsNode min = null; 
            for (int i = 0; i < minNode.Length; i++)
            {
                if (minNode[i] != null)
                {
                    if (min == null) { min = minNode[i]; }
                    else
                    {
                        if (minNode[i].getBobot() < min.getBobot())
                        {
                            min = minNode[i]; 
                        }
                    }
                }
            }

            return min; 
        }

        public int findIndexMin()
        {
            int indexmin = -1; 
            for (int i = 0; i < minNode.Length; i++)
            {
                if (minNode[i] != null)
                {
                    if (indexmin == -1) { indexmin = i; }
                    else
                    {
                        if (minNode[i].getBobot() < minNode[indexmin].getBobot())
                        {
                            indexmin = i; ;
                        }
                    }
                }
            }

            return indexmin;
        }

        public void addNode(string nama, int bobot,List<int> x,List<int> y)
        {
            ClsNode newNode = new ClsNode(nama, bobot,x,y);
            int nomer = 1;
            while (nomer != 0)
            {
                if (minNode[nomer] == null)
                {
                    newNode.setParent(null);
                    minNode[nomer] = newNode;
                    nomer = 0;
                }
                else
                {
                    if (minNode[nomer].getBobot() > newNode.getBobot())
                    {
                        newNode.setParent(null);
                        minNode[nomer].setParent(newNode);
                        newNode.insertChildAtFront(minNode[nomer]);
                        minNode[nomer] = null;
                        nomer = nomer * 2; 
                    }
                    else
                    {
                        newNode.setParent(minNode[nomer]);
                        minNode[nomer].insertChildAtFront(newNode);
                        newNode = minNode[nomer];
                        minNode[nomer] = null; 
                        nomer = nomer * 2;
                    }
                }
            }
        }

        public void printNode(ClsNode node)
        {
            if (node != null)
            {
                if (node.getParent() != null)
                {
                    //lb.Items.Add(node.getBobot() + " --> " + node.getParent().getBobot());
                }
                else
                {
                    //lb.Items.Add(node.getBobot() + " --> nothing");
                }

                for (int i = 0; i < node.getJumChildren(); i++)
                {
                    printNode(node.getChildren(i)); 
                }
            }
        }

        public int getCountNode(ClsNode node)
        {
            if (node != null)
            {
                int jum = 0; 
                for (int i = 0; i < node.getJumChildren(); i++)
                {
                    jum = jum + 1 + getCountNode(node.getChildren(i));
                }
                return jum; 
            }
            else { return 0; }
        }

        

        public void mergeHeap(int jumlah, ClsNode newNode)
        {
            int nomer = jumlah;
            while (nomer != 0)
            {
                if (minNode[nomer] == null)
                {
                    newNode.setParent(null);
                    minNode[nomer] = newNode;
                    nomer = 0;
                }
                else
                {
                    if (minNode[nomer].getBobot() > newNode.getBobot())
                    {
                        newNode.setParent(null);
                        minNode[nomer].setParent(newNode);
                        newNode.insertChildAtFront(minNode[nomer]);
                        minNode[nomer] = null;
                        nomer = nomer * 2;
                    }
                    else
                    {
                        newNode.setParent(minNode[nomer]);
                        minNode[nomer].insertChildAtFront(newNode);
                        newNode = minNode[nomer];
                        minNode[nomer] = null;
                        nomer = nomer * 2;
                    }
                }
            }
        }

        public void releaseMin()
        {
            int     indexmin = findIndexMin();
            ClsNode min = findMin();
            minNode[indexmin] = null; 
            for (int i = 0; i < min.getJumChildren(); i++)
            {
                //MessageBox.Show(min.getChildren(i).getBobot().ToString());
                int jumlah = getCountNode(min.getChildren(i)) + 1;
                mergeHeap(jumlah, min.getChildren(i));
            }
        }

        public void cetakNode()
        {
            for (int i = 0; i < 100; i++) {
                printNode(minNode[i]); 
            }
        }
    }
}
