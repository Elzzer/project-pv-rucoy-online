﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Drawing.Imaging;
using System.Xml.Serialization;
using System.Runtime.InteropServices;
using System.Media;

namespace ProyekRPG
{
    public partial class formdungeon : Form
    {
        int ctrserangmusuh = 0;
        bool diserang = false;
        player p = new player();
        List<Point> halangan = new List<Point>();
        string strmaptemp = "";
        List<Image> listmonster = new List<Image>();
        List<enemy> listClassEnemy = new List<enemy>();
        List<Label> listlabelenemy = new List<Label>();
        Image[,] listimagemonstereasy = new Image[4, 3];
        Image[,] listimagemonstermedium = new Image[4, 3];
        Image[,] listimagemonsterhard = new Image[4, 3];
        Image[] TempMati = new Image[8];

        Image imgphon;
        Random rnd = new Random();
        Image imgmonster = Image.FromFile("Monster.png");
        bool dblatk = false;
        bool pieringShot = false;
        int ctrmage = 0;
        Image[] slash = new Image[5];
        int dmgmage = 0;
        int thunderSize = 50;


        //1400 x 1400
        //700 x 700 perFrame
        Image[] lightningStrike = new Image[4];
        bool mageSKill = false;


        // buat jalan
        Image backgroundImg = Image.FromFile("mapdungeon.png");
        Image imgs, img;
        Image[,,] character;
        bool bolehjalan = true;
        int tempjalan = 0; int arahjalan = 0;
        int job = 0;

        Panel infoCharacter, panelInven;

        Label lblPlayer;

        Image bgInven = Image.FromFile("kotakinven1.png");
        Image[] cropWeapon = new Image[3];
        List<inventory> weapon = new List<inventory>();

        int clickWeaponY = -1, clickWeaponX = -1;
        bool isPaused = false;
        Image borderJob=Image.FromFile("borderjob.png"), borderChar=Image.FromFile("borderchar.png");
        int gold = 5000;
        Image[,,] animationAttack;
        List<character> player = new List<character>();

        Image imgAtt; //temp buat animasi serang
        int tempanimasi = 0; //buat timer animasi
        bool bolehSerang = true;

        //NewProgressBarHp hpbar;
        //NewProgressBarMp mpbar;

        List<Item> listItem = new List<Item>(); //simpan item
        List<Image> inputItem = new List<Image>();
        List<Label> halanganPohon = new List<Label>();

        //coding potong gambar gerak player
        public void cropImage()
        {
            character = new Image[3, 4, 3];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    //MessageBox.Show(i + " " + j);
                    Rectangle r = new Rectangle(j * 47, i * 48, 47, 48);
                    Bitmap bmp = new Bitmap(imgs);
                    character[job, i, j] = bmp.Clone(r, bmp.PixelFormat);
                }
            }
            
            
            if (arahjalan == 0) img = character[job, 2, 0];
            else if (arahjalan == 1) img = character[job, 1, 0];
            else if (arahjalan == 2) img = character[job, 0, 0];
            else if (arahjalan == 3) img = character[job, 3, 0];

        }


        public formdungeon()
        {
            InitializeComponent();
            //panel1.Enabled = true;
            //panel1.Visible = true;
            //panel1.Size = new Size(800, 600);
            //panel1.Location = new Point(0, 0);
            ////panel1.BackColor = Color.Black;
            //panel1.BringToFront();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Bitmap bmp = new Bitmap(imgmonster);
                    Rectangle r = new Rectangle((j * 48) + (1 * 144), (i * 48) + (0 * 192), 48, 48);
                    listimagemonstereasy[i, j] = bmp.Clone(r, bmp.PixelFormat);

                    bmp = new Bitmap(imgmonster);
                    r = new Rectangle((j * 48) + (0 * 144), (i * 48) + (0 * 192), 48, 48);
                    listimagemonstermedium[i, j] = bmp.Clone(r, bmp.PixelFormat);

                    bmp = new Bitmap(imgmonster);
                    r = new Rectangle((j * 48) + (3 * 144), (i * 48) + (0 * 192), 48, 48);
                    listimagemonsterhard[i, j] = bmp.Clone(r, bmp.PixelFormat);
                }
            }
            for (int i = 0; i < 8; i++)
            {
                Bitmap bmp = new Bitmap("States.png");
                Rectangle r = new Rectangle(i * 96, 96 * 8, 96, 96);
                TempMati[i] = bmp.Clone(r, bmp.PixelFormat);
            }

            imgphon = Image.FromFile("tree.png");
            loaddungeon();

            player.Add(new warrior());
            player.Add(new mage());
            player.Add(new archer());
            int ctr = 0;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Bitmap bmp1 = new Bitmap("lightning.png");
                    Rectangle rr = new Rectangle((i * 700), (j * 700), 700, 700);
                    lightningStrike[ctr] = bmp1.Clone(rr, bmp1.PixelFormat);
                    ctr++;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SoundPlayer soundPlayer = new SoundPlayer(Application.StartupPath + @"\Battle2.wav");
            soundPlayer.Play();

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic, null, panel1, new object[] { true });
            gold = ((Form1)MdiParent).gold;

            //AMBIL DIFFICULTY DAN DECLARE MUSUH BERDASARKAN DIFFICULTY
            string stage = ((Form1)MdiParent).stage;
            if (stage == "easy")
            {
                spawnenemyeasy();
                //spawnenemyeasy();
            }
            else if (stage == "medium")
            {
                spawnenemymedium();
                spawnenemymedium();
            }
            else if (stage == "hard")
            {
                spawnenemyhard();
                spawnenemyhard();
            }

            for (int i = 0; i < 6; i++)
            {
                inputItem.Add(Image.FromFile((1 + i) + ".png"));
            }
            inputItem.Add(Image.FromFile("Potion.png"));
            inputItem.Add(Image.FromFile("MaxPotion.png"));
            inputItem.Add(Image.FromFile("Clarity.png"));
            inputItem.Add(Image.FromFile("AttUp.png"));
            inputItem.Add(Image.FromFile("DefenseUp.png"));
            inputItem.Add(Image.FromFile("Arrow.png"));

            //LOAD LOAD
            StreamReader sr = new StreamReader(Application.StartupPath + "/player.txt");
            string line = sr.ReadLine();
            string[] temp = line.Split('-');
            
            int arahjalan = Convert.ToInt32(temp[3]);
            gold = Convert.ToInt32(temp[4]);
            sr.Close();

            XmlSerializer serializer = new XmlSerializer(typeof(List<inventory>));
            using (FileStream stream = File.OpenRead("inven.xml"))
            {
                weapon = (List<inventory>)serializer.Deserialize(stream);
            }

            serializer = new XmlSerializer(typeof(List<Item>));
            using (FileStream stream = File.OpenRead("item.xml"))
            {
                listItem = (List<Item>)serializer.Deserialize(stream);
            }

            sr = new StreamReader(Application.StartupPath + "/player.txt");
            line = sr.ReadLine();
            temp = line.Split('-');
            job = Convert.ToInt32(temp[2]);
            sr.Close();

            sr = new StreamReader(Application.StartupPath + "/playerInfo.txt");
            int index = 0;
            while ((line = sr.ReadLine()) != null)
            {
                temp = line.Split('-');
                player[index].hp = Convert.ToInt32(temp[0]);
                player[index].mp = Convert.ToInt32(temp[1]);
                player[index].lvl = Convert.ToInt32(temp[2]);
                player[index].skill = temp[3];
                player[index].exp = Convert.ToInt32(temp[4]);
                player[index].hpbase = Convert.ToInt32(temp[5]);
                player[index].mpbase = Convert.ToInt32(temp[6]);
                player[index].arrow = Convert.ToInt32(temp[7]);
                index++;
            }
            sr.Close();

            //inisialisasi potong" gambar character

            btnSave.Visible = false;
            btnSave.Enabled = false;
            richTextBox1.Visible = false;
            
            for (int i = 0; i < 5; i++)
            {
                Bitmap bmp1 = new Bitmap("Slash.png");
                Rectangle rr = new Rectangle((i*192), 0, 192, 192);
                slash[i] = bmp1.Clone(rr, bmp1.PixelFormat);
            }

            panel1.Size = new Size(800, 500);
            panel1.Location = new Point(300, 100);
            panel1.BackColor = Color.Transparent;
            //panel1.BackgroundImage = Image.FromFile("Map002.png");
            //this.BackgroundImage = Image.FromFile("Map002.png");
            this.BackgroundImageLayout = ImageLayout.Stretch;
            this.BackColor = Color.Black;
            //this.Size = new Size(800, 600);



            //awal print job(buat load)
            if (job == 0)
            {
                if (player[job].lvl < 10) imgs = Image.FromFile("character1.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("warrior2.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("warrior31.png") as Bitmap;
            }
            else if (job == 1)
            {
                if (player[job].lvl < 10) imgs = Image.FromFile("character2.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("mage11.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("mage21.png") as Bitmap;
            }
            else if (job == 2)
            {
                if (player[job].lvl < 10) imgs = Image.FromFile("character3.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("archer21.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("archer31.png") as Bitmap;
            }
            cropImage(); cropAttack(); panel1.Invalidate();

            infoCharacter = new Panel();
            infoCharacter.Location = new Point(650, 250);
            infoCharacter.Size = new Size(200, 200);
            infoCharacter.Paint += new PaintEventHandler(infoCharacter_paint);
            infoCharacter.MouseClick += new MouseEventHandler(Change_Job);
            this.Controls.Add(infoCharacter);
            infoCharacter.Visible = false;

            panelInven = new Panel();
            panelInven.Location = new Point(650, 250);
            panelInven.Size = new Size(260, 200);
            panelInven.Paint += new PaintEventHandler(panelInven_paint);
            panelInven.MouseClick += new MouseEventHandler(Click_Weapon);
            panelInven.MouseDoubleClick += new MouseEventHandler(DoubleClick_EquipWeapon);
            this.Controls.Add(panelInven);
            panelInven.Visible = false;

            Button b = new Button();
            b.Location = new Point(65, 160);
            b.Size = new Size(60, 30);
            b.Text = "Inventory";
            b.Click += new EventHandler(button_Inventory);
            infoCharacter.Controls.Add(b);

            imgs = Image.FromFile("weapon.png") as Bitmap;
            Rectangle r = new Rectangle(198, 64, 96, 64);
            Bitmap bmp = new Bitmap(imgs);
            cropWeapon[0] = bmp.Clone(r, bmp.PixelFormat); // sword
            r = new Rectangle(190, 324, 96, 60);
            bmp = new Bitmap(imgs);
            cropWeapon[1] = bmp.Clone(r, bmp.PixelFormat); //staff
            r = new Rectangle(480, 0, 96, 64);
            bmp = new Bitmap(imgs);
            cropWeapon[2] = bmp.Clone(r, bmp.PixelFormat); // bow
            
            lblPlayer = new Label();
            lblPlayer.Size = new Size(50, 50);
            lblPlayer.Location = new Point(p.X, p.Y);
            panel1.Controls.Add(lblPlayer);
            lblPlayer.BackColor = Color.Transparent;
            lblPlayer.Click += new EventHandler(InfoPanel_ClickLabel);
            lblPlayer.Image = img;

            //weapon.Add(new inventory(0, 50, 0)); weapon[0].isEquip = true;
            //weapon.Add(new inventory(1, 50, 0)); weapon[1].isEquip = true;
            //weapon.Add(new inventory(2, 50, 0)); weapon[2].isEquip = true;


            //panel1.BackColor = Color.Transparent;
            //panel1.BackgroundImage = SetImageOpacity(Image.FromFile("Map002" + ".png"), 1F);
            lblPlayer.BackColor = Color.Transparent;
            lblPlayer.Image = SetImageOpacity(img, 1F);

            progressBar1.ForeColor = Color.Red;
            progressBar1.Maximum = player[job].hpbase;
            progressBar1.Value = player[job].hp;
            progressBar2.ForeColor = Color.Blue;
            progressBar2.Maximum = player[job].mpbase;
            progressBar2.Value = player[job].mp;
        }

        public void loaddungeon() {
            StreamReader sr = new StreamReader("map.txt");
            List<string> str = new List<string>();
            string temp;
            while ((temp = sr.ReadLine()) != null)
            {
                str.Add(temp);
            }

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 17; j++)
                {
                    if(i == 0 || i == 9)
                    {
                        halangan.Add(new Point(j * 50 + 300, i * 50 + 100));
                    }
                    else if(j == 0 || j == 16)
                    {
                        halangan.Add(new Point(j * 50 + 300, i * 50 + 100));
                    }
                }
            }

            for (int i = 0; i < str.Count; i++)
            {
                for (int j = 0; j < str[i].Length; j++)
                {
                    if (str[i][j].ToString().Equals("X"))
                    {
                        halangan.Add(new Point(j * 50 + 300, i * 50 + 100));
                    }
                }
            }
            sr.Close();
            for (int i = 0; i < halangan.Count; i++)
            {
                Label a = new Label();
                a.BackgroundImage = imgphon;
                a.Location = new Point(halangan[i].X, halangan[i].Y);
                a.Size = new Size(50, 50);
                a.BackgroundImageLayout = ImageLayout.Stretch;
                a.BackColor = Color.Transparent;
                a.Parent = this;
                a.BringToFront();
                halanganPohon.Add(a);
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Font f = new Font("Arial", 16, FontStyle.Bold);
            Brush b = new SolidBrush(Color.White);
            g.DrawImage(backgroundImg, 300, 100, 850, 500);
            if (player[job].hp <= 0)
            {
                g.DrawString("Game Over !!!", f, b, 660, 630);
                g.DrawString("Press P to Go Home", f, b, 630, 660);
            }

            //g.DrawImage(inputItem[0], 610, 390, 20, 20); 
            g.DrawString(" " + listItem[0].banyak, new Font("arial", 11, FontStyle.Bold), b, 910, 620); g.DrawImage(inputItem[6], 910, 600, 20, 20);
            //g.DrawImage(inputItem[1], 640, 390, 20, 20); 
            g.DrawString(" " + listItem[1].banyak, new Font("arial", 11, FontStyle.Bold), b, 940, 620); g.DrawImage(inputItem[7], 940, 600, 20, 20);
            //g.DrawImage(inputItem[2], 670, 390, 20, 20); 
            g.DrawString(" " + listItem[3].banyak, new Font("arial", 11, FontStyle.Bold), b, 970, 620); g.DrawImage(inputItem[8], 970, 600, 20, 20);
            //g.DrawImage(inputItem[3], 700, 390, 20, 20); 
            g.DrawString(" " + listItem[4].banyak, new Font("arial", 11, FontStyle.Bold), b, 1000, 620); g.DrawImage(inputItem[9], 1000, 600, 20, 20);
            //g.DrawImage(inputItem[4], 730, 390, 20, 20); 
            g.DrawString(" " + listItem[5].banyak, new Font("arial", 11, FontStyle.Bold), b, 1030, 620); g.DrawImage(inputItem[10], 1030, 600, 20, 20);
            //g.DrawImage(inputItem[5], 760, 390, 20, 20); 
            g.DrawString(" " + listItem[6].banyak, new Font("arial", 11, FontStyle.Bold), b, 1060, 620); g.DrawImage(inputItem[11], 1060, 600, 20, 20);
        }

        public void whirlwind(int damage)
        {
            Rectangle a = new Rectangle(lblPlayer.Location.X-25, lblPlayer.Location.Y-25, lblPlayer.Width+50, lblPlayer.Height+50);
            for (int i = 0; i < listClassEnemy.Count; i++)
            {
                Rectangle b = new Rectangle(listClassEnemy[i].x-10, listClassEnemy[i].y-10, 50+20, 50+20);
                if (a.IntersectsWith(b))
                {
                    listClassEnemy[i].Hp -= damage;
                    MessageBox.Show(damage+"");
                }
            }
        }
        public void instantKill(int arah)
        {
            int x = lblPlayer.Location.X;
            int y = lblPlayer.Location.Y;
            
            if (arah == 0)
            {
                x += 25;
            }
            else if (arah == 1)
            {
                x -= 25;
            }
            else if (arah == 2)
            {
                y += 25;
            }
            else if (arah == 3)
            {
                y -= 25;
            }
            Rectangle a = new Rectangle(x,y,lblPlayer.Width,lblPlayer.Height);
            // kanan kiri bawah atas

            for (int i = listClassEnemy.Count-1; i >=0 ; i--)
            {
                Rectangle b = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                if (a.IntersectsWith(b))
                {
                    listClassEnemy.RemoveAt(i);
                    break;
                }
            }
        }
        public void doubleAttack()
        {
            player[job].arrow--;
            bolehSerang = false;
            tempanimasi = 0;
            imgAtt = animationAttack[job, arahjalan, 0];
            dblatk = true;
            timer1.Start();
            serang();
            
        }
        public void piercingShot()
        {
            player[job].arrow--;
            bolehSerang = false;
            tempanimasi = 0;
            imgAtt = animationAttack[job, arahjalan, 0];
            pieringShot = true;
            timer1.Start();
            serang();
        }
        
        private void btnSave_Click(object sender, EventArgs e)
        {
            strmaptemp += richTextBox1.Text.Substring(strmaptemp.Length);
            StreamWriter sw = new StreamWriter(Application.StartupPath + "\\map.txt");
            for (int i = 0; i < strmaptemp.Length; i++)
            {
                if (!strmaptemp.Substring(i, 1).Equals("-"))
                {
                    sw.Write(strmaptemp.Substring(i, 1));
                }
                else
                {
                    sw.WriteLine();
                }
            }
            sw.Close();
        }
        public bool cekserang(int x,int y,int arah) {
            bool cek = false;
            int height = 50;
            int width = 50;
            if(arah == 0)
            {
                height += 10;
            }
            else if(arah == 1)
            {
                x -= 10;
            }
            else if(arah == 2)
            {
                width += 10;
            }
            else if(arah == 3)
            {
                y -= 10;
            }
            Rectangle a = new Rectangle(p.X, p.Y, 50, 50);
            Rectangle b = new Rectangle(x, y,width, height);
            if (a.IntersectsWith(b))
            {
                cek = true;
            }
            
            return cek;

        }
        public bool cekTabrak(int x, int y, int indexplayer)
        {
            bool cek = true;
            Rectangle a = new Rectangle(x, y, 50, 50);
            for (int i = 0; i < halangan.Count; i++)
            {
                Rectangle b = new Rectangle(halangan[i].X - 300, halangan[i].Y - 100, 50, 50);
                if (a.IntersectsWith(b))
                {
                    cek = false;
                    //MessageBox.Show("Test");
                }
            }
            if (cek)
            {
                for (int i = 0; i < listClassEnemy.Count; i++)
                {
                    if (i != indexplayer)
                    {
                        Rectangle b = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                        if (a.IntersectsWith(b))
                        {
                            cek = false;
                        }
                    }
                }
            }
            return cek;
        }
        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                strmaptemp += richTextBox1.Text.Substring(strmaptemp.Length);
                strmaptemp += "-";
            }

        }
        //public void spawn()
        //{
        //    //576x384
        //    //48
        //    listmonster.Clear();


        //    int randommusuhx = 0;
        //    int randommusuhy = 0;
        //    List<int> randomx = new List<int>();
        //    List<int> randomy = new List<int>();
        //    for (int k = 0; k < 10; k++)
        //    {
        //        randommusuhx = rnd.Next(3);
        //        randommusuhy = rnd.Next(2);

        //        listimagemonster = new Image[4, 3];
        //        for (int i = 0; i < 4; i++)
        //        {
        //            for (int j = 0; j < 3; j++)
        //            {
        //                Bitmap bmp = new Bitmap(imgmonster);
        //                Rectangle r = new Rectangle((j * 48) + (randommusuhx * 144), (i * 48) + (randommusuhy * 192), 48, 48);
        //                listimagemonster[i, j] = bmp.Clone(r, bmp.PixelFormat);
        //            }
        //        }
        //        //enemy ab = new enemy(listmonster.ToArray(), listimagemonster);
        //        //ab.x = 50 + (k * 50) + (50 * k);
        //        //ab.y = 100;
                
        //        //listClassEnemy.Add(ab);
        //    }

        //    //enemyHard a = new enemyHard();
        //}
        public void spawnenemyeasy() {
            
            enemyeasy ab = new enemyeasy(listmonster.ToArray(), listimagemonstereasy);
            if (listClassEnemy.Count == 0) { ab.x = 50; ab.y = 300; }
            else if (listClassEnemy.Count == 1) { ab.x = 250; ab.y = 150; }
            else if (listClassEnemy.Count == 2) { ab.x = 500; ab.y = 50; }
            else if (listClassEnemy.Count == 3) { ab.x = 300; ab.y = 250; }
            else if (listClassEnemy.Count == 4) { ab.x = 150; ab.y = 400; }


            ab.Mati = TempMati;

            listClassEnemy.Add(ab);

        }
        public void spawnenemymedium()
        {
            enemymedium ab = new enemymedium(listmonster.ToArray(), listimagemonstermedium);
            if (listClassEnemy.Count == 0) { ab.x = 100; ab.y = 100; }
            else if (listClassEnemy.Count == 1) { ab.x = 250; ab.y = 150; }
            else if (listClassEnemy.Count == 2) { ab.x = 500; ab.y = 50; }
            else if (listClassEnemy.Count == 3) { ab.x = 300; ab.y = 250; }
            else if (listClassEnemy.Count == 4) { ab.x = 150; ab.y = 400; }
            ab.Mati = TempMati;

            listClassEnemy.Add(ab);

        }
        public void spawnenemyhard()
        {
            enemyHard ab = new enemyHard(listmonster.ToArray(), listimagemonsterhard);
            if (listClassEnemy.Count == 0) { ab.x = 100; ab.y = 100; }
            else if (listClassEnemy.Count == 1) { ab.x = 250; ab.y = 150; }
            else if (listClassEnemy.Count == 2) { ab.x = 500; ab.y = 50; }
            else if (listClassEnemy.Count == 3) { ab.x = 300; ab.y = 250; }
            else if (listClassEnemy.Count == 4) { ab.x = 150; ab.y = 400; }
            //960 x 768
            ab.Mati = TempMati;
            
            listClassEnemy.Add(ab);

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!bolehSerang)
            {
                if (pieringShot)
                {
                    //kanan kiri bawah atas
                    if (tempanimasi <= 4) imgAtt = animationAttack[job, arahjalan, 0];
                    else if (tempanimasi <= 19) imgAtt = animationAttack[job, arahjalan, 1];
                    else if (tempanimasi == 20)
                    {
                        imgAtt = animationAttack[job, arahjalan, 2];
                        bolehSerang = true; timer1.Stop();
                        bolehjalan = true;
                        pieringShot = false;
                    }
                    tempanimasi++;
                    panel1.Invalidate();
                }
                else if (mageSKill)
                {
                    if (ctrmage > 35)
                    {
                        mageSKill = false;
                        for (int i = 0; i < listClassEnemy.Count; i++)
                        {
                            Rectangle a = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                            int x = lblPlayer.Location.X;
                            int y = lblPlayer.Location.Y;
                            // kanan kiri bawah atas
                            if(arahjalan == 0)
                            {
                                x += 50;
                            }
                            else if(arahjalan == 1)
                            {
                                x -= 50;
                            }
                            else if(arahjalan == 2)
                            {
                                y += 50;
                            }
                            else if(arahjalan == 3)
                            {
                                y -= 50;
                            }
                            Rectangle b = new Rectangle(x+10, y+10, 30, 30);
                            if (a.IntersectsWith(b))
                            {
                                listClassEnemy[i].Hp -= dmgmage;
                                //if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyeasy) tambahExp("easy");
                                //if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemymedium) tambahExp("medium");
                                //if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyHard) tambahExp("hard");
                            }
                        }
                        ctrmage = 0;
                        timer1.Stop();
                        bolehjalan = true;
                        bolehSerang = true;
                    }
                    else
                    {
                        ctrmage++;
                        if (ctrmage > 20)
                        {
                            for (int i = 0; i < listClassEnemy.Count; i++)
                            {
                                Rectangle a = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                                int x = lblPlayer.Location.X;
                                int y = lblPlayer.Location.Y;
                                // kanan kiri bawah atas
                                if (arahjalan == 0)
                                {
                                    x += 50;
                                }
                                else if (arahjalan == 1)
                                {
                                    x -= 50;
                                }
                                else if (arahjalan == 2)
                                {
                                    y += 50;
                                }
                                else if (arahjalan == 3)
                                {
                                    y -= 50;
                                }
                                Rectangle b = new Rectangle(x + 10, y + 10, 30, 30);
                                if (a.IntersectsWith(b))
                                {
                                    listClassEnemy[i].Hp -= dmgmage;
                                    //if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyeasy) tambahExp("easy");
                                    //if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemymedium) tambahExp("medium");
                                    //if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyHard) tambahExp("hard");
                                }
                            }
                        }
                        
                    }
                    panel1.Invalidate();
                }
                else
                {
                    //kanan kiri bawah atas
                    if (tempanimasi <= 4) imgAtt = animationAttack[job, arahjalan, 0];
                    else if (tempanimasi <= 9) imgAtt = animationAttack[job, arahjalan, 1];
                    else if (tempanimasi == 10)
                    {
                        imgAtt = animationAttack[job, arahjalan, 2];
                        bolehSerang = true; timer1.Stop();
                        bolehjalan = true;
                        if (dblatk)
                        {
                            player[job].arrow--;
                            bolehSerang = false;
                            tempanimasi = 0;
                            imgAtt = animationAttack[job, arahjalan, 0];
                            dblatk = false;
                            timer1.Start();
                            serang();
                        }
                    }
                    tempanimasi++;
                    panel1.Invalidate();
                }
                
            }
            //gerak animasi player
            if (!bolehjalan && !mageSKill)
            {
                if (arahjalan == 0) //Ke Kanan
                {
                    if (tempjalan == 0) img = character[job, 2, 1];
                    else if (tempjalan == 1) img = character[job, 2, 2];
                    else if (tempjalan == 2) img = character[job, 2, 0];
                    else if (tempjalan == 3) img = character[job, 2, 2];
                    else if (tempjalan == 4) { img = character[job, 2, 0]; bolehjalan = true; timer1.Stop(); }
                    p.X += 10; tempjalan++;
                    if (p.X == 760) p.X -= 10;

                    for (int i = 0; i < halangan.Count; i++)
                    {
                        Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                        Rectangle r2 = new Rectangle(halangan[i].X - 300, halangan[i].Y - 100, 50, 50);
                        if (r1.IntersectsWith(r2))
                        {
                            p.X -= 10;
                        }
                    }

                    
                }
                else if (arahjalan == 1)
                { //Ke Kiri
                    if (tempjalan == 0) img = character[job, 1, 1];
                    else if (tempjalan == 1) img = character[job, 1, 2];
                    else if (tempjalan == 2) img = character[job, 1, 0];
                    else if (tempjalan == 3) img = character[job, 1, 2];
                    else if (tempjalan == 4) { img = character[job, 1, 0]; bolehjalan = true; timer1.Stop(); }
                    p.X -= 10; tempjalan++;
                    if (p.X == 40) p.X += 10;

                    for (int i = 0; i < halangan.Count; i++)
                    {
                        Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                        Rectangle r2 = new Rectangle(halangan[i].X - 300, halangan[i].Y - 100, 50, 50);
                        if (r1.IntersectsWith(r2))
                        {
                            p.X += 10;
                        }
                    }
                }
                else if (arahjalan == 2) //Ke Bawah
                {
                    if (tempjalan == 0) img = character[job, 0, 1];
                    else if (tempjalan == 1) img = character[job, 0, 2];
                    else if (tempjalan == 2) img = character[job, 0, 0];
                    else if (tempjalan == 3) img = character[job, 0, 2];
                    else if (tempjalan == 4) { img = character[job, 0, 0]; bolehjalan = true; timer1.Stop(); }
                    p.Y += 10; tempjalan++;
                    if (p.Y == 410) p.Y -= 10;

                    for (int i = 0; i < halangan.Count; i++)
                    {
                        Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                        Rectangle r2 = new Rectangle(halangan[i].X - 300, halangan[i].Y - 100, 50, 50);
                        if (r1.IntersectsWith(r2))
                        {
                            p.Y -= 10;
                        }
                    }
                }
                else if (arahjalan == 3) //Ke Atas
                {
                    if (tempjalan == 0) img = character[job, 3, 1];
                    else if (tempjalan == 1) img = character[job, 3, 2];
                    else if (tempjalan == 2) img = character[job, 3, 0];
                    else if (tempjalan == 3) img = character[job, 3, 2];
                    else if (tempjalan == 4) { img = character[job, 3, 0]; bolehjalan = true; timer1.Stop(); }
                    p.Y -= 10; tempjalan++;
                    if (p.Y == 40) p.Y += 10;

                    for (int i = 0; i < halangan.Count; i++)
                    {
                        Rectangle r1 = new Rectangle(p.X, p.Y, 50, 50);
                        Rectangle r2 = new Rectangle(halangan[i].X - 300, halangan[i].Y - 100, 50, 50);
                        if (r1.IntersectsWith(r2))
                        {
                            p.Y += 10;
                        }
                    }
                }
                lblPlayer.Image = img;
                lblPlayer.Location = new Point(p.X, p.Y);
            }
        }

        private void formdungeon_MouseClick(object sender, MouseEventArgs e)
        {
            spawnenemyeasy();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            //MessageBox.Show("Test1");
            //List<int> listremove = new List<int>();
            //int x = 0;
            //int y = 0;
            //int width = 50;
            //int height = 50;

            //int xtemp = 0;
            //int ytemp = 0;
            Graphics g = e.Graphics;
            Brush b = new SolidBrush(Color.Black);
            Brush bb = new SolidBrush(Color.White);
            Font f = new Font("Arial", 15, FontStyle.Bold);
            //bool hasilcekserang = false;

            //for (int i = 0; i < halangan.Count; i++)
            //{
            //    g.DrawRectangle(new Pen(Color.Red), halangan[i].X, halangan[i].Y, 50, 50);
            //}


            for (int i = 0; i < listClassEnemy.Count; i++)
            {
                if (listClassEnemy[i].Status == 0)
                {
                    g.DrawImage(listClassEnemy[i].b, listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                }
                else
                {

                    g.DrawImage(listClassEnemy[i].b, listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                    //g.DrawImage(p.B, p.X, p.Y, 50, 50);
                    //g.DrawImage(img, p.X, p.Y, 50, 50);
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    if (listClassEnemy[i].IsAttacking)
                    {
                        g.DrawImage(slash[ctrserangmusuh], p.X - 25, p.Y - 25, 100, 100);
                    }
                    //g.DrawRectangle(new Pen(Color.Red), xtemp, ytemp, width, height);  //untuk cek range area musuh

                }
            }

            if (!bolehSerang)
            {
                int x = 0;
                int y = 0;

                int gerakBow = 0;
                if (job == 2) gerakBow = tempanimasi * 5;
                
                if (pieringShot)
                {
                    int index = -1;
                    for (int i = 0; i < weapon.Count; i++)
                    {
                        if (job == weapon[i].jenis && weapon[i].isEquip)
                        {
                            index = i;
                        }
                    }
                    int damage = 0;
                    if (index != -1)
                    {
                        damage = weapon[index].att;
                    }
                    damage = 100;
                    

                    Rectangle a = new Rectangle(x, y, 50, 50);
                    for (int i = 0; i < listClassEnemy.Count; i++)
                    {
                        Rectangle ab = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                        if (a.IntersectsWith(ab))
                        {
                            listClassEnemy[i].Hp -= damage;
                            if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyeasy) tambahExp("easy");
                            if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemymedium) tambahExp("medium");
                            if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyHard) tambahExp("hard");
                        }
                    }
                }
                else if (mageSKill)
                {
                    x = lblPlayer.Location.X;
                    y = lblPlayer.Location.Y;
                    if (arahjalan == 0) { x += 50; }
                    else if (arahjalan == 1) { x -=50; }
                    else if (arahjalan == 2) {y += 50; }
                    else if (arahjalan == 3) {y -= 50; }

                    if (ctrmage < 10)
                    {
                        g.DrawImage(lightningStrike[0], x - (thunderSize/100*25), y - (thunderSize - 50), thunderSize, thunderSize);
                    }
                    else if (ctrmage < 20)
                    {
                        g.DrawImage(lightningStrike[1], x - (thunderSize / 100 * 25), y - (thunderSize - 50), thunderSize, thunderSize);
                    }
                    else if (ctrmage < 30)
                    {
                        g.DrawImage(lightningStrike[2], x - (thunderSize / 100 * 25), y - (thunderSize - 50), thunderSize, thunderSize);
                    }
                    else{
                        g.DrawImage(lightningStrike[3], x - (thunderSize / 100 * 25), y - (thunderSize - 50), thunderSize, thunderSize);
                    }
                    
                }
                else
                {
                    if (arahjalan == 0) { x = p.X + 30 + gerakBow; y = p.Y; }
                    else if (arahjalan == 1) { x = p.X - 30 - gerakBow; y = p.Y; }
                    else if (arahjalan == 2) { x = p.X; y = p.Y + 30 + gerakBow; }
                    else if (arahjalan == 3) { x = p.X; y = p.Y - 30 - gerakBow; }
                    g.DrawImage(imgAtt, x, y, 50, 50);
                }
            }

            ////g.DrawImage(inputItem[0], 610, 390, 20, 20); 
            //g.DrawString(" " + listItem[0].banyak, new Font("arial", 11, FontStyle.Bold), b, 610, 430); g.DrawImage(inputItem[6], 610, 410, 20, 20);
            ////g.DrawImage(inputItem[1], 640, 390, 20, 20); 
            //g.DrawString(" " + listItem[1].banyak, new Font("arial", 11, FontStyle.Bold), b, 640, 430); g.DrawImage(inputItem[7], 640, 410, 20, 20);
            ////g.DrawImage(inputItem[2], 670, 390, 20, 20); 
            //g.DrawString(" " + listItem[3].banyak, new Font("arial", 11, FontStyle.Bold), b, 670, 430); g.DrawImage(inputItem[8], 670, 410, 20, 20);
            ////g.DrawImage(inputItem[3], 700, 390, 20, 20); 
            //g.DrawString(" " + listItem[4].banyak, new Font("arial", 11, FontStyle.Bold), b, 700, 430); g.DrawImage(inputItem[9], 700, 410, 20, 20);
            ////g.DrawImage(inputItem[4], 730, 390, 20, 20); 
            //g.DrawString(" " + listItem[5].banyak, new Font("arial", 11, FontStyle.Bold), b, 730, 430); g.DrawImage(inputItem[10], 730, 410, 20, 20);
            ////g.DrawImage(inputItem[5], 760, 390, 20, 20); 
            //g.DrawString(" " + listItem[6].banyak, new Font("arial", 11, FontStyle.Bold), b, 760, 430); g.DrawImage(inputItem[11], 760, 410, 20, 20);
            
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (listClassEnemy.Count == 0)
            {
                ((Form1)MdiParent).gold = gold;
                saveData();
                ((Form1)MdiParent).OpenBoss();
                this.Close();
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (player[job].attBoost)
            {
                player[job].timerAttBoost--;
                if (player[job].timerAttBoost == 0) player[job].attBoost = false;
            }
            if (player[job].defBoost)
            {
                player[job].timerDefBoost--;
                if (player[job].timerDefBoost == 0) player[job].defBoost = false;
            }

            //cek 
            //MessageBox.Show("Test1");
            List<int> listremove = new List<int>();
            int x = 0;
            int y = 0;
            int width = 50;
            int height = 50;

            int xtemp = 0;
            int ytemp = 0;
            //Graphics g = e.Graphics;
            bool hasilcekserang = false;
            //for (int i = 0; i < halangan.Count; i++)
            //{
            //    g.DrawImage(imgphon, halangan[i].X, halangan[i].Y, 50, 50);
            //}
            for (int i = 0; i < listClassEnemy.Count; i++)
            {
                if (listClassEnemy[i].Status == 0)
                {
                    if (listClassEnemy[i].CtrMati > 8)
                    {
                        listremove.Add(i);
                        
                    }
                    else
                    {
                        //g.DrawImage(listClassEnemy[i].b, listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                        listClassEnemy[i].CtrMati++;
                    }
                }
                else
                {
                    //int moveornot = 0;
                    //int arahgerak = 0;
                    int moveornot = rnd.Next(2);
                    int arahgerak = rnd.Next(4);
                    if (listClassEnemy[i].IsAttacking == true)
                    {
                        arahgerak = listClassEnemy[i].Arah;
                    }

                    //listClassEnemy[i].Arah = arahgerak;


                    moveornot = 1;
                    if (moveornot == 1)
                    {
                        x = listClassEnemy[i].x;
                        y = listClassEnemy[i].y;

                        xtemp = listClassEnemy[i].x;
                        ytemp = listClassEnemy[i].y;
                        if (arahgerak == 0) //Ke Bawah
                        {
                            y += 40;
                        }
                        else if (arahgerak == 1) //Ke Kiri
                        {
                            x -= 40;
                        }
                        else if (arahgerak == 2) //Ke Kanan
                        {
                            x += 40;
                        }
                        else if (arahgerak == 3) //Ke Atas
                        {
                            y -= 40;
                        }
                        //temp
                        if (arahgerak == 0)
                        {
                            height += 40;
                        }
                        else if (arahgerak == 1)
                        {
                            xtemp -= 40;
                        }
                        else if (arahgerak == 2)
                        {
                            width += 40;
                        }
                        else if (arahgerak == 3)
                        {
                            ytemp -= 40;
                        }

                        if (!cekTabrak(x, y, i)) { moveornot = 0; }
                        if (cekserang(listClassEnemy[i].x, listClassEnemy[i].y, arahgerak))
                        {
                            moveornot = 0;
                            hasilcekserang = true;
                            //diserang = true;
                            listClassEnemy[i].IsWalking = false;
                            listClassEnemy[i].IsAttacking = true;
                        }
                        else
                        {
                            listClassEnemy[i].IsAttacking = false;
                        }

                    }
                    if (!hasilcekserang)
                    {
                        //cek player
                        Rectangle atemp = new Rectangle(listClassEnemy[i].x - 100, listClassEnemy[i].y - 100, 250, 250);
                        Rectangle btemp = new Rectangle(p.X, p.Y, 50, 50);
                        if (atemp.IntersectsWith(btemp))
                        {
                            moveornot = 1;
                            if (listClassEnemy[i].x <= p.X + 30 && listClassEnemy[i].x >= p.X - 30)
                            {
                                if (listClassEnemy[i].y - p.Y < 0)
                                {
                                    arahgerak = 0;

                                }
                                else
                                {
                                    arahgerak = 3;
                                }
                            }
                            else
                            {
                                if (listClassEnemy[i].x - p.X < 0)
                                {
                                    arahgerak = 2;

                                }
                                else
                                {
                                    arahgerak = 1;
                                }
                            }
                            //MessageBox.Show("X musuh : "+listClassEnemy[i].x+" | Y : "+listClassEnemy[i].y+"\nPlayer X : "+p.X+" | Y : "+p.Y);
                        }
                    }

                    listClassEnemy[i].jalan(moveornot, arahgerak);
                    //g.DrawImage(listClassEnemy[i].b, listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);

                    //g.DrawImage(p.B, p.X, p.Y, 50, 50);
                    //g.DrawImage(img, p.X, p.Y, 50, 50);
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    if (listClassEnemy[i].IsAttacking)
                    {
                        if (listClassEnemy[i].Ctrattack == 0)
                        {
                            //g.DrawImage(slash[ctrserangmusuh], p.X - 25, p.Y - 25, 100, 100);
                            ctrserangmusuh++;
                            if (ctrserangmusuh > 4)
                            {
                                //listClassEnemy[i].Hp -= 30;
                                if(player[job].defBoost) player[job].hp -= (listClassEnemy[i].Attack/2);
                                else player[job].hp -= listClassEnemy[i].Attack;
                                progressBar1.Value = player[job].hp; //PROGRESS BAR HP
                                Console.WriteLine("Sisa Nyawa : " + listClassEnemy[i].Hp);
                                diserang = false;
                                ctrserangmusuh = 0;
                                listClassEnemy[i].Ctrattack++;
                                if (player[job].hp <= 0) {
                                    player[job].hp = 0;
                                    //MessageBox.Show("You Lose!!!");
                                    saveData();
                                    panel1.BackColor = Color.Transparent;
                                    backgroundImg = SetImageOpacity(backgroundImg, 0.25F);
                                    isPaused = true;

                                    for (int j = 0; j < halanganPohon.Count; j++)
                                    {
                                        halanganPohon[j].BackgroundImage = SetImageOpacity(imgphon, 0.25F);
                                    }
                                    timer1.Stop();
                                    timer2.Stop();
                                    timerLightning.Stop();
                                    this.Invalidate();
                                }
                            }
                        }
                        else
                        {
                            if (listClassEnemy[i].Ctrattack > 10)
                            {
                                listClassEnemy[i].Ctrattack = 0;
                                listClassEnemy[i].IsAttacking = false;
                            }
                            else
                            {
                                listClassEnemy[i].Ctrattack++;
                            }
                        }

                    }
                    //g.DrawRectangle(new Pen(Color.Red), xtemp, ytemp, width, height);  //untuk cek range area musuh

                }
            }
            for (int i = listremove.Count-1; i >= 0; i--)
            {
                listClassEnemy.RemoveAt(listremove[i]);
                //spawnenemyhard();
            }

            panel1.Invalidate();
        }

        private void Form1__KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if (e.KeyChar == 'p')
            {
                ((Form1)MdiParent).gold = gold;
                saveData();
                ((Form1)MdiParent).loadGame();
                this.Close();
            }
            if (!diserang && !isPaused)
            {
                if (e.KeyChar == 'w' && bolehjalan)
                {
                    bool cek = true;
                    for (int i = 0; i < listClassEnemy.Count; i++)
                    {
                        Rectangle r1 = new Rectangle(p.X, p.Y - 30, 50, 50);
                        Rectangle r2 = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);

                        if (r1.IntersectsWith(r2)) cek = false;
                    }
                    //p.Y -= 50;
                    if (cek)
                    {
                        img = character[job, 3, 0];
                        tempjalan = 0; arahjalan = 3;
                        bolehjalan = false;
                        timer1.Start();
                    }
                }
                else if (e.KeyChar == 's' && bolehjalan)
                {
                    bool cek = true;
                    for (int i = 0; i < listClassEnemy.Count; i++)
                    {
                        Rectangle r1 = new Rectangle(p.X, p.Y + 30, 50, 50);
                        Rectangle r2 = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);

                        if (r1.IntersectsWith(r2)) cek = false;
                    }
                    if (cek)
                    {
                        //p.Y += 50;
                        img = character[job, 0, 0];
                        tempjalan = 0; arahjalan = 2;
                        bolehjalan = false;
                        timer1.Start();
                    }
                }
                else if (e.KeyChar == 'a' && bolehjalan)
                {
                    bool cek = true;
                    for (int i = 0; i < listClassEnemy.Count; i++)
                    {
                        Rectangle r1 = new Rectangle(p.X - 30, p.Y, 50, 50);
                        Rectangle r2 = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);

                        if (r1.IntersectsWith(r2)) cek = false;
                    }
                    if (cek)
                    {
                        //p.X -= 50;
                        img = character[job, 1, 0];
                        tempjalan = 0; arahjalan = 1;
                        bolehjalan = false;
                        timer1.Start();
                    }
                }
                else if (e.KeyChar == 'd' && bolehjalan)
                {
                    bool cek = true;
                    for (int i = 0; i < listClassEnemy.Count; i++)
                    {
                        Rectangle r1 = new Rectangle(p.X + 30, p.Y, 50, 50);
                        Rectangle r2 = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);

                        if (r1.IntersectsWith(r2)) cek = false;
                    }
                    if (cek)
                    {
                        //p.X += 50;
                        img = character[job, 2, 0];
                        tempjalan = 0; arahjalan = 0;
                        bolehjalan = false;
                        timer1.Start();
                    }

                    for (int i = 0; i < halangan.Count; i++)
                    {
                        //Rectangle r1 = new Rectangle(p.X+50, p.Y, 50, 50);
                        //Rectangle r2 = new Rectangle(halangan[i].X, halangan[i].Y, 50, 50);
                        //if (r1.IntersectsWith(r2))
                        //{
                        //    p.X -= 30;
                        //}
                        //if (p.X + 50 == halangan[i].X && p.Y == halangan[i].Y)
                        //{
                        //    p.X -= 30;
                        //}
                    }
                }
                //USE ITEM
                else if (e.KeyChar == '1')
                {
                    if (listItem[0].banyak > 0)
                    {
                        listItem[0].banyak--;
                        player[job].hp += (player[job].hpbase * 10 / 100);
                        progressBar1.Value = player[job].hp;
                        if (player[job].hp > player[job].hpbase) player[job].hp = player[job].hpbase;
                        this.Invalidate();
                    }
                }
                else if (e.KeyChar == '2')
                {
                    if (listItem[1].banyak > 0)
                    {
                        listItem[1].banyak--;
                        player[job].hp = player[job].hpbase;
                        progressBar1.Value = player[job].hp;
                        this.Invalidate();
                    }
                }
                else if (e.KeyChar == '3')
                {
                    if (listItem[3].banyak > 0)
                    {
                        listItem[3].banyak--;
                        player[job].mp += (player[job].mpbase * 10 / 100);
                        progressBar2.Value = player[job].mp;
                        if (player[job].mp > player[job].mpbase) player[job].mp = player[job].mpbase;
                        this.Invalidate();
                    }
                }
                else if (e.KeyChar == '4' && listItem[4].banyak > 0)
                {
                    player[job].attBoost = true;
                    player[job].timerAttBoost = 200;
                    listItem[4].banyak--;
                    this.Invalidate();
                }
                else if (e.KeyChar == '5' && listItem[5].banyak > 0)
                {
                    player[job].defBoost = true;
                    player[job].timerDefBoost = 200;
                    listItem[5].banyak--;
                    this.Invalidate();
                }
                else if (e.KeyChar == '6' && job == 2)
                {
                    if (listItem[6].banyak > 0)
                    {
                        MessageBox.Show("Test");
                        listItem[6].banyak--;
                        player[job].arrow += 10;
                        this.Invalidate();
                    }
                }
                else if (e.KeyChar == 'l') {
                    player[job].lvl += 10;
                    if (player[job].lvl > 9 && player[job].lvl < 20)
                    {
                        MessageBox.Show("evo 2");
                        if (job == 0) { imgs = Image.FromFile("warrior2.png") as Bitmap; player[job].skill = "skill1"; }
                        else if (job == 1) { imgs = Image.FromFile("mage11.png") as Bitmap; player[job].skill = "skill1"; }
                        else if (job == 2) { imgs = Image.FromFile("archer21.png") as Bitmap; player[job].skill = "skill1"; }
                        cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
                        lblPlayer.Image = img;
                        player[job].hpbase *= 2;
                        player[job].mpbase *= 2;
                        progressBar1.Maximum = player[job].hpbase;
                        progressBar2.Maximum = player[job].mpbase;
                    }
                    else if (player[job].lvl >= 20) {
                        MessageBox.Show("evo 3");
                        if (job == 0) { imgs = Image.FromFile("warrior31.png") as Bitmap; player[job].skill = "skill2"; }
                        else if (job == 1) { imgs = Image.FromFile("mage21.png") as Bitmap; player[job].skill = "skill2"; }
                        else if (job == 2) { imgs = Image.FromFile("archer31.png") as Bitmap; player[job].skill = "skill2"; }
                        cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
                        lblPlayer.Image = img;
                        player[job].hpbase *= 2;
                        player[job].mpbase *= 2;
                        progressBar1.Maximum = player[job].hpbase;
                        progressBar2.Maximum = player[job].mpbase;
                    }
                }
                else if (e.KeyChar == 'q')
                {
                    if (job == 0 && player[job].mp > 10) //warrior
                    {
                        if (player[job].skill.Equals("skill1"))
                        {
                            int index = 0;
                            for (int i = 0; i < weapon.Count; i++)
                            {
                                if (job == weapon[i].jenis && weapon[i].isEquip)
                                {
                                    index = i;
                                }
                            }

                            whirlwind(weapon[index].att);
                        }
                        else if (player[job].skill.Equals("skill2"))
                        {
                            instantKill(arahjalan);
                        }
                        player[job].mp -= 10;
                        progressBar2.Value = player[job].mp; //PROGRESS BAR MP
                    }
                    else if (job == 1) // mage
                    {
                        if (player[job].skill.Equals("skill1") && player[job].mp > 45)
                        {
                            int index = -1;
                            for (int i = 0; i < weapon.Count; i++)
                            {
                                if (job == weapon[i].jenis && weapon[i].isEquip)
                                {
                                    index = i;
                                }
                            }
                            int damage = 0;
                            if (index != -1)
                            {
                                damage = weapon[index].att;
                            }

                            skill1_mage(damage);
                            player[job].mp /= 2;
                            progressBar2.Value = player[job].mp; //PROGRESS BAR MP
                        }
                        else if (player[job].skill.Equals("skill2"))
                        {
                            int index = -1;
                            for (int i = 0; i < weapon.Count; i++)
                            {
                                if (job == weapon[i].jenis && weapon[i].isEquip)
                                {
                                    index = i;
                                }
                            }
                            int damage = 0;
                            if (index != -1)
                            {
                                damage = weapon[index].att;
                            }

                            //player[job].mp /= 2;
                            skill2_mage(damage);
                            progressBar2.Value = player[job].mp; //PROGRESS BAR MP
                        }

                    }
                    else if (job == 2 && player[job].mp > 15) //archer
                    {
                        if (player[job].skill.Equals("skill1"))
                        {
                            int index = 0;
                            for (int i = 0; i < weapon.Count; i++)
                            {
                                if (job == weapon[i].jenis && weapon[i].isEquip)
                                {
                                    index = i;
                                }
                            }
                            doubleAttack();
                            //whirlwind(weapon[index].att);
                        }
                        else if (player[job].skill.Equals("skill2"))
                        {
                            piercingShot();
                        }
                        player[job].mp -= 15;
                        progressBar2.Value = player[job].mp; //PROGRESS BAR MP
                    }

                }
            }
        }

        public void skill1_mage(int damage)
        {
            //skill damage x 1,5 -- mana cost -50%

            thunderSize = 50;
            dmgmage = (damage * 3) / 2;
            mageSKill = true;
            bolehSerang = false;
            bolehjalan = false;
            timer1.Start();

            
        }
        public void skill2_mage(int damage)
        {
            //skill damage x2 -- no mana

            thunderSize = 100;
            dmgmage = damage * 2;
            mageSKill = true;
            bolehSerang = false;
            bolehjalan = false;
            timer1.Start();

        }
        //UNTUK PLAYER
        private void InfoPanel_ClickLabel(object sender, EventArgs e)
        {
            timer2.Stop();
            panel1.BackColor = Color.Transparent;
            backgroundImg = SetImageOpacity(backgroundImg, 0.25F);
            this.Invalidate();
            lblPlayer.BackColor = Color.Transparent;
            lblPlayer.Image = SetImageOpacity(img, 0.5F);
            infoCharacter.BringToFront();
            isPaused = true;

            //MessageBox.Show("Info Karakter!!!");
            infoCharacter.Visible = true;
            infoCharacter.BackColor = Color.FromArgb(150, Color.MintCream);
            panelInven.BackColor = Color.FromArgb(150, Color.MintCream);
        }
        private void DoubleClick_EquipWeapon(object sender, MouseEventArgs e)
        {
            clickWeaponX = e.X - 18; clickWeaponX /= 25;
            clickWeaponY = e.Y; clickWeaponY /= 66;

            bool cekBenarEquip = false;
            int index = clickWeaponX + clickWeaponY * 6;
            if (weapon[index].jenis == job)
            {
                weapon[index].isEquip = true;
                cekBenarEquip = true;
            }

            for (int i = 0; i < weapon.Count; i++)
            {
                if (cekBenarEquip)
                {
                    if (job == 0 && weapon[i].jenis == 0 && i != index) weapon[i].isEquip = false;
                    else if (job == 1 && weapon[i].jenis == 1 && i != index) weapon[i].isEquip = false;
                    else if (job == 2 && weapon[i].jenis == 2 && i != index) weapon[i].isEquip = false;
                }
            }
            panelInven.Invalidate();
        }

        private void Click_Weapon(object sender, MouseEventArgs e)
        {
            if (e.X >= 19 && e.X <= 170)
            {
                clickWeaponX = e.X - 18; clickWeaponX /= 25;
                clickWeaponY = e.Y; clickWeaponY /= 66;
            }
            
            panelInven.Invalidate();
            //MessageBox.Show("X : " + e.X + "\nY : " + e.Y);
            //170x20
        }

        private void panelInven_paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(bgInven, 18, 0, 150, 200);
            Brush b = new SolidBrush(Color.Black);
            Brush bw = new SolidBrush(Color.White);
            Font f = new Font("Monotype Corsiva", 13, FontStyle.Bold);
            // buat kluarin shop
            for (int i = 0; i < weapon.Count; i++)
            {
                if (weapon[i].jenis == 0) g.DrawImage(cropWeapon[0], i % 6 * 25 + 15, i / 6 * 70, 55, 50); //draw sword
                else if (weapon[i].jenis == 1) g.DrawImage(cropWeapon[1], i % 6 * 25 + 9, i / 6 * 70, 55, 50); //draw staff
                else if (weapon[i].jenis == 2) g.DrawImage(cropWeapon[2], i % 6 * 25, i / 6 * 70, 55, 50); //draw bow

                //draw gambar e utk equip
                if (weapon[i].isEquip) g.DrawString("E", f, bw, i % 6 * 25 + 25, i / 6 * 67);
            }
            if (clickWeaponX != -1 && clickWeaponY != -1)
            {
                int index = clickWeaponX + clickWeaponY * 6;
                if (weapon[index].jenis == 0) g.DrawImage(cropWeapon[0], 193, 10, 55, 50);
                else if (weapon[index].jenis == 1) g.DrawImage(cropWeapon[1], 193 - 6, 10, 55, 50);
                else if (weapon[index].jenis == 2) g.DrawImage(cropWeapon[2], 193 - 15, 10, 55, 50);
                g.DrawString(weapon[index].att + "", f, b, 195, 70);
                g.DrawString("+" + weapon[index].plusUpgrade, f, b, 213, 20);
                
            }
            
        }

        private void Change_Job(object sender, MouseEventArgs e)
        {
            Rectangle r = new Rectangle(e.X, e.Y, 1, 1);
            Rectangle r1 = new Rectangle(10, 130, 60, 30);
            Rectangle r2 = new Rectangle(70, 130, 50, 30);
            Rectangle r3 = new Rectangle(120, 130, 50, 30);

            if (r.IntersectsWith(r1))
            {
                job = 0;
                //masukkan gambar ke array
                if (player[job].lvl < 10) imgs = Image.FromFile("character1.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("warrior2.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("warrior31.png") as Bitmap;
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();

                progressBar1.ForeColor = Color.Red;
                progressBar1.Maximum = player[job].hpbase;
                progressBar1.Value = player[job].hp;
                progressBar2.ForeColor = Color.Blue;
                progressBar2.Maximum = player[job].mpbase;
                progressBar2.Value = player[job].mp;
            }
            else if (r.IntersectsWith(r2))
            {
                job = 1;
                //masukkan gambar ke array
                if (player[job].lvl < 10) imgs = Image.FromFile("character2.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("mage11.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("mage21.png") as Bitmap;
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();

                progressBar1.ForeColor = Color.Red;
                progressBar1.Maximum = player[job].hpbase;
                progressBar1.Value = player[job].hp;
                progressBar2.ForeColor = Color.Blue;
                progressBar2.Maximum = player[job].mpbase;
                progressBar2.Value = player[job].mp;
            }
            else if (r.IntersectsWith(r3))
            {
                job = 2;
                //masukkan gambar ke array
                if (player[job].lvl < 10) imgs = Image.FromFile("character3.png") as Bitmap;
                else if (player[job].lvl < 20) imgs = Image.FromFile("archer21.png") as Bitmap;
                else if (player[job].lvl < 30) imgs = Image.FromFile("archer31.png") as Bitmap;
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();

                progressBar1.ForeColor = Color.Red;
                progressBar1.Maximum = player[job].hpbase;
                progressBar1.Value = player[job].hp;
                progressBar2.ForeColor = Color.Blue;
                progressBar2.Maximum = player[job].mpbase;
                progressBar2.Value = player[job].mp;
            }
            lblPlayer.Image = img;
        }

        private void button_Inventory(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            MessageBox.Show("INVENTORY!!!");
            infoCharacter.Visible = false;
            panelInven.Visible = true;
            panelInven.BringToFront();
            clickWeaponX = -1; clickWeaponY = -1;
        }

        List<int> hasilHeapY = new List<int>();
        List<int> hasilHeapX = new List<int>();

        private void panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //spawnenemymedium();
            int titikX = e.X / 50;
            int titikY = e.Y / 50;
            int playerX = p.X / 50;
            int playerY = p.Y / 50;

            if (hasilHeapX.Count != 0) playerX = hasilHeapX[hasilHeapX.Count - 1];
            if (hasilHeapY.Count != 0) playerY = hasilHeapY[hasilHeapY.Count - 1];

            MessageBox.Show("Click : \nX: " + titikX + "\nY: " + titikY);
            //MessageBox.Show("Player : \nX: " + playerX + "\nY: " + playerY);

            Maze m = new Maze();

            int[,] maptemp = new int[10, 10];
            StreamReader sr = new StreamReader(Application.StartupPath + "/map.txt");
            string temp = "";

            temp = sr.ReadToEnd();
            //Console.WriteLine(temp);

            string[] maptemp2 = new string[10];
            for (int i = 0; i < 10; i++)
            {
                maptemp2 = temp.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            }

            //MessageBox.Show(maptemp2.Length + "");

            //dirubah jadi 0 / 1
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    string tempstring = maptemp2[i].Substring(j, 1);
                    if (tempstring == "Z") tempstring = "0";
                    else if (tempstring == "X") tempstring = "1";
                    maptemp[i, j] = Convert.ToInt32(tempstring);
                }
            }
            sr.Close();

            //string halo = "";
            //for (int i = 0; i < 10; i++)
            //{
            //    for (int j = 0; j < 10; j++)
            //    {
            //        Console.Write(maptemp[i, j]);
            //        halo += maptemp[i, j];
            //    }
            //    halo += "\n";
            //    Console.WriteLine();
            //}
            //MessageBox.Show(halo + "..."); //cetak map

            //m.InitMaze();
            m.InitMaze(maptemp, playerY, playerX, titikY, titikX);
            m.SolveMaze();
            //m.PrintMaze();

            List<int> hasilHeapYtemp;
            List<int> hasilHeapXtemp;
            //hasilHeapX = m.jalurkecily();
            //hasilHeapY = m.jalurkecilx();

            string tempp = "";
            //MessageBox.Show(m.CompletePathX[0].Count + "");
            //for (int i = 0; i < m.CompletePathX[0].Count; i++)
            //{
            //    tempp += "[";
            //    tempp += m.CompletePathY[0][i].ToString();
            //    tempp += ",";
            //    tempp += m.CompletePathX[0][i].ToString();
            //    tempp += "], ";
            //}
            //MessageBox.Show("jalur TERKECIL : " + tempp);

            //hasilHeapXtemp = m.CompletePathY[0];
            //hasilHeapYtemp = m.CompletePathX[0];

            //pake heap
            hasilHeapYtemp = m.jalurkecilx();
            hasilHeapXtemp = m.jalurkecily();

            //MessageBox.Show(hasilHeapXtemp.Count + "");

            hasilHeapX.AddRange(hasilHeapXtemp);
            hasilHeapY.AddRange(hasilHeapYtemp);
            //MessageBox.Show(hasilHeapX.Count + "");

            tempp = "";
            //for (int i = 0; i < hasilHeapX.Count; i++) //CETAK HASIL HEAPNYA
            //{
            //    tempp += "[";
            //    tempp += hasilHeapX[i].ToString();
            //    tempp += ",";
            //    tempp += hasilHeapY[i].ToString();
            //    tempp += "], ";
            //}
            //MessageBox.Show("jalur TERKECIL : " + tempp);
            
            //if (p.X % 50 != 0) //coba"
            //{
            //    int xTemp = p.X / 50;
            //    int TempxKiri = p.X - (xTemp * 50);
            //    int TempxKanan = 50 - TempxKiri;
            //    if (arahJalanSDL == 0) p.X -= TempxKiri;
            //    else if (arahJalanSDL == 1) p.X += TempxKanan;
            //}
            //if (p.Y % 50 != 0)
            //{
            //    int yTemp = p.Y / 50;
            //    int TempyKiri = p.Y - (yTemp * 50);
            //    int TempyKanan = 50 - TempyKiri;
            //    if (arahJalanSDL == 2) p.Y -= TempyKiri;
            //    else if (arahJalanSDL == 3) p.Y += TempyKanan;
            //}

            //ClsLinkList root = new ClsLinkList();
            //root = m.getFibo;
            //ClsNode nodetemp = new ClsNode();
            //nodetemp = root.findMin();
            //for (int i = 0; i < nodetemp.X.Count; i++)
            //{
            //    Console.WriteLine("ke-" + i + "   ===>>> " + nodetemp.X[i]);
            //}
            //MessageBox.Show("jalan terpendek adalah " + nodetemp.getNama() + " - " + " dengan jarak " + nodetemp.getBobot());
            timerSDL.Start();
            lagiSDL = true;
        }

        int ctrSDL = 0;
        bool lagiSDL = false;
        int arahJalanSDL = 0;
        private void timerSDL_Tick(object sender, EventArgs e)
        {
            //MessageBox.Show(lagiSDL+"");
            if (ctrSDL != hasilHeapX.Count)
            {
                //MessageBox.Show(lagiSDL + "");
                //MessageBox.Show(hasilHeapX[ctrSDL] + "," + hasilHeapY[ctrSDL]);

                if (hasilHeapX[ctrSDL] != (p.X/50))
                {
                    if (hasilHeapX[ctrSDL] < (p.X / 50))
                    {
                        img = character[job, 1, 0];
                        tempjalan = 0; arahjalan = 1;
                        bolehjalan = false;
                        timer1.Start();
                        arahJalanSDL = 0;
                    }
                    else
                    {
                        img = character[job, 2, 0];
                        tempjalan = 0; arahjalan = 0;
                        bolehjalan = false;
                        timer1.Start();
                        arahJalanSDL = 1;
                    }
                }
                else if(hasilHeapY[ctrSDL] != (p.Y / 50))
                {
                    if (hasilHeapY[ctrSDL] < (p.Y / 50))
                    {
                        img = character[job, 3, 0];
                        tempjalan = 0; arahjalan = 3;
                        bolehjalan = false;
                        timer1.Start();
                        arahJalanSDL = 2;
                    }
                    else
                    {
                        img = character[job, 0, 0];
                        tempjalan = 0; arahjalan = 2;
                        bolehjalan = false;
                        timer1.Start();
                        arahJalanSDL = 3;
                    }
                }

                hasilHeapX.RemoveAt(0);
                hasilHeapY.RemoveAt(0);
                //ctrSDL++;
            }
            else { timerSDL.Stop(); lagiSDL = false; }
        }

        private void infoCharacter_paint(object sender, PaintEventArgs e)
        {

            Graphics g = e.Graphics;
            Brush b = new SolidBrush(Color.Black);
            Font f = new Font("Monotype Corsiva", 17, FontStyle.Bold);

            //cetak border job
            if (job == 0) g.DrawImage(borderJob, 8, 128, 62, 26);
            else if (job == 1) g.DrawImage(borderJob, 68, 128, 52, 26);
            else if (job == 2) g.DrawImage(borderJob, 118, 128, 52, 26);

            //cetak border Character
            //g.DrawImage(borderChar, 120, 10, 60, 70);

            g.DrawImage(img, 135, 20, 50, 50);
            g.DrawString("HP : " + player[job].hp + "/" + player[job].hpbase, f, b, 10, 10);
            g.DrawString("MP : " + player[job].mp + "/" + player[job].mpbase, f, b, 10, 40);
            g.DrawString("Lvl : " + player[job].lvl, f, b, 10, 70);
            g.DrawString("Gold : " + gold + " Dz", f, b, 10, 100);
            f = new Font("Monotype Corsiva", 13, FontStyle.Bold);
            g.DrawString("Warrior", f, b, 10, 130);
            g.DrawString("Mage", f, b, 70, 130);
            g.DrawString("Archer", f, b, 120, 130);
        }
        public Image SetImageOpacity(Image image, float opacity)
        {
            Bitmap bmp = new Bitmap(image.Width, image.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                ColorMatrix matrix = new ColorMatrix();
                matrix.Matrix33 = opacity;
                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default,
                                                  ColorAdjustType.Bitmap);
                g.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height),
                                   0, 0, image.Width, image.Height,
                                   GraphicsUnit.Pixel, attributes);
            }
            return bmp;
        }

        private void timerLightning_Tick(object sender, EventArgs e)
        {
            
        }

        

        public void cropAttack()
        {
            //potong gambar buat animasi pedang
            animationAttack = new Image[3, 4, 3];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i <= 1)
                    {
                        Rectangle r = new Rectangle((i * 360) + (j * 180), 180, 180, 180);
                        Bitmap bmp = new Bitmap(Image.FromFile("swordslash.png"));
                        animationAttack[0, i, j] = bmp.Clone(r, bmp.PixelFormat);
                    }
                    else if (i == 2)
                    {
                        Rectangle r = new Rectangle(j * 180, 360, 180, 180);
                        Bitmap bmp = new Bitmap(Image.FromFile("swordslash.png"));
                        animationAttack[0, i, j] = bmp.Clone(r, bmp.PixelFormat);
                    }
                    else if (i == 3)
                    {
                        Rectangle r = new Rectangle(j * 180 + 360, 540, 180, 180);
                        Bitmap bmp = new Bitmap(Image.FromFile("swordslash.png"));
                        animationAttack[0, i, j] = bmp.Clone(r, bmp.PixelFormat);
                    }
                }
            }
            for (int i = 0; i < 3; i++)
            {
                Rectangle r = new Rectangle(i * 192 + 192, 576, 192, 192);
                Bitmap bmp = new Bitmap(Image.FromFile("magethunder.png"));
                for (int j = 0; j < 4; j++)
                {
                    animationAttack[1, j, i] = bmp.Clone(r, bmp.PixelFormat);
                }
            }
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i == 0) animationAttack[2, i, j] = Image.FromFile((j + 1) + " kanan.png");
                    else if (i == 1) animationAttack[2, i, j] = Image.FromFile((j + 1) + " kiri.png");
                    else if (i == 2) animationAttack[2, i, j] = Image.FromFile((j + 1) + " bwh.png");
                    else if (i == 3) animationAttack[2, i, j] = Image.FromFile((j + 1) + " atas.png");
                }
            }
        }

        private void panel1_MouseClick_1(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (player[job].hp > 0)
                {
                    Rectangle mouse = new Rectangle(e.X, e.Y, 1, 1);
                    Rectangle karakter = new Rectangle(p.X - 50, p.Y, 50, 50);
                    if (mouse.IntersectsWith(karakter))
                    {
                        //MessageBox.Show("Info Karakter!!!");
                        //infoCharacter.Visible = true;
                        //pindah di event click pada label player
                    }
                    else
                    {
                        timer2.Start();
                        isPaused = false;
                        //ganti OPACITY
                        panel1.BackColor = Color.Transparent;
                        backgroundImg = Image.FromFile("mapdungeon.png");
                        this.Invalidate();
                        lblPlayer.BackColor = Color.Transparent;
                        lblPlayer.Image = SetImageOpacity(img, 1F);

                        infoCharacter.Visible = false;
                        panelInven.Visible = false;

                        //animasi serang
                        if ((job == 2 && player[job].arrow > 0) || job == 1 || job == 0)
                        {
                            if (job == 2) { player[job].arrow--; }
                            bolehSerang = false;
                            tempanimasi = 0;
                            imgAtt = animationAttack[job, arahjalan, 0];
                            timer1.Start();
                        }

                        //BUAT SERANG!!!!
                        serang();
                    }
                }
            }
        }
        public void serang()
        {
            int serang = 0;
            for (int i = 0; i < weapon.Count; i++)
            {
                if (job == weapon[i].jenis && weapon[i].isEquip) serang = weapon[i].att;
            }
            if (player[job].attBoost) serang = serang + (serang / 2); //buat boost item attack
            if (arahjalan == 0)
            {
                //kanan
                Rectangle kotakPlayer = new Rectangle(p.X + 50, p.Y, 50, 50);
                for (int i = 0; i < listClassEnemy.Count; i++)
                {
                    Rectangle mshKotak = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                    if (kotakPlayer.IntersectsWith(mshKotak))
                    {
                        listClassEnemy[i].Hp -= serang;
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyeasy) tambahExp("easy"); 
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemymedium) tambahExp("medium"); 
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyHard) tambahExp("hard"); 
                    }
                }

            }
            else if (arahjalan == 1)
            {
                //kiri
                Rectangle kotakPlayer = new Rectangle(p.X - 50, p.Y, 50, 50);
                for (int i = 0; i < listClassEnemy.Count; i++)
                {
                    Rectangle mshKotak = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                    if (kotakPlayer.IntersectsWith(mshKotak))
                    {
                        listClassEnemy[i].Hp -= serang;
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyeasy) tambahExp("easy"); 
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemymedium) tambahExp("medium"); 
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyHard) tambahExp("hard");
                    }
                }
            }
            else if (arahjalan == 2)
            {
                //bawah
                Rectangle kotakPlayer = new Rectangle(p.X, p.Y + 50, 50, 50);
                for (int i = 0; i < listClassEnemy.Count; i++)
                {
                    Rectangle mshKotak = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                    if (kotakPlayer.IntersectsWith(mshKotak))
                    {
                        listClassEnemy[i].Hp -= serang;
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyeasy) tambahExp("easy"); 
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemymedium) tambahExp("medium"); 
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyHard) tambahExp("hard"); 
                    }
                }
            }
            else if (arahjalan == 3)
            {
                //atas
                Rectangle kotakPlayer = new Rectangle(p.X, p.Y - 50, 50, 50);
                for (int i = 0; i < listClassEnemy.Count; i++)
                {
                    Rectangle mshKotak = new Rectangle(listClassEnemy[i].x, listClassEnemy[i].y, 50, 50);
                    if (kotakPlayer.IntersectsWith(mshKotak))
                    {
                        listClassEnemy[i].Hp -= serang;
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyeasy) tambahExp("easy"); 
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemymedium) tambahExp("medium"); 
                        if (listClassEnemy[i].Hp <= 0 && listClassEnemy[i] is enemyHard) tambahExp("hard"); 
                    }
                }
            }
        }
        public void tambahExp(string musuh) {
            if (musuh == "easy") { player[job].exp += 10; gold += 100; }
            else if (musuh == "medium") { player[job].exp += 15; gold += 110; }
            else if (musuh == "hard") { player[job].exp += 120; gold += 120; }

            if (player[job].exp >= player[job].batasExp) {
                player[job].exp -= player[job].batasExp;
                player[job].batasExp += (player[job].batasExp / 5);
                player[job].lvl+= 1;
                player[job].hpbase += 1;
                player[job].mpbase += 1;
                progressBar1.Maximum = player[job].hpbase;
                progressBar2.Maximum = player[job].mpbase;
            }
            if (player[job].lvl >= 10 && player[job].lvl < 20)
            {
                //MessageBox.Show("evo 2");
                if (job == 0) { imgs = Image.FromFile("warrior2.png") as Bitmap; player[job].skill = "skill1"; }
                else if (job == 1) { imgs = Image.FromFile("mage11.png") as Bitmap; player[job].skill = "skill1"; }
                else if (job == 2) { imgs = Image.FromFile("archer21.png") as Bitmap;player[job].skill = "skill1"; }
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate(); 
                lblPlayer.Image = img;
                player[job].hpbase *= 2;
                player[job].mpbase *= 2;
                progressBar1.Maximum = player[job].hpbase;
                progressBar2.Maximum = player[job].mpbase;

            }
            else if (player[job].lvl >= 20) {
                //MessageBox.Show("evo 3");
                if (job == 0) { imgs = Image.FromFile("warrior31.png") as Bitmap; player[job].skill = "skill2"; }
                else if (job == 1) { imgs = Image.FromFile("mage21.png") as Bitmap; player[job].skill = "skill2"; }
                else if (job == 2) { imgs = Image.FromFile("archer31.png") as Bitmap; player[job].skill = "skill2"; }
                cropImage(); panel1.Invalidate(); infoCharacter.Invalidate();
                lblPlayer.Image = img;
                player[job].hpbase *= 2;
                player[job].mpbase *= 2;
                progressBar1.Maximum = player[job].hpbase;
                progressBar2.Maximum = player[job].mpbase;
            }
        }

        //untuk save data
        public void saveData() {
            //save posisi dan last job
            StreamWriter sw = new StreamWriter(Application.StartupPath + "/player.txt");
            sw.Write(((Form1)MdiParent).pX + "-" + ((Form1)MdiParent).pY + "-" + job + "-" + arahjalan + "-" + gold);
            sw.Close();

            System.IO.File.WriteAllText("inven.xml", string.Empty);
            Stream stream = File.OpenWrite(Environment.CurrentDirectory + "\\inven.xml");
            XmlSerializer xmlser = new XmlSerializer(typeof(List<inventory>));
            xmlser.Serialize(stream, weapon);
            stream.Close();

            System.IO.File.WriteAllText("item.xml", string.Empty);
            stream = File.OpenWrite(Environment.CurrentDirectory + "\\item.xml");
            xmlser = new XmlSerializer(typeof(List<Item>));
            xmlser.Serialize(stream, listItem);
            stream.Close();

            // save info dari karakter spt hp, mp dkk
            sw = new StreamWriter(Application.StartupPath + "/playerInfo.txt");
            for (int i = 0; i < 3; i++)
            {
                sw.WriteLine(player[i].hp + "-" + player[i].mp + "-" + player[i].lvl + "-" + player[i].skill + "-" + player[i].exp + "-" + player[i].hpbase + "-" + player[i].mpbase + "-" + player[i].arrow);
            }
            sw.Close();
        }

    }
    //class enemy
    public class enemy
    {
        protected int hp;
        protected int attack;
        public int x { get; set; }
        public int y { get; set; }
        public int Arah { get => arahgerak; set => arahgerak = value; }
        public bool IsWalking { get => isWalking; set => isWalking = value; }
        public bool IsAttacking { get => isAttacking; set => isAttacking = value; }
        public int Status { get => status; set => status = value; }
        public int Hp { get => hp; set { hp = value; if (this.hp <= 0) { this.status = 0;ctrMati = 0; this.b = Mati[ctrMati]; } } }
        public int Attack { get => attack; set => attack = value; }
        public int CtrMati { get => ctrMati; set => ctrMati = value; }
        public Image[] Mati { get => mati; set => mati = value; }
        public int Ctrattack { get => ctrattack; set => ctrattack = value; }

        protected Image[] a = new Image[3]; //tidak dipakai

        public Image b; //image yang digambar di form

        //animasi jalan
        protected int rndgerak = 0;
        private bool isWalking = false;
        protected int ctrwalk = 0;

        protected Image[,] cobaaja = new Image[4, 3]; //frame gerak

        //untuk animasi menyerang
        protected Image[,] menyerang = new Image[4, 2];
        protected int arahgerak;
        protected bool towardplayer = false; //ke arah player
        //kalau menyerang ganti animasi
        private bool isAttacking = false;

        protected int ctrattack = 0;

        protected int status = 1;
        //status 1->alive 0->death
        protected bool death = false;
        //kalau true mainkan animasi mati
        private Image[] mati = new Image[8];
        private int ctrMati;
        //animasi ketika enemy mati

        public enemy()
        {
            
        }

        public void jalan(int moveornot, int arah)
        {
            if (!IsWalking)
            {
                if (moveornot == 1)
                {
                    //move
                    IsWalking = true;
                    this.arahgerak = arah;
                }

            }
            else
            {
                if (arahgerak == 0)
                {
                    this.y += 10;
                }
                else if (arahgerak == 1)
                {
                    this.x -= 10;
                }
                else if (arahgerak == 2)
                {
                    this.x += 10;
                }
                else if (arahgerak == 3)
                {
                    this.y -= 10;
                }

                if (this.ctrwalk > 5)
                {
                    //MessageBox.Show("X : "+this.x+" | Y : "+this.y);
                    IsWalking = false;
                    ctrwalk = 0;
                }
                this.ctrwalk++;
            }

            if (isAttacking)
            {


            }
            else
            {
                b = cobaaja[arahgerak, rndgerak];
                if (rndgerak == 0)
                {
                    rndgerak = 2;
                }
                else if (rndgerak == 2)
                {
                    rndgerak = 1;
                }
                else if (rndgerak == 1)
                {
                    rndgerak = 0;
                }
            }
        }
       
    }
    public class enemyHard : enemy{
        public enemyHard(Image[] ab, Image[,] test) : base()
        {
            this.attack = 80;
            this.hp = 300;
            this.status = 1;
            this.a = ab;
            this.cobaaja = test;
            b = this.cobaaja[0, 1];
            
        }
    }
    public class enemyeasy : enemy
    {
        public enemyeasy(Image[] ab, Image[,] test) : base()
        {
            this.attack = 10;
            this.hp = 75;
            this.status = 1;
            this.a = ab;
            this.cobaaja = test;
            b = this.cobaaja[0, 1];

        }
    }
    public class enemymedium : enemy
    {
        public enemymedium(Image[] ab, Image[,] test) : base()
        {
            this.attack = 50;
            this.hp = 150;
            this.status = 1;
            this.a = ab;
            this.cobaaja = test;
            b = this.cobaaja[0, 1];
        }
    }

    //bos berbeda dengan enemy biasa
    public class enemyboss : enemy {
        public enemyboss(Image[] ab, Image[,] test) : base()
        {
            this.a = ab;
            this.cobaaja = test;
            b = this.cobaaja[0, 1];

        }
        public void jalan()
        {
            
        }
    }

    public class player {
        int x;
        int y;
        Image b;
        //Image[,] serang = new Image[4, 3];
        public player()
        {
            this.x = 50;
            this.y = 50;
            this.B = Image.FromFile("ghost.png");
        }
        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
        public Image B { get => b; set => b = value; }
    }
}


